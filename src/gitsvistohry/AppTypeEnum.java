/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry;

/**
 *
 * @author Ondra
 */
public enum AppTypeEnum {
    U_N_K_N_O_W_N("unknownn","Wait, wut"), // na pořadí  ZÁLEŽÍ!!!! v AppMyMy.java se podle toho nastavujou hry.
    GAME(Settings.APP_FOLDER_GAME,"Hry"),
    KRAVINA(Settings.APP_FOLDER_KRAVINA,"Kraviny"),
    SKOLA(Settings.APP_FOLDER_SKOLA,"Škola"),
    PRODVA(Settings.APP_FOLDER_PRODVA,"Hry pro více hráčů");

    private final String folderName;
    private final String nameOfGroup;

    private AppTypeEnum(String folderName,String nameOfGroup) {
        this.folderName = folderName;
        this.nameOfGroup=nameOfGroup;
    }
    
    public static AppTypeEnum getEnByOrdinal(int num){
        return values()[num];
    }

    public String getNameOfGroup() {
        return nameOfGroup;
    }
    
    public String getFolderName() {
        return folderName;
    }

}
