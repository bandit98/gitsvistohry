/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.*;
import java.io.File;
import javafx.application.Application;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import gitsvistohry.IMGS.*;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Ondra
 */
public class FXMLDocument_launcherController implements Initializable { //    private final String[][] gamesInStr = Game.getGameInString();

//    private GridPane mainGridID;
//    private TilePane mainTilePaneID;
    @FXML
    private AnchorPane BasePaneID;
    @FXML
    private Pane paneLogoID;
    @FXML
    private TabPane AppTabPane;

//    private Button[] gameButtons;

    /* seznam her  */
    private final AppMyMy[][] Apps = AppMyMy.getAppsMyMy();
    private final int appsPadding=14;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        AddMainLogoImage();
        AddAppsToPane();
        
        BasePaneID.setId("mainPane");
    }

    private void AddAppsToPane() {
        for (int i = 1; i < AppMyMy.getAppsMyMy().length; i++) {
            AppTypeEnum aptenum=AppTypeEnum.getEnByOrdinal(i);
            Tab tab = new Tab(aptenum.getNameOfGroup());
            tab.setClosable(false);
            AppTabPane.getTabs().add(tab); //tady změnit na název skupiny, né složky
            TilePane tilePane = new TilePane(){{setId("GamePaneInTab");}};
            tilePane.setPadding(new Insets(appsPadding,appsPadding,appsPadding,appsPadding));
            tab.setContent(tilePane);
            for (AppMyMy appMyMy : Apps[i]) {
                tilePane.getChildren().add(appMyMy.getAppNode());
                appMyMy.getAppNode().setOnMouseClicked(new EventHandler<MouseEvent>() {    //při stisknutí tlačítka se spustí handleButtonAction
                    @Override
                    public void handle(MouseEvent event) {
                        handleButtonAction(appMyMy.getName());
                    }
                });
            }

        }
    }

    private void AddMainLogoImage() {
        Image img = ImageMyMy.getImage("launcher", "mainLogo.png");
        BasePaneID.getChildren().add(new ImageView(img));
    }

    /**
     * metoda se spustit hru, její název pozná podle textu na tlačítku že
     * kterýho byla spuštěna
     *
     * @param event - Event tlačítka hry
     */
    private void handleButtonAction(String name) {
        if (TryStartApp(name)) ;
    }

    private boolean TryStartApp(String PossibleGameName) {
        Parent root;
        String selectedGamePath;
        String FinalPath;
        String GamePath = "";
        String GameFolder="";
        try {
            GamePath = AppMyMy.getPathInFolder(PossibleGameName);
            GameFolder=AppMyMy.getFolderByEnum(PossibleGameName);
            FinalPath = "/" + Settings.PROJECT_FOLDER + "/" + 
                    GameFolder + "/" + GamePath;
        } catch (Exception ex) {
            System.err.println("Nepodařilo se načíst cestu k aplikaci z názvu tlačítka. Resp. prostě název tlačítka nikde.");
            return true;
        }
        Settings.Originalscene = Settings.mainStage.getScene();
        try {
            if (FinalPath.endsWith(".fxml")) {
                OpenAppByFXML(FinalPath);
            } else {
                OpenAppByJavaFile(FinalPath.replaceAll("/", ".").substring(1),
                        AppMyMy.getApp_byPath(GamePath).isRunSeparatedApplication());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        //nastavení tittle
        if (AppMyMy.PlayingRightNow) {
            try {
                Settings.mainStage.setTitle(AppMyMy.getAppName(GamePath));
                Settings.mainStage.getIcons().clear();
                Settings.mainStage.getIcons().add(AppMyMy.getApp_byName(PossibleGameName).getAppImage());
            } catch (Exception ex) {
                Logger.getLogger(FXMLDocument_launcherController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return false;
    }

    private boolean OpenAppByFXML(String path) {
        Parent root;
        System.out.println("Trying open (FXML) path: " + path);
        try {
            URL location = getClass().getResource(path);
            System.out.println("Loading FXML URL -> location: " + location + "\n");
            root = FXMLLoader.load(location);
        } catch (Exception ex) {
            System.err.println("Nepodařilo se načíst FXML aplikace. [" + path + "]");
            return false;
        }
        Scene scene = new Scene(root);
        Settings.mainStage.setScene(scene);
        Settings.mainStage.show();
        AppMyMy.PlayingRightNow = true;
        return true;
    }

    private boolean OpenAppByJavaFile(String path, boolean separated) throws Exception {
        if (separated) {
            System.out.println("Trying run (WITHOUT FXML) separated App -> path: " + path);
            Class clazz = Class.forName(path);
            Application app2 = (Application) clazz.newInstance();
            Stage anotherStage = new Stage();
            app2.start(anotherStage);
        } else {
            try {
                System.out.println("Trying open (WITHOUT FXML) path: " + path);
                Class clazz = Class.forName(path);
                AbstractApp abs = (AbstractApp) clazz.newInstance();
                Settings.mainStage.setScene(abs.getScene());
            } catch (Exception e) {
                System.err.println("Error - OpenGameByJavaFile: " + e.getMessage());
                return false;
            }
            AppMyMy.PlayingRightNow = true;
            return true;
        }
        return true;
    }

}
