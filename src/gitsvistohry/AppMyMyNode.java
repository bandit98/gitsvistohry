/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Ondra
 */
public class AppMyMyNode extends Group {
    private AppMyMy game=null;
    private int width;
    private int heigh;
    
    private ImageView imageView;
    private Label label=new Label(){{setId("fancytext");}};

    private VBox vbox = new VBox();
    
    
    public String getGameName() {
        return game.getName();
    }

    
    
    
    public AppMyMyNode(AppMyMy game) {
        this(game,110,120);
    }
    
    public AppMyMyNode(AppMyMy game, int width, int heigh) {
        this.game = game;
        this.width=width;
        this.heigh=heigh;
        this.setId("myGame");
        initialize();
    }
    
    private void initialize() {
        
        imageView=new ImageView(game.getAppImage());
        imageView.setFitHeight(heigh);
        imageView.setFitWidth(width);
        label.setText(game.getName());
        
        vbox.setMinSize(width+80, heigh+80);
        vbox.getChildren().addAll(imageView,label);
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(20);
        vbox.setPadding(new Insets(20, 20, 20, 20));
        
        
        this.getChildren().add(vbox);
    }
}
