/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.IMGS;

import java.io.File;
import javafx.scene.image.Image;

/**
 *
 * @author Ondra
 */
public class ImageMyMy {

    public static Image getImage(String fileName) {
        try {
            Image image = new Image(ImageMyMy.class.getResourceAsStream(fileName));
            if (image.isError()) {
                System.out.println("Image "+fileName+" is Error");
            }
//            System.out.println(fileName+" image FOUND.");
            return image;
        } catch (Exception e) {
            Image image = new Image(ImageMyMy.class.getResourceAsStream("notFound.png"));
                System.out.println("Opening "+fileName+" failed. Opening 'not found'");
            return image;
        }
    }
    
    public static Image getImage(String partOfPath, String... partsOfPath) {
        String filename=partOfPath;
        for(String part : partsOfPath){
            filename+="/"+part;
        }
        return getImage(filename);
    }

    public static void main(String[] args) {

    }
}
