package gitsvistohry;

/**
 * (Tady sou funkce, který se nám hodí když to vyvíjíme ale nemají pro výslednej
 * program žádnej úplně velkej užitek :)
 *
 * @author Ondra
 */
public class DevFunctions {

    public static void SOUT_myOutput_nepodstatne(Object str) {
        System.out.println("MyOutput: " + str);
    }

    public static void SOUT_myOutput_nepodstatne(Object str, boolean error) {
        if (error) {
            System.err.println("MyOutput: " + str);
        } else {
            SOUT_myOutput_nepodstatne(str);
        }
    }

    public static void SOUT_myOutput_nepodstatne(Object str, int error) {
        if (error == 1) {
            SOUT_myOutput_nepodstatne(str, true);
        } else {
            SOUT_myOutput_nepodstatne(str, false);
        }
    }
}
