/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry;

import java.util.HashSet;
import java.util.Set;
import javafx.scene.image.Image;
import gitsvistohry.IMGS.*;

/**
 *
 * @author Ondra
 */
public class AppMyMy {

    private String name = "_-1";
    private String pathToStartFile = "_-1";
    private boolean runSeparatedApplication = false;
    private Image appMyMyImage = null;
    private AppTypeEnum appTypeEn = AppTypeEnum.U_N_K_N_O_W_N;

    public static boolean PlayingRightNow = false;

    AppMyMyNode appNode;

    //seznam her ..pro přidání skupiny je potřeba ještě přidat do Enumu (AppTypeEn)
    private static AppMyMy[][] appsMyMy = new AppMyMy[][]{
        { /*UNKNOWN*/},
        {/*GAMES*/
            new AppMyMy("Miny", "g001xOxMiny/FXMLDocument.fxml", ImageMyMy.getImage(Settings.APP_FOLDER_GAME, "mine.png")),
//            new AppMyMy("Pacman", "g002xOxPacman.Main"),
//            new AppMyMy("Sudoku", "g003xOxSudoku.Main",true),
            new AppMyMy("Wave Master", "g004xOxWaveMaster.Main",true,ImageMyMy.getImage(Settings.APP_FOLDER_GAME,"g004xOxWaveMaster","waweLogo.png"))},
        {/*KRAVINY*/
            new AppMyMy("Binary Clock", "k001xOxBinaryClock/FXMLDocument.fxml", ImageMyMy.getImage(Settings.APP_FOLDER_KRAVINA, "binary.png")),
            new AppMyMy("Alkohol", "k002xOxAlkohol.Main", true, ImageMyMy.getImage(Settings.APP_FOLDER_KRAVINA, "alkohol.png")),
            new AppMyMy("Chlebárna", "k003xOxChlebarna.Chlebarna", true, ImageMyMy.getImage(Settings.APP_FOLDER_KRAVINA, "chleba.png")), //            new AppMyMy("Stínohra", "k004xOxStinoHra.Main", true,ImageMyMy.getImage(Settings.APP_FOLDER_KRAVINA, "stinohra.png")),
        },
        {/*SKOLA*/
            new AppMyMy("Titulky", "s002xOxTitulky.Kaspi", true, ImageMyMy.getImage(Settings.APP_FOLDER_SKOLA, "titulky.png")),
            new AppMyMy("PM3 Úloha 4", "s003xOxPm3Uloha4.PM3_Uloha4", true, ImageMyMy.getImage(Settings.APP_FOLDER_SKOLA, "pyramida.png")),
            new AppMyMy("Barvičky", "s004xOxBarvicky.Main", true, ImageMyMy.getImage(Settings.APP_FOLDER_SKOLA, "barvicky.png")),
            new AppMyMy("Maturita - prakt. (Cmyk, RGB)", "s005xOxCmykRgb.Main", true, ImageMyMy.getImage(Settings.APP_FOLDER_SKOLA, "barvicky.png")),},
        {/*PRO DVA*/
            new AppMyMy("Párty Svišťo had", "p001xOxHad.Main", true, ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "speedBoost.png"))
        }
    };

    static {
        setAppsEnums();
        soutAllApps();
        CheckDuplicityAppsMyMy();
    }

    private static void setAppsEnums() {
        //nastavit hrám jejich typ (enum)
        for (int i = 0; i < AppTypeEnum.values().length; i++) {
            for (AppMyMy app : appsMyMy[i]) {
                app.setAppTypeEn(AppTypeEnum.getEnByOrdinal(i));
                System.out.println("appka " + app + " byla nastavena na enum: " + app.getAppTypeEn().name());
            }
        }
    }

    /**
     *
     * @return true when there is NO duplicity
     */
    private static boolean CheckDuplicityAppsMyMy() {
        Set<String> gameNameSet = new HashSet<>();
        Set<String> gamePathSet = new HashSet<>();
        boolean found = false;
        for (AppMyMy[] appsField : appsMyMy) {
            for (AppMyMy game : appsField) {
                if (!gameNameSet.add(game.name)) {
                    System.err.println("CheckDuplicityGames: Duplicity FOUND! (game.name) : " + game.name);
                    found = true;
                }
                if (!gamePathSet.add(game.pathToStartFile)) {
                    System.err.println("CheckDuplicityGames: Duplicity FOUND! (game.pathToStartFile) : " + game.pathToStartFile);
                    found = true;
                }
            }
        }
        if (!found) {
            System.out.println("\nNo duplicity found in the list of games.");
        }
        return !found;
    }

    /**
     * zkusí zjistit autora z názvu hry, vyjde jen pokud je zachovaná syntaxe (třeba Ondra
     * -> (nazevHry_O_cislohry))
     *
     * @return Autor
     */
    public Autor getAutor() {
        char nameChar = 'Đ';
        for (char actualChar : pathToStartFile.toCharArray()) {
            if (nameChar != 'Đ') {
                nameChar = actualChar;
                break;
            }
            if (actualChar == Settings.GAME_PACKAGE_NAME_SEPARATOR) {
                nameChar = actualChar;
            }
        }
        if (nameChar != 'Đ') {
            return Autor.getAutor(nameChar);
        } else {
            DevFunctions.SOUT_myOutput_nepodstatne("Autor nenalezen, asi chyba v syntaxi, chce to asi 'x'.", true);
            return Autor.getUnknownAutor();
        }
    }

    public static void soutAllApps() {
        System.out.println("---------\n\n ALL GAMES: \n");
        for (AppMyMy[] appsField : appsMyMy) {
            for (AppMyMy game : appsField) {
                System.out.println(game);
            }
        }
    }

    /**
     * ukončí hru a vrátí se do launcheru.. nebo.. mělo by ..
     */
    public static void EndTheApp() {
        System.out.println("ending game");
        PlayingRightNow = false;
        Settings.mainStage.setScene(Settings.Originalscene);
        Settings.mainStage.show();
        Settings.mainStage.setTitle(Settings.STAGE_TITTLE);
        Settings.mainStage.getIcons().clear();
        Settings.mainStage.getIcons().add(Settings.LAUNCHER_IMAGE_ICON);
        System.out.println("game ended\n");
    }

    public static String getPathInFolder(String name) throws Exception {
        for (AppMyMy[] appsField : appsMyMy) {
            for (AppMyMy app : appsField) {
                if (app.name.equals(name)) {
                    return app.pathToStartFile;
                }
            }
        }
        throw new Exception("getPath: hra nenalezena");
    }

    public static String getAppName(String path) throws Exception {
        for (AppMyMy[] appsField : appsMyMy) {
            for (AppMyMy app : appsField) {
                if (app.pathToStartFile.equals(path)) {
                    return app.name;
                }
            }
        }
        throw new Exception("getGameName: hra nenalezena");
    }

    public static AppMyMy getApp_byName(String name) throws Exception {
        for (AppMyMy[] appsField : appsMyMy) {
            for (AppMyMy app : appsField) {
                if (app.name.equals(name)) {
                    return app;
                }
            }
        }
        throw new Exception("getGame_byName: hra nenalezena");
    }

    public static AppMyMy getApp_byPath(String path) throws Exception {
        for (AppMyMy[] appsField : appsMyMy) {
            for (AppMyMy app : appsField) {
                if (app.pathToStartFile.equals(path)) {
                    return app;
                }
            }
        }
        throw new Exception("getGame_byPath: hra nenalezena");
    }

    public static String getFolderByEnum(String name) throws Exception {
        for (AppMyMy[] appsField : appsMyMy) {
            for (AppMyMy app : appsField) {
                if (app.name.equals(name)) {
                    return app.appTypeEn.getFolderName();
                }
            }
        }
        throw new Exception("getPath: hra nenalezena");
    }

    public String getName() {
        return name;
    }

    public static AppMyMy[][] getAppsMyMy() {
        return appsMyMy;
    }

    public static void setAppsMyMy(AppMyMy[][] appsMyMy) {
        AppMyMy.appsMyMy = appsMyMy;
    }

    public boolean isRunSeparatedApplication() {
        return runSeparatedApplication;
    }

//    public static String[][] getGameInString() {
//        return gamesInString;
//    }
    public String getJavaClassName() {
        return pathToStartFile;
    }

    public Image getAppImage() {
        return appMyMyImage;
    }

    public void setAppImage(Image appImage) {
        this.appMyMyImage = appImage;
    }

    public AppMyMyNode getAppNode() {
        return appNode;
    }

    public AppTypeEnum getAppTypeEn() {
        return appTypeEn;
    }

    public void setAppTypeEn(AppTypeEnum appTypeEn) {
        this.appTypeEn = appTypeEn;
    }

    @Override
    public String toString() {
        return "AppMyMy{" + "name=" + name + ", pathToStartFile="
                + pathToStartFile + ", runSeparatedApplication=" + runSeparatedApplication
                + ", appTypeEn=" + appTypeEn + ", getAutor: " + getAutor() + '}';
    }

    public AppMyMy(String name, String startFileName) {
        this(name, startFileName, false);
    }

    public AppMyMy(String name, String startFileName, boolean runSeparatedApplication) {
        this(name, startFileName, runSeparatedApplication, ImageMyMy.getImage("notFound.png"));
    }

    public AppMyMy(String name, String startFileName, Image image) {
        this(name, startFileName, false, image);
    }

    public AppMyMy(String name, String startFileName, boolean runSeparatedApplication, Image image) {
        this.name = name;
        this.pathToStartFile = startFileName;
        this.runSeparatedApplication = runSeparatedApplication;
        this.appMyMyImage = image;

        appNode = new AppMyMyNode(this);
    }

}
