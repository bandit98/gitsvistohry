/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry;

import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import gitsvistohry.IMGS.*;

/**
 *
 * @author Ondra
 */
public final class Settings {
    public static Stage mainStage;  //window
    public static Scene Originalscene;  //scéna ke které se vrátíme po ukončení hry.
    
    //-------------------- (^statické věcičky který sou potřeba^) (↓ nastavení ; konstanty ↓)-------------------
    
    public static final String STAGE_TITTLE="Launcher SvišťoHry"; 
    public static final String PROJECT_FOLDER="gitsvistohry";
    public static final String SYSTEM_FILE_SEPARATOR=System.getProperty("file.separator");
    public static final Image LAUNCHER_IMAGE_ICON = ImageMyMy.getImage("launcherImg.png");
    public static final char GAME_PACKAGE_NAME_SEPARATOR='x';
    
    public static final String APP_FOLDER_GAME = "APPS/hry";
    public static final String APP_FOLDER_KRAVINA = "APPS/kraviny";
    public static final String APP_FOLDER_SKOLA = "APPS/skola";
    public static final String APP_FOLDER_PRODVA = "APPS/hryProDva";

    //privátní konstruktor, ať nejdou dělat instance 
    private Settings() {
    }
}
