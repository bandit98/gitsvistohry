/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry;

/**
 *
 * @author Ondra
 */
public class Autor {

    private String name = "_-1";
    private char nameChar = 'Đ';

    private static final Autor[] autors = new Autor[]{
        new Autor("UNKNOWN", 'Đ'),
        new Autor("Petr Skála", 'P'),
        new Autor("Petr Tržil", 'T'),
        new Autor("Anetka Dufková & Svištěnka", 'A'),
        new Autor("Ondra", 'O')
    };

    public static Autor[] getAutors() {
        return autors;
    }

    public static Autor getAutor(char nameChar) {
        for (Autor actualAutor : autors) {
            if (actualAutor.getNameChar() == nameChar) {
                return actualAutor;
            }
        }
        DevFunctions.SOUT_myOutput_nepodstatne("Asi se vyskytla chyba, hledáš autora podle Charu, ale tenhle je neznámý.", true);
        return autors[0]; //nenalezeno, takže vrátí neznámého autora.. ale spíš je špatně syntaxe u hry
    }

    public Autor(String name, char nameChar) {
        this.name = name;
        this.nameChar = nameChar;
    }

    public static Autor getUnknownAutor() {
        return autors[0];
    }

    public String getName() {
        return name;
    }

    public char getNameChar() {
        return nameChar;
    }

    @Override
    public String toString() {
        return "Autor{" + "name=" + name + ", nameChar=" + nameChar + '}';
    }

}
