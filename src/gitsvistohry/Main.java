/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import gitsvistohry.IMGS.*;
import java.net.URL;

/**
 *
 * @author Ondra
 */
public class Main extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument_launcher.fxml"));

        Scene scene = new Scene(root);

        useMyStyleSheet(scene);

        stage.setScene(scene);
        Settings.mainStage = stage;
        stage.getIcons().add(Settings.LAUNCHER_IMAGE_ICON);
        stage.setTitle(Settings.STAGE_TITTLE);
        stage.show();
    }


    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    
    private void useMyStyleSheet(Scene scene) {
        URL url = this.getClass().getResource("stylMyMy.css");
        if (url == null) {
            System.out.println("Resource not found. Aborting.");
            System.exit(-1);
        }
        String css = url.toExternalForm();
        scene.getStylesheets().add(css);
    }
}
