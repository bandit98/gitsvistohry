package gitsvistohry.APPS.skola.s001xAxBacktrack;
//autorka : Anetka - Svištěnka
public class KrizBezOpakovani {

    static int[] kriz = new int[5];
    static int level;
    static int pocitadlo;
    static boolean[] jetam = new boolean[9];

    public static void backtrack() {
        level++;
        for (int i = 1; i <= 8; i++) { //generuje kandidaty
            if (!jetam[i]) {
                jetam[i] = true; //zabookuj ho
                kriz[level] = i;
                if (testuj()) {
                    backtrack();
                }
                jetam[i] = false; //odbookuj ho
            }
        }
        level--;
    }

    public static boolean testuj() {
        switch (level) {
            case 0:
                return true;
            case 1:
                return kriz[1] - kriz[0] >= 2;
            case 2:
                return kriz[0] - kriz[2] >= 2;
            case 3:
                return kriz[0] - kriz[3] >= 2;
            case 4: {
                if (kriz[0] - kriz[4] >= 2) {
                    pocitadlo++;
                    System.out.print("  " + kriz[2] + "  \n" + kriz[1] + " " + kriz[0] + " " + kriz[3]
                            + "\n  " + kriz[4] + "  \n\n");
                }
            }
        }

        return false;
    }

    public static void main(String[] args) {
        level = -1;
        backtrack();
        System.out.println("Pocet: " + pocitadlo);
    }

    static {
        // co se provede jeste pred spustenim main
        for (int i = 1; i <= 8; i++) {
            jetam[i] = false;
        }
    }
}
