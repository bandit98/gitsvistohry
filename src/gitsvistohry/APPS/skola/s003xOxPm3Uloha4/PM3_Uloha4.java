/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.skola.s003xOxPm3Uloha4;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 *
 * @author Ondra
 */
public class PM3_Uloha4 extends Application {
    
    AnchorPane rootAnchor;
    AnchorPane pyramidAnchor= new AnchorPane();
    TextField textField;
    Label label;
    Button button;
    Stage primaryStage;
    
    private int _rectwidth;
    private int _recheigh;
    private final int windowwidth=1200;
    private final int windowheigh=700;
    
    private int pyramidHeigh;
    private int blocksneeded;
    
    @Override
    public void start(Stage primaryStage) {
        this.primaryStage=primaryStage;
        StackPane root =initt();
        root.setLayoutX(0);
        root.setLayoutY(0);
        
        Scene scene= new Scene(root,windowwidth , windowheigh);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
        
        button.requestFocus();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    /**
     * vypočítá výšku jednoho bloku tak aby bloky vyplnily okno
     */
    private void ComputeRectHeight(){
        int i=(int)primaryStage.getHeight();
        _recheigh=((int)primaryStage.getHeight()-190)/pyramidHeigh;
    }
    
    private void ComputeRectWidth(){
        _rectwidth=_recheigh*2;
    }

    private StackPane initt() {
         StackPane root=new StackPane();
         rootAnchor=new AnchorPane();
         root.getChildren().add(rootAnchor);
         rootAnchor.getChildren().add(pyramidAnchor);
         pyramidAnchor.setPrefSize(windowwidth, windowheigh-200);
         AnchorPane.setTopAnchor(pyramidAnchor, (double)100);
         AnchorPane.setLeftAnchor(pyramidAnchor, (double)0);
         
         pyramidAnchor.setLayoutY(100);
         AnchorPane.setLeftAnchor(rootAnchor, (double)0);
         AnchorPane.setTopAnchor(rootAnchor, (double)0);
         //vytvoření textu pro zadání výsky pyramidy
         
         
         //vytvoření tlačítka pro zahájení výpočtu
         button = new Button();
         button.setOnAction(new EventHandler<ActionEvent>() {
             @Override
             public void handle(ActionEvent t) {
                 TryToCompute();
             }
         });
         button.setLayoutY(30);
         button.setLayoutX(10);
         button.setPrefWidth(90);
         button.setText("Compute.");
         
         
         
         textField = new TextField();
         textField.setPrefWidth(button.getPrefWidth());
         textField.setLayoutX(button.getLayoutX());
         textField.setPromptText("Výška");
         
         label= new Label();
         label.setLayoutX(textField.getLayoutX()+textField.getPrefWidth()+10);
         label.setText("");
         
         rootAnchor.getChildren().addAll(textField,button,label);
         
         
         return root;
    }
    
    /** Spočítá kolik kvádrů je potřeba pro vytvoření 2D pyramidy  * */
    private void CompiteAmoutOfBlocksNeeded() {
        //vynulování před sčítáním
        blocksneeded=0;
        for (int i = 0; i <= pyramidHeigh; i++) {
            blocksneeded+=i;
        }
    }
    
    
    //pokusí se spočítat počet potřebných kvádrů a vykreslit 2D pyramidu
    private void TryToCompute() {
        try{
            pyramidHeigh=Integer.valueOf(textField.getText());
            if(pyramidHeigh<1 || pyramidHeigh>500){throw new Exception("Je potřeba zadat číslo 0-500");}
        }catch(Exception e){
            System.err.println(e.getMessage());
            //zbarví textField na červeno
            textField.setStyle("-fx-text-box-border: red ;  -fx-focus-color: red ;");
            return;
        }
        textField.setStyle("");
        //Spočítá kolik kvádrů je potřeba pro vytvoření 2D pyramidy
        CompiteAmoutOfBlocksNeeded();
        DrawPyramid();
        //pro kontrolu
        label.setText("Je potřeba "+blocksneeded+" kvádrů.");
    }

    private void DrawPyramid() {
        //vyčištění panelu před vykreslováním další pyramidy
        pyramidAnchor.getChildren().clear();
        
        //přepočítá výšku z velikosti okna
        ComputeRectHeight();
        //přepočítá šířku z výšky
        ComputeRectWidth();
        
        if(_recheigh==0){
            _recheigh=1;
            _rectwidth=2;
        }
        
        //aby byla pyramida uprostřed
        int pomPlus=(windowwidth -(pyramidHeigh*_rectwidth))/2;
//        Rectangle rect;
        //přidá všechny bloky do AnchorPane
        for (int rowInt = 0; rowInt < pyramidHeigh; rowInt++) {
            for (int blockInt = 0; blockInt < pyramidHeigh-rowInt; blockInt++) {
                Rectangle rect =new Rectangle(_rectwidth,_recheigh);
                pyramidAnchor.getChildren().add(rect);
                rect.setY(pyramidAnchor.getPrefHeight()-(rowInt*_recheigh)-_recheigh);
                rect.setX(pomPlus+(blockInt*_rectwidth+(rowInt*_rectwidth/2)));
                rect.setFill(Color.BLUE);
                rect.setStroke(Color.BLACK);
            }
        }
    }
    
    
    
}
