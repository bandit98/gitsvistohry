/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.skola.s005xOxCmykRgb;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
//import javafx.scene.paint.Color; tranzist bd234
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Ondra
 */
public class FXMLController implements Initializable {

    @FXML
    private Slider C;
    @FXML
    private Slider M;
    @FXML
    private Slider Y;
    @FXML
    private Slider K;
    @FXML
    private Slider R;
    @FXML
    private Slider G;
    @FXML
    private Slider B;
    @FXML
    private Label Clab;
    @FXML
    private Label Mlab;
    @FXML
    private Label Ylab;
    @FXML
    private Label Klab;
    @FXML
    private Label Rl1;
    @FXML
    private Label Gl1;
    @FXML
    private Label Bl1;
    @FXML
    private Label Rl2;
    @FXML
    private Label Gl2;
    @FXML
    private Label Bl2;
    @FXML
    private Rectangle outputColorRect;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        R.setOnMouseDragged((MouseEvent event) -> {
            recomputeByRGB();
        });
        G.setOnMouseDragged((MouseEvent event) -> {
            recomputeByRGB();
        });
        B.setOnMouseDragged((MouseEvent event) -> {
            recomputeByRGB();
        });

        C.setOnMouseDragged((MouseEvent event) -> {
            recomputeByCMYK();
        });
        M.setOnMouseDragged((MouseEvent event) -> {
            recomputeByCMYK();
        });
        Y.setOnMouseDragged((MouseEvent event) -> {
            recomputeByCMYK();
        });
        K.setOnMouseDragged((MouseEvent event) -> {
            recomputeByCMYK();
        });
    }

    public void recomputeByRGB() {
        Color clr = new Color(R.getValue(), G.getValue(), B.getValue(), 1);
        outputColorRect.setFill(clr);
        K.setValue((1 - Math.max(Math.max(R.getValue(), G.getValue()), B.getValue())));
        C.setValue(((1 - R.getValue() - K.getValue()) / (1 - K.getValue())));
        M.setValue(((1 - G.getValue() - K.getValue()) / (1 - K.getValue())));
        Y.setValue(((1 - B.getValue() - K.getValue()) / (1 - K.getValue())));
        setTextLabels();
    }

    public void recomputeByCMYK() {
        R.setValue((1 - C.getValue()) * (1 - K.getValue()));
        G.setValue((1 - M.getValue()) * (1 - K.getValue()));
        B.setValue((1 - Y.getValue()) * (1 - K.getValue()));
        Color clr = new Color(R.getValue(), G.getValue(), B.getValue(), 1);
        outputColorRect.setFill(clr);
        setTextLabels();
    }

    protected void setTextLabels() {
        Rl1.setText(("" + R.getValue()).substring(0, 3));
        Gl1.setText(("" + G.getValue()).substring(0, 3));
        Bl1.setText(("" + B.getValue()).substring(0, 3));

        Rl2.setText("" + ((int) (R.getValue() * 255)));
        Gl2.setText("" + ((int) (G.getValue() * 255)));
        Bl2.setText("" + ((int) (B.getValue() * 255)));

        Clab.setText("" + (int) (C.getValue() * 100));
        Mlab.setText("" + (int) (M.getValue() * 100));
        Ylab.setText("" + (int) (Y.getValue() * 100));
        Klab.setText("" + (int) (K.getValue() * 100));
    }
//    R = 255 × (1-C) × (1-K)
//
//The green color (G) is calculated from the magenta (M) and black (K) colors:
//
//G = 255 × (1-M) × (1-K)
//
//The blue color (B) is calculated from the yellow (Y) and black (K) colors:
//
//B = 255 × (1-Y) × (1-K)

//    K = 1-max(R', G', B')
//
//The cyan color (C) is calculated from the red (R') and black (K) colors:
//
//C = (1-R'-K) / (1-K)
//
//The magenta color (M) is calculated from the green (G') and black (K) colors:
//
//M = (1-G'-K) / (1-K)
//
//The yellow color (Y) is calculated from the blue (B') and black (K) colors:
//
//Y = (1-B'-K) / (1-K)
}
