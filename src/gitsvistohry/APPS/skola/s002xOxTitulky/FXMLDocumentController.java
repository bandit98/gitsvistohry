package gitsvistohry.APPS.skola.s002xOxTitulky;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ResourceBundle;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 *
 * @author Ondra
 */
public class FXMLDocumentController implements Initializable {
//    private  String nazevSouboru = "C:\\Users\\Ondra\\Documents\\NetBeansProjects\\SkolaKasUz20Minut\\src\\skolakasuz20minut\\titulky.sub";
//    private String nazevSouboru = "titulky.sub";

    private String nazevSouboru = null;

//    FileInputStream fis = (FileInputStream) FXMLDocumentController.class.getResourceAsStream("");
    FileInputStream fileIs = getFileInStr();

    //nee
//    String nazevSouboru = ;
//    File fl = new FileInputStream()
    private final int zpozdeniVMilisekundach = 25;//přidá další zpoždění v milisekundách. ---> VĚTŠÍ NEŽ 0!!... :D <----

    int frame = 0;
    AnimationTimer loop;

    @FXML
    private Label lblFrameID;
    @FXML
    private Label lblLyricsID;
    @FXML
    private Button btnStartID;
    @FXML
    private AnchorPane paneID;
    @FXML
    private Button btnChooseFileID;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        paneID.setStyle("-fx-background-color: #363636");
        lblFrameID.setStyle("-fx-text-fill: #edfffd");
        lblLyricsID.setStyle("-fx-text-fill: #28ffe9");

        lblLyricsID.setWrapText(true);  //label s textem na více řádků
    }

    public FileInputStream getFileInStr() {
        try {
            Path temp = Files.createTempFile("titulky-", ".sub");
            Files.copy(FXMLDocumentController.class.getResourceAsStream("titulky.sub"), temp, StandardCopyOption.REPLACE_EXISTING);
            FileInputStream inStream = new FileInputStream(temp.toFile());

            return inStream;
        } catch (Exception ex) {
            System.err.println(ex);
        }
        return null;
    }

    @FXML
    private void startButtonAction(ActionEvent event) {
        btnStartID.setDisable(true);
        btnChooseFileID.setDisable(true);
        startAnimation();
    }

    Scanner in = null;

    private void startAnimation() {
        try {
            loop.stop();
            System.out.println("__animation stopped__");
        } catch (Exception e) {
        }

        try {
            //Cp1250 protože to nen iv UNICODE ale v nějákým wtf kódování zase..
            if(nazevSouboru!=null){
            in = new Scanner(new InputStreamReader(new FileInputStream(nazevSouboru), "Cp1250"));
            System.out.println("Čtu zvolený soubor: " + nazevSouboru);
            }else{
                in=new Scanner (new InputStreamReader(fileIs, "Cp1250"));
                System.out.println("Čtu defaultní soubor, žádný nebyl uživatelem zvolen.");
            }
            
        } catch (Exception e) {
            System.err.println("Soubor se nepodařilo načíst. [" + e.getMessage() + "]");
            return;
        }

        loop = new AnimationTimer() {

            String line; //celý načtený řádek
            String[] lineParts;//rozkouskovaná řádek na 1 číslo, 2. číslo, text

            int startFrame = 0;   //startovací frame posledně načteného titulku.
            int endFrame = 0; //poslední frame posledně načteného titulku

            @Override
            public void handle(long now) {
                //načtu další řádek
                if (frame == endFrame) {
                    if (in.hasNext()) { //naprosto brutální kousek kódu.. teď sem vám názorně ukázal.. jak se to dělat NEMÁ.. :D 
                        line = in.nextLine();
                        lineParts = line.split("}");
                        lineParts[0] = lineParts[0].substring(1);
                        lineParts[1] = lineParts[1].substring(1);
                        startFrame = Integer.valueOf(lineParts[0]);
                        endFrame = Integer.valueOf(lineParts[1]);
                        lblLyricsID.setText(" ");
                    } else {
                        startFrame = 0;
                        endFrame = 0;
                        frame = 0;
                        loop.stop();
                    }
                }

                if (frame == startFrame) {
                    lblLyricsID.setText(lineParts[2]);
                }

                lblFrameID.setText("" + frame);
                frame++;

                try {
                    Thread.sleep(zpozdeniVMilisekundach);
                } catch (InterruptedException ex) {
                    System.err.println(ex);
                }
            }
        };

        loop.start();
        System.out.println("__Animation started__");

    }

    @FXML
    private void btnChooseIdAction(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        try {
            File file = fileChooser.showOpenDialog(new Stage());
            if (file != null && file.getName().endsWith(".sub")) {
                nazevSouboru = file.getAbsolutePath();
                System.out.println("Soubor připraven ke čtení : " + nazevSouboru);
            } else {
                System.err.println("někde.. bude chyba kámo.. pokoušíš se otevřít : " + file.getPath());
                System.out.println("aktuální soubor k otvírání zůstává: " + nazevSouboru);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }

}
