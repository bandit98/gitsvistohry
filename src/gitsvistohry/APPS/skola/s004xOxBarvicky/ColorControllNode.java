/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.skola.s004xOxBarvicky;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.control.Slider;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Ondra
 */
public class ColorControllNode extends Pane {

    final DoubleProperty redProperty = new SimpleDoubleProperty(0);
    final DoubleProperty greenProperty = new SimpleDoubleProperty(0);
    final DoubleProperty blueProperty = new SimpleDoubleProperty(0);

    private canListenToChanges mymyParent;
    private Rectangle rect;

    ChangeListener clis = new ChangeListener() {
        @Override
        public void changed(ObservableValue observable, Object oldValue, Object newValue) {
            somethingChanged();
            mymyParent.somethingChanged();
        }
    };

    private final int _topSpace = 50;

    public ColorControllNode(canListenToChanges mymyParent) {
        this.setStyle("-fx-background-color:#eaeaea;");
        this.mymyParent = mymyParent;

        rect = new Rectangle(10, 10, 130, _topSpace - 13);
        this.getChildren().add(rect);

        createAndBindSliders();
    }

    private void createAndBindSliders() {
        Slider redSli = new Slider(0, 1, 0);
        Slider greenSli = new Slider(0, 1, 0);
        Slider blueSli = new Slider(0, 1, 0);
        
        redSli.setStyle("-fx-background-color:#aa0000;");
        greenSli.setStyle("-fx-background-color:#00aa00;");
        blueSli.setStyle("-fx-background-color:#0000aa;");

        redSli.setLayoutX(10);
        redSli.setLayoutY(10 + _topSpace);

        greenSli.setLayoutX(10);
        greenSli.setLayoutY(30 + _topSpace);

        blueSli.setLayoutX(10);
        blueSli.setLayoutY(50 + _topSpace);

        redProperty.bindBidirectional(redSli.valueProperty());
        greenProperty.bindBidirectional(greenSli.valueProperty());
        blueProperty.bindBidirectional(blueSli.valueProperty());

        redSli.valueProperty().addListener(clis);
        greenSli.valueProperty().addListener(clis);
        blueSli.valueProperty().addListener(clis);

        this.getChildren().add(redSli);
        this.getChildren().add(greenSli);
        this.getChildren().add(blueSli);

        redSli.setBlockIncrement(0.00125);
        greenSli.setBlockIncrement(0.00125);
        blueSli.setBlockIncrement(0.00125);
    }

    public void somethingChanged() {
        rect.setFill(new Color(redProperty.get(), greenProperty.get(), blueProperty.get(), 1));
    }
}
