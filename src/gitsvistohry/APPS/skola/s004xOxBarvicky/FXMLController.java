/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.skola.s004xOxBarvicky;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;

/**
 *
 * @author Ondra
 */
public class FXMLController implements Initializable, canListenToChanges {

    @FXML
    private TilePane pane_mainControllTilePane;
    @FXML
    private AnchorPane mainPaintPane;
    @FXML
    private Canvas paintCanvas;
    @FXML
    private ScrollPane controllScrollPane;
    GraphicsContext gc;

    List<ColorControllNode> ccnList = new ArrayList<>();

    final ChangeListener clis = (ChangeListener) (ObservableValue observable, Object oldValue, Object newValue) -> {
        somethingChanged();
    };

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mainPaintPane.widthProperty().addListener(clis);
        mainPaintPane.heightProperty().addListener(clis);
        gc = paintCanvas.getGraphicsContext2D();

        pane_mainControllTilePane.prefWidthProperty().bind((controllScrollPane).widthProperty());
        pane_mainControllTilePane.prefHeightProperty().bind((controllScrollPane).heightProperty());
    }

    @FXML
    private void btnAddColorAction(ActionEvent event) {
        ColorControllNode ccn = new ColorControllNode(this);
        pane_mainControllTilePane.getChildren().add(ccn);
        ccnList.add(ccn);
        somethingChanged();
    }

    @Override
    public void somethingChanged() {
        paintToCanvas();

        paintCanvas.widthProperty().bind(mainPaintPane.widthProperty().subtract(28));
        paintCanvas.heightProperty().bind(mainPaintPane.heightProperty().subtract(28));
    }

    private void paintToCanvas() {
        double width = (int) paintCanvas.getWidth();

        gc.setFill(Color.BLACK);
        gc.clearRect(0, 0, paintCanvas.getWidth(), paintCanvas.getHeight());
        gc.fillRect(0, 0, paintCanvas.getWidth(), paintCanvas.getHeight());

        for (int actualCanvasLine = 0; actualCanvasLine < width; actualCanvasLine++) {
            int kolikatyCcn = 0;
            for (ColorControllNode actualCcn : ccnList) {
                double opacity = 0;
                double oneColorWidth = paintCanvas.getWidth() / ccnList.size();//šířka jedný barvy
                double colorMaxCenter = oneColorWidth * kolikatyCcn + (oneColorWidth / 2);
                double distanceFromMaxCenter = Math.abs(actualCanvasLine - colorMaxCenter);

                if (distanceFromMaxCenter < oneColorWidth) {//takže tu ta barva úbec má smysl
                    opacity = 1 - (distanceFromMaxCenter / (oneColorWidth));

                    gc.setFill(Color.color(actualCcn.redProperty.get(), actualCcn.greenProperty.get(), actualCcn.blueProperty.get(),
                            opacity));
                    gc.fillRect(actualCanvasLine, 0, 1, paintCanvas.getHeight());
                }

                kolikatyCcn++;
            }
        }
    }

    @FXML
    private void deleteLastBtnAction(ActionEvent event) {
        if (!ccnList.isEmpty()) {
            ColorControllNode ccnToDel = ccnList.get(ccnList.size() - 1);
            ccnList.remove(ccnToDel);
            pane_mainControllTilePane.getChildren().remove(ccnToDel);
            somethingChanged();
        }
    }

}
