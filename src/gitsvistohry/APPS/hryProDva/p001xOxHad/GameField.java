/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import java.util.*;
import javafx.scene.paint.Color;

/**
 *
 * @author Ondra
 */
public class GameField {

    private final List<Color> listOfColors = new ArrayList<>(Arrays.asList(
            new Color[]{Color.DARKRED, Color.BLUE, Color.DARKGREEN, Color.DARKORCHID, Color.CHOCOLATE, Color.DARKGREY}));

    private int width = 0;
    private int height = 0;
    private int numberOfSnakes = 0;
    private boolean printInnerWall = true;
    private PolickoEnum outerWall = PolickoEnum.TELEPORTWALL;
    private boolean printOuterWall = true;
    private PolickoEnum innerWall = PolickoEnum.WALL;

    private final int speedSetting;
    private final int hungerTypeSetting;
    private final MapEnum mapEnum;

    private Policko[][] polePolicekProRenderovani;
    private Snake[] snakes;

    private List<MyPaintable> listOfChildren = new ArrayList<>();
    Random rnd = new Random();
    
    public GameField(int width, int height, int numberOfSnakes, int speedSetting, int hungerTypeSetting,MapEnum mapEnum) {
        this.width = width;
        this.height = height;
        this.numberOfSnakes = numberOfSnakes;
        this.speedSetting = speedSetting;
        this.hungerTypeSetting = hungerTypeSetting;
        this.mapEnum=mapEnum==null ? MapEnum.DEFAULT_MAP_ENUM : mapEnum;

        snakes = new Snake[numberOfSnakes];

        polePolicekProRenderovani = new Policko[width][height];

        createAndAddWalls();
        createAndAddSnakes();
        createRandomCookie();
        createRandomBonus();

        System.out.println("Vytvořeno pole Políček o velikosti : " + width + " * " + height);
    }

    public GameField(int width, int height, int numberOfSnakes, int speedSetting, int hungerTypeSetting) {
        this(width, height, numberOfSnakes, speedSetting, hungerTypeSetting, MapEnum.DEFAULT_MAP_ENUM);
    }

    private void createAndAddSnakes() {
        createSnakes();
        addSnakesToChildren();
    }

    private void createSnakes() {
        int i = 0;
        for (Snake snake : snakes) {
            int x = 10 + (i * 6);
            int y = height / 2;
            snakes[i] = new Snake(x, y, this, 4, listOfColors.get(i), speedSetting, hungerTypeSetting);
            snakes[i].setMyID(i);
            System.out.println("Vytvořen had s x: " + x + ", y: " + y);
            i++;
        }
    }

    private void addSnakesToChildren() {
        listOfChildren.addAll(Arrays.asList(snakes));
    }

    private void createAndAddWalls() {
        MapEnum.buildMapMyMy(width, height, mapEnum,this, listOfChildren);
    }


    public void pullFromChildrenToPole() {
        for (int i = 0; i < width; i++) {//vynulování pole
            for (int j = 0; j < height; j++) {
                polePolicekProRenderovani[i][j] = null;
            }
        }

        for (MyPaintable child : listOfChildren) {//pro každý políčko každýho Child se do souřadnic přiřadí příslušný políčko
            for (Policko policko : child.getAllPolickos()) {
                int x = policko.getX();
                int y = policko.getY();
                if (polePolicekProRenderovani[x][y] != null) {
                    System.err.println("VÍCE POLOŽEK NA 1 POLÍČKU. X:" + x + ", Y:" + y
                            + ", " + polePolicekProRenderovani[x][y] + ", " + policko);
                }
                polePolicekProRenderovani[x][y] = policko;
            }
        }
    }

    public void doTick() {
        doAllChildrensPlaningMoves();
    }

    public void doAllChildrensPlaningMoves() {
//////////////////////////        System.out.println("Moving all children");
//        pullFromChildrenToPole();
        doChildrenMoves();
        pullFromChildrenToPole();
    }

    int[] souradnicePredchoziho;

    /**
     * @param aktualniPolicko
     * @return true if move was successful, false if u died
     */
    public boolean doPolickosMove(Policko aktualniPolicko) {
        if (aktualniPolicko.getDirection() < 0 || aktualniPolicko.getDirection() >= 5) {
            System.err.println("ŠPATNÝ SMĚR. POVOLENÉ HODNOTY JSOU 0-4 [0 = nic nedělej]");
            return true;
        }
        try {
            Snake lolSnake = (Snake) aktualniPolicko.getMyParent();
            if (!lolSnake.isDied()) {
                throw new Exception("to je schvalne.");
            }
            //sem to dojde pokud je had MRTVÝ
        } catch (Exception e) {
//            if (aktualniPolicko.getPolickoEnum() != PolickoEnum.TELEPORTWALL) {
////                System.out.println(Arrays.toString(souradnicePredchoziho));
//            }
            int possibleNewX = aktualniPolicko.getX();
            int possibleNewY = aktualniPolicko.getY();
            switch (aktualniPolicko.getDirection()) {
                case 0:
                    return true;
                case 1://north
                    possibleNewY = getResultMoveY(++possibleNewY);
                    break;
                case 2://east
                    possibleNewX = getResultMoveX(++possibleNewX);
                    break;
                case 3://soulth
                    possibleNewY = getResultMoveY(--possibleNewY);
                    break;
                case 4://west
                    possibleNewX = getResultMoveX(--possibleNewX);
                    break;
                default:
                    System.err.println("wtf....");
                    break;
            }
            switch (aktualniPolicko.getPolickoEnum()) {
                case SNAKESHEAD:
                    pullFromChildrenToPole();

                    Snake snake = (Snake) aktualniPolicko.getMyParent();

                    if (getResultMovePolicko(possibleNewX, possibleNewY) == null) {
                        souradnicePredchoziho = new int[]{aktualniPolicko.getX(), aktualniPolicko.getY()};
                        snake.setPreviousHeadDirection(snake.getHlavaHada().getDirection());
                        DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko);
                        return true;
                    }

                    //kontrola abych nepáchal sebevraždu
                    try {
                        Snake hopefullyNotPreviousSnake = (Snake) polePolicekProRenderovani[possibleNewX][possibleNewY].getMyParent();
                        if (hopefullyNotPreviousSnake == aktualniPolicko.getMyParent()
                                && polePolicekProRenderovani[possibleNewX][possibleNewY] == snake.getPrvniPartHada()) {
                            //průser, musíme obrátit direction
                            System.err.println("Někdo se pokusil o sebevraždu [ had : " + snake.getMyID() + "]...");
                            snake.getHlavaHada().setDirection(snake.getPreviousHeadDirection());
                            return doPolickosMove(aktualniPolicko);
                        } else {
                            throw new Exception("to je schvalne.");
                        }
                    } catch (Exception ex) {

                    }

                    switch (getResultMovePolicko(possibleNewX, possibleNewY).getPolickoEnum()) {
                        case COOKIE:
                            snake.growUp();
                            moveCookieToRandom((Cookie) getResultMovePolicko(possibleNewX, possibleNewY));
                            DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko);
                            return true;
                        case BONUS:
                            snake.speedUp();
                            moveBonusToRandom((Bonus) getResultMovePolicko(possibleNewX, possibleNewY));
                            //tady by se měla přidat logika bonusu jestli to budeš dělat...
                            DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko);
                            return true;
                    }

                    if (getResultMovePolicko(possibleNewX, possibleNewY).getPolickoEnum().isDeadly()) {
                        //System.err.println("SMRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRRT");
                        snake.setDied(true);
                        return false;
                    } else if (true) {
                        souradnicePredchoziho = new int[]{aktualniPolicko.getX(), aktualniPolicko.getY()};
                        DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko);
                        return true;
                    }
                    break;
                case SNAKESPART:
                    DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko, souradnicePredchoziho);
                    break;
                case SNAKESTAIL://přidá nový ocas, starý posune
                    Snake snakee = (Snake) aktualniPolicko.getMyParent();
                    if (snakee.isMamVyrust()) {
                        snakee.addPolicko(new Policko(aktualniPolicko.getX(), aktualniPolicko.getY(),
                                aktualniPolicko.getDirection(), PolickoEnum.SNAKESTAIL, snakee, this));
                        DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko, souradnicePredchoziho);
                        aktualniPolicko.setPolickoEnum(PolickoEnum.SNAKESPART);
                    } else {
                        DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko, souradnicePredchoziho);
                    }
                    souradnicePredchoziho = null;
                    break;
                default:
                    DoTheAdvancedMove(possibleNewX, possibleNewY, aktualniPolicko);
                    break;
            }
            return true;
        }
        return false;
    }

    private void DoTheAdvancedMove(int newX, int newY, Policko policko, int[] souradnicePredchoziho) {
        this.souradnicePredchoziho = new int[]{policko.getX(), policko.getY()};
        if (souradnicePredchoziho != null
                && (policko.getPolickoEnum() == PolickoEnum.SNAKESPART || policko.getPolickoEnum() == PolickoEnum.SNAKESTAIL)) {
            policko.setX(souradnicePredchoziho[0]);
            policko.setY(souradnicePredchoziho[1]);
        } else {
            policko.setX(getResultMoveX(newX));
            policko.setY(getResultMoveY(newY));
        }
    }

    private void DoTheAdvancedMove(int newX, int newY, Policko policko) {
        DoTheAdvancedMove(newX, newY, policko, null);
    }

    private Policko getResultMovePolicko(int inputX, int inputY) {
        return polePolicekProRenderovani[getResultMoveX(inputX)][getResultMoveY(inputY)];
    }

    private int getResultMoveX(int inputX) {
        return (0 == inputX ? width - 2 : (inputX % (width - 1) == 0 ? 1 : inputX % (width - 1)));
    }

    private int getResultMoveY(int inputY) {
        return (0 == inputY ? height - 2 : (inputY % (height - 1) == 0 ? 1 : inputY % (height - 1)));
    }

    public static int computeDirection(int xFrom, int yFrom, int xTo, int yTo) {
        if (yFrom < yTo) {
            return 1;
        }
        if (xFrom < xTo) {
            return 2;
        }
        if (yFrom > yTo) {
            return 3;
        }
        return 4;
    }

    public static int computeDirection(Policko polFrom, Policko polTo) {
        return computeDirection(polFrom.getX(), polFrom.getY(), polTo.getX(), polTo.getY());
    }

    private void doChildrenMoves() {
        for (MyPaintable child : listOfChildren) {
            Snake snake = null;
            try {
                snake = (Snake) child;
            } catch (Exception e) {

            }
            if (snake != null && snake.shouldITick()) {
                for (Policko policko : child.getAllPolickos()) {
                    policko.TryToMove();
                }
            }
        }
    }

    public Policko[][] getPolePolicek() {
        return polePolicekProRenderovani;
    }

    private void createRandomCookie() {
        pullFromChildrenToPole();
        Cookie cookie = new Cookie(3, 3, this);
        moveCookieToRandom(cookie);
        listOfChildren.add(cookie);
    }

    private void createRandomBonus() {
        pullFromChildrenToPole();
        Bonus bonus = new Bonus(3, 3, this);
        moveBonusToRandom(bonus);
        listOfChildren.add(bonus);
    }

    private void moveCookieToRandom(Cookie cookie) {
        int[] coordinates = getEmptyCoordinates();
        cookie.setX(coordinates[0]);
        cookie.setY(coordinates[1]);
    }

    private void moveBonusToRandom(Bonus bonus) {
        int[] coordinates = getEmptyCoordinates();
        bonus.setX(coordinates[0]);
        bonus.setY(coordinates[1]);
    }

    private int[] getEmptyCoordinates() {
        int x, y;
        do {
            x = rnd.nextInt(width - 3) + 1;
            y = rnd.nextInt(height - 3) + 1;
        } while (!isPolickoEmpty(x, y));
        return new int[]{x, y};
    }

    private boolean isPolickoEmpty(int x, int y) {
//        return true;
        return polePolicekProRenderovani[x][y] == null;
    }

    public static int getReverseDirection(int originalDirection) {
        switch (originalDirection) {
            case 0:
                return 0;
            case 1:
                return 3;
            case 2:
                return 4;
            case 3:
                return 1;
            case 4:
                return 2;
            default:
                System.err.println("ooo, wtf [reverse direction]... vracím 0...");
        }
        return 0;
    }

//    private void replaceCookie(Cookie puvodniCookie) {
////        listOfChildren.remove(puvodniCookie);
////        polePolicekProRenderovani[puvodniCookie.getX()][puvodniCookie.getY()]=null;
////        createRandomCookie();
//    }
    public Snake[] getSnakes() {
        return snakes;
    }

}
