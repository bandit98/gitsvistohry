/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

/**
 *
 * @author Ondra
 */
public class Cookie extends Policko{

    
    public Cookie(int x, int y, int direction, PolickoEnum polickoEnum, MyPaintable myParent, GameField gameField) {
        super(x, y, direction, polickoEnum, myParent, gameField);
    }
    
    public Cookie(int x, int y, GameField gameField) {
        super(x, y, 0, PolickoEnum.COOKIE, null, gameField);
    }
    
}
