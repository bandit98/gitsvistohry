/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import javafx.scene.image.Image;

/**
 *
 * @author Ondra
 */
public enum PolickoEnum {
    U_N_K_N_O_W_N(true,null),
    WALL(true,null),
    TELEPORTWALL(null), //pro případ že se při najetí na stěnu teleportuju na opačnou stranu
    SNAKESHEAD(true,null,true),
    SNAKESPART(true,null,true),
    SNAKESTAIL(true,null,true),//nepoužívám...
    COOKIE(null),
    BONUS(null);
    
    private boolean isDeadly;
    private boolean isSnakesPart;
    Image image;

    private PolickoEnum(boolean isDeadly, Image image,boolean isSnakesPart) {
        this.isDeadly = isDeadly;
        this.image = image;
        this.isSnakesPart=isSnakesPart;
    }
    
    private PolickoEnum(boolean isDeadly, Image image) {
        this(isDeadly, image, false);
    }
    
    private PolickoEnum( Image image) {
        this(false, image);
    }

    public boolean isIsSnakesPart() {
        return isSnakesPart;
    }

    public boolean isDeadly() {
        return isDeadly;
    }
    
    
}
