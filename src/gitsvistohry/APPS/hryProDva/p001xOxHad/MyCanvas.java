/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;
import javafx.scene.paint.Color;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;

/**
 *
 * @author Ondra
 */
public class MyCanvas extends Canvas {

    private static final Image[] images = new Image[]{
        ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "speedBoost.png"),//0
        ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "cookie.png"),//1
        ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "water.png"),//2
        ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "wall.png"),//3
        ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "cracked.png"),};//4

    private final GraphicsContext gc;
    private Policko[][] polePolicek;
    private final Paint MY_DEFAULT_COLOR = Color.BLACK;

    private boolean whBinded = false;

//////    private final int strokeThick=3;// X px 
    public MyCanvas(double width, double height) {
        super(width, height);
        gc = this.getGraphicsContext2D();
    }

    public void printPaintableField(Policko[][] polePolicek) {
//        double width = this.getWidth();
//        double heigh=this.getHeight();
        this.polePolicek = polePolicek;

        drawShapes(gc);
    }

    private void drawShapes(GraphicsContext gc) {
        gc.clearRect(0, 0, this.getWidth(), this.getHeight());

        for (int y = polePolicek[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < polePolicek.length; x++) {//půjdeme odzhora levo do prava, pak o řádek dolů atd..
                if (!(polePolicek[x][y] == null)) {
                    drawPolickoOnCanvas((int) (this.getWidth() / polePolicek.length) * x, (((int) this.getHeight() / (polePolicek[0].length))) * ((polePolicek[0].length - 1) - y),
                            (this.getWidth() / polePolicek.length) - 1, (this.getHeight() / (polePolicek[0].length)) - 1,
                            polePolicek[x][y]);
                }
            }
        }
    }

    private void drawPolickoOnCanvas(int canvasFieldX, int canvasFieldY, double polWidth, double polHeigh, Policko mojePolicko) {//pozor, X,Y budou jen : 0,1,2..
        if (!whBinded) {
            this.widthProperty().bind(((AnchorPane) this.getParent()).widthProperty());
            this.heightProperty().bind(((AnchorPane) this.getParent()).heightProperty());
            whBinded = true;
        }
        
        gc.setFill(MY_DEFAULT_COLOR);
        try {
            if (mojePolicko.getPolickoEnum().isIsSnakesPart()) {
                Snake actualSnake = (Snake) mojePolicko.getMyParent();
                if (mojePolicko.getPolickoEnum() == PolickoEnum.SNAKESHEAD) {
                    canvasFieldX--;
                    canvasFieldY--;
                    polWidth += 2;
                    polHeigh += 2;
                }
                gc.setFill((actualSnake).getColor());
                gc.fillRoundRect(canvasFieldX, canvasFieldY, polWidth, polHeigh, polWidth / 2, polWidth / 2);

sww:            switch (mojePolicko.getPolickoEnum()) {
                    case SNAKESPART:
                        if (actualSnake.getSpeedIterator() == actualSnake.getmoveEveryTicksFieldLength() - 1) {
                            double opacity = ((double) actualSnake.getSlowDownIn() / (double) actualSnake.getHadntEatenLimit()) * 2;
                            gc.setGlobalAlpha((opacity));
                            gc.drawImage(images[0], canvasFieldX + 2, canvasFieldY + 2, polWidth - 4, polHeigh - 4);
                            gc.setGlobalAlpha(1);
                        }
                        break;
                    case SNAKESTAIL:
                        int speedIterator = actualSnake.getSpeedIterator() == actualSnake.getSpeed_MoveEveryTicksFieldLength() - 1
                                ? actualSnake.getSpeedIterator() - 1 : actualSnake.getSpeedIterator();
                        int i = speedIterator;
//                        if(speedIterator==0){break;}
                        while (i > 0) {
                            double opacity = (double) actualSnake.getSlowDownIn() / (double) actualSnake.getHadntEatenLimit();
                            if (i == speedIterator) {
                                gc.setGlobalAlpha((opacity + 0.16));
                                if (actualSnake.getSpeedIterator() == actualSnake.getSpeed_MoveEveryTicksFieldLength() - 1) {
                                    gc.setGlobalAlpha(1);
                                }
                                gc.drawImage(images[0], canvasFieldX + ((double) (i - 1) / speedIterator) * polWidth,
                                        canvasFieldY + 1, polWidth / speedIterator, polHeigh - 2);
                                gc.setGlobalAlpha(1);
                            } else {
                                gc.drawImage(images[0], canvasFieldX + ((double) (i - 1) / speedIterator) * polWidth,
                                        canvasFieldY + 1, polWidth / speedIterator, polHeigh - 2);
                            }
                            i--;
                        }
                        break;
                }

                if (actualSnake.isDied()) {
                    gc.drawImage(images[4], canvasFieldX, canvasFieldY, polWidth, polHeigh);
                }
                return;
            }
            switch (mojePolicko.getPolickoEnum()) {
                case BONUS:
                    gc.drawImage(images[0], canvasFieldX, canvasFieldY, polWidth, polHeigh);
                    if (true) {
                        return;
                    }
                    break;
                case COOKIE:
                    gc.drawImage(images[1], canvasFieldX, canvasFieldY, polWidth, polHeigh);
                    if (true) {
                        return;
                    }
                    break;
                case TELEPORTWALL:
                    gc.drawImage(images[2], canvasFieldX, canvasFieldY, polWidth + 1, polHeigh + 1);
                    if (true) {
                        return;
                    }
                    break;
                case WALL:
                    gc.drawImage(images[3], canvasFieldX, canvasFieldY, polWidth + 1, polHeigh + 1);
                    if (true) {
                        return;
                    }
                    break;

            }
        } catch (Exception e) {

        }
        gc.fillRoundRect(canvasFieldX, canvasFieldY, polWidth, polHeigh, polWidth / 2, polWidth / 2);

//gc.fillRect(10, 50, 60, 60);
    }
}
