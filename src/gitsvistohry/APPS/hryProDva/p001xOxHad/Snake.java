/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import java.util.*;
import javafx.scene.paint.Color;

/**
 *
 * @author Ondra
 */
public class Snake implements MyPaintable {

//    String name;
    int myID;

    private Color color;

    private GameField gameField__parent;
    private List<Policko> listPolicek = new ArrayList<>();
    private boolean died;
    private int mamVyrustO = 0;

    private final int[] possibleSpeeds = new int[]{60, 25, 19, 15, 11, 8, 6, 4, 3, 2, 1}; //5 z nich se přiřadí do moveEveryTicksField podle speedSetting
    private final int[] possibleHunger = new int[]{3000, 2000, 1600, 1200, 900, 600, 400};

    private final int moveEveryTicksIteratorDEFAULT = 1;
    private final int[] moveEveryTicksField = new int[5];
    private int moveEveryTicksIterator = moveEveryTicksIteratorDEFAULT;
    private final int speedSetting;
    private final int hungerTypeSetting;

    private int ticks = 0;
    private int hadntEatenCookiesForTicks = 0;
    private int hadntEatenLimit;//XY ticků, potm ze zpomalí když do tý doby nebude jíst

    private int previousHeadDirection;

    public Snake(int startX, int startY, GameField gameField, int direction, Color color, int speedSetting, int hungerTypeSetting) {
        this.gameField__parent = gameField;
        this.color = color;
        this.speedSetting = speedSetting;
        this.hungerTypeSetting = hungerTypeSetting;
        this.hadntEatenLimit = possibleHunger[hungerTypeSetting];
        System.arraycopy(possibleSpeeds, speedSetting, moveEveryTicksField, 0, moveEveryTicksField.length);

        listPolicek.add(new Policko(startX, startY, direction, PolickoEnum.SNAKESHEAD, this, gameField__parent));
        listPolicek.add(new Policko(startX + 1, startY, direction, PolickoEnum.SNAKESPART, this, gameField__parent));
        listPolicek.add(new Policko(startX + 2, startY, direction, PolickoEnum.SNAKESPART, this, gameField__parent));
        listPolicek.add(new Policko(startX + 3, startY, direction, PolickoEnum.SNAKESTAIL, this, gameField__parent));
        died = false;
    }

//    public Snake(int startX, int startY, GameField gameField) {
//        this(startX, startY, gameField, 4, Color.CHOCOLATE, 0);
//    }
    /**
     * cookie eaten.
     */
    public void growUp() {
        resetHadntEatenCookiesForTicks();
        mamVyrustO++;
    }

    public boolean isMamVyrust() {
        if (mamVyrustO > 0) {
            mamVyrustO--;
            return true;
        }
        return false;
    }

    public void addPolicko(Policko policko) {
        listPolicek.add(policko);
    }

    @Override
    public void doMove() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //-----------------------------------------------
    //-----------------------------------------------
    //-----------------------------------------------
    //-----------------------------------------------
    //-----------------------------------------------
    //-----------------------------------------------
    public boolean shouldITick() {
        doTick();
        if (++ticks == getMoveEveryTicks()) {
            ticks = 0;
            return true;
        }
        return false;
    }

    private void doTick() {
        if (moveEveryTicksIterator != 0) {
            if (hadntEatenCookiesForTicks++ >= hadntEatenLimit) {
                slowDown();
                hadntEatenCookiesForTicks = 0;
            }
        } else {
            hadntEatenCookiesForTicks = 0;
        }
    }

    public void speedUp() {
        if (this.moveEveryTicksIterator == moveEveryTicksField.length - 1) {
//            System.out.println("Nemůžu zrychlyt, vylezl bych z pole rychlostí");
        } else {
            moveEveryTicksIterator++;
            resetHadntEatenCookiesForTicks();
            System.out.println(getSpeedIterator(true));

        }
    }

    public void slowDown() {
        if (this.moveEveryTicksIterator == 0) {
//            System.out.println("Nemůžu zpomalit, vylezl bych z pole rychlostí");
        } else {
            moveEveryTicksIterator--;
            System.out.println(getSpeedIterator(false));

        }
    }

    private void resetHadntEatenCookiesForTicks() {
        if (moveEveryTicksIterator == moveEveryTicksField.length - 1) {
            hadntEatenCookiesForTicks = hadntEatenLimit / 2;
        }else{
            hadntEatenCookiesForTicks=0;
        }
    }

    public String getSpeedIterator(boolean speedUp) {
        if (speedUp) {
            return (" +Speeding up snake :" + this + " to iterator speed: " + moveEveryTicksIterator + "/" + (moveEveryTicksField.length - 1));
        } else if (true) {
            return ("    -Slowing down snake :" + this + " to iterator speed: " + moveEveryTicksIterator + "/" + (moveEveryTicksField.length - 1));
        }
        return "";
    }

    public int getSpeedIterator() {
        return moveEveryTicksIterator;
    }

    public int getSpeed_MoveEveryTicksFieldLength() {
        return moveEveryTicksField.length;
    }

    private int getMoveEveryTicks() {
        return moveEveryTicksField[moveEveryTicksIterator];
    }

    public boolean isDied() {
        return died;
    }

    public Policko getPrvniPartHada() {
        return listPolicek.get(1);
    }

    public Policko getHlavaHada() {
        return listPolicek.get(0);
    }

//////////////////////////////////////////    public String getName() {
//////////////////////////////////////////        return name;
//////////////////////////////////////////    }
//////////////////////////////////////////
//////////////////////////////////////////    public void setName(String name) {
//////////////////////////////////////////        this.name = name;
//////////////////////////////////////////    }
    public int getPreviousHeadDirection() {
        return previousHeadDirection;
    }

    public void setPreviousHeadDirection(int previousHeadDirection) {
        this.previousHeadDirection = previousHeadDirection;
    }

    public void setDied(boolean died) {
        System.out.println("SNAKE DIED [" + this.toString() + "]");
        this.died = died;
    }

    @Override
    public Policko[] getAllPolickos() {
        Policko[] tmp = new Policko[listPolicek.size()];
        for (int i = 0; i < listPolicek.size(); i++) {
            tmp[i] = listPolicek.get(i);
        }
        return tmp;
//        return (Policko[]) listPolicek.toArray();
    }

    public void changeHeadsDirection(int direction) {
        this.listPolicek.get(0).setDirection(direction);
    }

    public GameField getGameField__parent() {
        return gameField__parent;
    }

    public void setGameField__parent(GameField gameField__parent) {
        this.gameField__parent = gameField__parent;
    }

    public int getSlowDownIn() {
        return hadntEatenLimit - hadntEatenCookiesForTicks;
    }

    public int getHadntEatenCookiesForTicks() {
        return hadntEatenCookiesForTicks;
    }

    public int getHadntEatenLimit() {
        return hadntEatenLimit;
    }
    

    public List<Policko> getListPolicek() {
        return listPolicek;
    }

    public void setListPolicek(List<Policko> listPolicek) {
        this.listPolicek = listPolicek;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getSizeOfListPolicek() {
        return listPolicek.size();
    }

    public int getMyID() {
        return myID;
    }

    public void setMyID(int myID) {
        this.myID = myID;
        Player.getListOfAllPlayers().get(myID).setSnake(this);
    }
    
    public int getmoveEveryTicksFieldLength(){
        return moveEveryTicksField.length;
    }

    @Override
    public String toString() {
        return "Snake{" + ", died=" + died + ", mamVyrustO=" + mamVyrustO + '}';
    }

}
