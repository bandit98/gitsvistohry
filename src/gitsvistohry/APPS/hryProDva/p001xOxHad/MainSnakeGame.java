/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.Scene;

/**
 *
 * @author Ondra
 */
public class MainSnakeGame {

    public boolean isPAUSE = false;

    FXMLDocController fxmlCont;

    private Scene scene = Main.getScenee();
    private Model modelThread;

    private boolean running = true;
//    final int moveEveryXTics = 7;

    private int width = 0;
    private int height = 0;
    private int numberOfSnakes = 0;
    private final int speedSetting;
    private final int hungerTypeSetting;
    private final MapEnum mapEnum;

    GameField gameField;

    private long totalTickCount = 1;
    private static int fps = 0;
    private final boolean renderInCanvas; // true = canvas, false = label...

    public MainSnakeGame(int width, int height, int numberOfSnakes, boolean renderInCanvas, int speedSetting, int hungerType, MapEnum mapEnum) {
        this.width = width;
        this.height = height;
        this.numberOfSnakes = numberOfSnakes;
        this.renderInCanvas = renderInCanvas;
        this.speedSetting = speedSetting;
        this.hungerTypeSetting = hungerType;
        this.mapEnum = mapEnum;

//        gameField = new GameField(width, height,numberOfSnakes);
    }

    public void run(FXMLDocController fxmlCont) {
        this.fxmlCont = fxmlCont;
        modelThread = new Model();
        modelThread.start();
//        gameThread.setDaemon(true);
    }

    public class Model extends Thread {

        {
            this.setDaemon(true);
        }

        @Override
        public void run() {
            gameField = new GameField(width, height, numberOfSnakes, speedSetting, hungerTypeSetting, mapEnum);
            fxmlCont.installEventHandler(gameField.getSnakes());
            int frames = 0;
            double unprocessedSeconds = 0;
            long previousTime = System.nanoTime();

            double secondsForEachTick = 1 / (60.0);

            long tickCount = 0; //RENDER COUNTER
            boolean ticked = false;

            long currentTime = 0;
            long passedTime = 0;
            while (running) {
                currentTime = System.nanoTime();
                passedTime = currentTime - previousTime;

                previousTime = currentTime;

                unprocessedSeconds = unprocessedSeconds + passedTime / 1000000000.0;

                int count = 0;
                while (unprocessedSeconds > secondsForEachTick) {
                    tick();
                    count++;
                    unprocessedSeconds -= secondsForEachTick;

                    ticked = true;
                    tickCount++;
                    totalTickCount++;

                    if (tickCount % 60 == 0) {
//                            System.out.println(frames + " fps");
                        MainSnakeGame.fps = frames;
                        previousTime += 1000;
                        frames = 0;
                    }
                }
//        System.out.println("proběhlo loopů ticků za 1 while: "+count);
                if (ticked) {
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            render();
                        }
                    });
                    frames++;
                    ticked = false;
                }
                while (isPAUSE) {
                    try {
                        double satan = 666; //protože proto.
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
//                            Logger.getLogger(MainSnakeGame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                previousTime = System.nanoTime();
            }
        }
    }

    private void tick() {
        gameField.doTick();

    }

    private void render() {
        fxmlCont.getLabelFPS().setText("" + fps + " fps");

        renderMainGame();
        renderPlayerssStats();
    }

    private void renderMainGame() {
        if (renderInCanvas) {
            renderMainGameInCanvas();
        } else {
            renderMainGameInLabel();
        }
    }

    private void renderMainGameInCanvas() {
        fxmlCont.soutGamesOutputToCanvas(gameField.getPolePolicek());
    }

    private void renderMainGameInLabel() {
        //        System.out.println("\n\n-----------------------------");
        StringBuffer o_u_t_p_u_tBuffer = new StringBuffer("");
        Policko[][] tmp = gameField.getPolePolicek();
        for (int y = tmp[0].length - 1; y >= 0; y--) {
            for (int x = 0; x < tmp.length; x++) {
                if (tmp[x][y] != null) {
                    char myChar = 'Đ';

                    if (tmp[x][y].getPolickoEnum().isIsSnakesPart()) {
                        if (((Snake) tmp[x][y].getMyParent()).isDied()) {
                            myChar = 'X';
                        } else {
                            myChar = 'O';
                        }
                        if (tmp[x][y].getPolickoEnum() == PolickoEnum.SNAKESHEAD) {
                            myChar = 'Q';
                        }
                    }
                    switch (tmp[x][y].getPolickoEnum()) {
                        case TELEPORTWALL:
                            myChar = '+';
                            break;
                        case WALL:
                            myChar = 'w';
                            break;
                        case COOKIE:
                            myChar = 'C';
                            break;
                        case BONUS:
                            myChar = 'B';
                            break;
                    }
//                    System.out.print(myChar);
                    o_u_t_p_u_tBuffer.append("" + ' ' + myChar + ' ');
                } else {
                    o_u_t_p_u_tBuffer.append("" + ' ' + ", ," + ' ');
//                    System.out.print(" ");
                }
            }
            o_u_t_p_u_tBuffer.append("\n");
//            System.out.println("");
        }
//        System.out.println("\n\n-----------------------------");
        fxmlCont.soutGamesOutputToLabel(o_u_t_p_u_tBuffer.toString());
    }

    private void renderPlayerssStats() {
        fxmlCont.updateGraphics_PlayerssStats();
    }

    public void tryToExit() {
        try {
            running = false;
            modelThread.interrupt();
            System.out.println("Interrupted");
            Thread.sleep(1000);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

//    public static void main(String[] args) {
//        new MainSnakeGame(10, 40, 2).run();
//    }
}
