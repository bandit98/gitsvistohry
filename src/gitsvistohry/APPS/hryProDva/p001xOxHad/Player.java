/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 *
 * @author Ondra
 */
public final class Player extends Group {

    private static List<Player> listOfAllPlayers=new ArrayList<>();

    private Snake snake;
    private String name;
    
    private VBox vbox;
    
    private Label idLabel;
    private Label colorLabel;
    private Label isDeadOrAlive;
    private Label lengthLabel;
    
    private Label slowDownIn;//NEDODĚLÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁÁNO... asi 
    private Label speeed;//taky ne 

    public Player(Snake snake, String name) {
        this.snake = snake;
        this.name = name;
        listOfAllPlayers.add(this);
        
        idLabel=new Label(){{setStyle("-fx-font-weight: bold;-fx-font-size:20");}};
        isDeadOrAlive=new Label();
        lengthLabel=new Label();
        slowDownIn= new Label();
        speeed= new Label();
        updatee();
        
        
        vbox=new VBox();//sdsdsda ds 
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(20);
        vbox.setBorder(new Border(new BorderStroke(Color.DIMGRAY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
        this.getChildren().add(vbox);
        vbox.getChildren().addAll(idLabel,isDeadOrAlive,lengthLabel,slowDownIn,speeed);
        
    }
    
    public void updatee(){
        idLabel.setText(snake==null ? "defaulttt snake.." : ""+(snake.getMyID()+1)+". Hráč");
        isDeadOrAlive.setText(snake==null ? "defaulttt snake.." : ""+(snake.isDied() ? "MRTEV" : "žije"));
        idLabel.setTextFill(snake==null ? Color.BLACK : snake.getColor());
        lengthLabel.setText(snake==null ? "defaulttt snake len.." : "Délka hada: "+snake.getSizeOfListPolicek());
        slowDownIn.setText(snake == null? "Slow down in.def " : "Zpomalíš kvůli hladu za :"+(snake.getSlowDownIn()/100+1));
        speeed.setText(snake == null? "Speed boost level .def " : "Speed boost level :"+snake.getSpeedIterator()+"/"+(snake.getSpeed_MoveEveryTicksFieldLength()-1));
    }

    
    //-----------------------
    //-----------------------
    public static void addNewPlayer(Player... players) {
        listOfAllPlayers.addAll(Arrays.asList(players));
    }

    public static void removeAllPlayers() {
        listOfAllPlayers.clear();
    }

    //----------------------------------------------------
    //----------------------------------------------------
    //----------------------------------------------------
    //----------------------------------------------------
    public Snake getSnake() {
        return snake;
    }

    public void setSnake(Snake snake) {
        this.snake = snake;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<Player> getListOfAllPlayers() {
        return listOfAllPlayers;
    }
    
    

}
