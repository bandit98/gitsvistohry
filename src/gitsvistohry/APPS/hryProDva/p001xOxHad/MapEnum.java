/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;
import java.util.List;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Ondra
 */
public enum MapEnum {
    ONLY_TELEPORT_WALLS("Čistá mapa"),
    ALL_INNER_WALLS("Věžení", "vezeni.png"),
    ADVANCED_WALLS_1("Klasika - level 1", "level1.png"),
    ADVANCED_WALLS_2("level 2", "level2.png"),
    ADVANCED_WALLS_3("level 3", "level3.png"),
    ADVANCED_WALLS_4("level 4", "level4.png");

    String name;
    Image img;

    private static final PolickoEnum outerWall = PolickoEnum.TELEPORTWALL;
    private static final PolickoEnum hardWall = PolickoEnum.WALL;
    public static final MapEnum DEFAULT_MAP_ENUM = MapEnum.ONLY_TELEPORT_WALLS;

    private MapEnum(String name) {
        this(name, "default.png");
    }

    private MapEnum(String name, String imgName) {
        this.name = name;
        this.img = ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "maps", imgName);
    }
//ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "water.png"),//2
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getImg() {
        return img;
    }

    public ImageView getImgView() {
        ImageView imv = new ImageView(img);
        imv.setFitWidth(100);
        imv.setFitHeight(100);
        return imv;
    }

    public void setImg(Image img) {
        this.img = img;
    }

    public static void buildMapMyMy(int width, int height, MapEnum mapEnum, GameField gf, List<MyPaintable> listOfChildren) {
        if (mapEnum == null) {
            mapEnum = MapEnum.ONLY_TELEPORT_WALLS;
        }
        switch (mapEnum) {
            case ONLY_TELEPORT_WALLS:
                _buildOuterTeleportWalls(width, height, gf, listOfChildren);
                break;
            case ALL_INNER_WALLS:
                _buildOuterTeleportWalls(width, height, gf, listOfChildren);
                _buildAllInnerWalls(width, height, listOfChildren, gf);
                break;
            case ADVANCED_WALLS_1:
                _buildOuterTeleportWalls(width, height, gf, listOfChildren);
                _buildLevel1(width, height, gf, listOfChildren);
                break;
            case ADVANCED_WALLS_2:
                _buildOuterTeleportWalls(width, height, gf, listOfChildren);
                _buildLevel2(width, height, gf, listOfChildren);
                break;
            case ADVANCED_WALLS_3:
                _buildOuterTeleportWalls(width, height, gf, listOfChildren);
                _buildLevel2(width, height, gf, listOfChildren);
                _build4Rects(width, height, gf, listOfChildren, false);
                break;
            case ADVANCED_WALLS_4:
                _buildOuterTeleportWalls(width, height, gf, listOfChildren);
                _buildLevel2(width, height, gf, listOfChildren);
                _build4Rects(width, height, gf, listOfChildren, true);
                break;
            default:
                System.err.println("errr...");
                break;
        }
    }

    private static void _buildOuterTeleportWalls(int width, int height, GameField gf, List<MyPaintable> listOfChildren) {
        for (int x = 0; x < width; x++) {
            if (x == 0 || x == width - 1) {
                for (int y = 0; y < height; y++) {
                    listOfChildren.add(new Policko(x, y, 0, outerWall, null, gf));
                }
            } else {
                listOfChildren.add(new Policko(x, 0, 0, outerWall, null, gf));
                listOfChildren.add(new Policko(x, height - 1, 0, outerWall, null, gf));
            }
        }
    }

    private static void _buildAllInnerWalls(int width, int height, List<MyPaintable> listOfChildren, GameField gf) {
        for (int x = 1; x < width - 1; x++) {
            if (x == 1 || x == width - 2) {
                for (int y = 1; y < height - 1; y++) {
                    listOfChildren.add(new Policko(x, y, 0, hardWall, null, gf));
                }
            } else {
                listOfChildren.add(new Policko(x, 1, 0, hardWall, null, gf));
                listOfChildren.add(new Policko(x, height - 2, 0, hardWall, null, gf));
            }
        }
    }

    private static void _buildLevel1(int width, int height, GameField gf, List<MyPaintable> listOfChildren) {
        for (int x = 1; x < width - 1; x++) {
            if (x == (width / 5) * 2) {
                x = (width / 5) * 3 + 1;
            }
            if (x == 1 || x == width - 2) {
                for (int y = 1; y < height - 1; y++) {
                    listOfChildren.add(new Policko(x, y, 0, hardWall, null, gf));
                }
            } else {
                listOfChildren.add(new Policko(x, 1, 0, hardWall, null, gf));
                listOfChildren.add(new Policko(x, height - 2, 0, hardWall, null, gf));
            }
        }
    }

    private static void _buildLevel2(double width, double height, GameField gf, List<MyPaintable> listOfChildren) {
        for (int x = 1; x < width - 1; x++) {
            if (x == (int) (((width / 9) * 4))) {
                x = (int) ((width / 9) * 5) + 1;
            }
            if (x == 1 || x == width - 2) {
                for (int y = 1; y < height - 1; y++) {
                    if (y == (int) (((height / 9) * 4))) {
                        y = (int) ((height / 9) * 5) + 1;
                    }
                    listOfChildren.add(new Policko(x, y, 0, hardWall, null, gf));
                }
            } else {
                listOfChildren.add(new Policko(x, 1, 0, hardWall, null, gf));
                listOfChildren.add(new Policko(x, (int) height - 2, 0, hardWall, null, gf));
            }
        }
    }

    private static void _build4Rects(double width, double height, GameField gf, List<MyPaintable> listOfChildren, boolean walkable) {
        double pocetVyplnenychDilkuNaRect = 3;// takže 3/10 na eden, takže 6/10 délky bude zabírat zeď
        double pocetPofidernichDilku = 13;

        int rectWidth = (int) (width * (pocetVyplnenychDilkuNaRect / pocetPofidernichDilku));
        int rectHeigh = (int) (height * (pocetVyplnenychDilkuNaRect / pocetPofidernichDilku));

        int startX;
        int startY;

        startX = (int) (((width / 2) - rectWidth) / 2);
        startY = (int) (((height / 2) - rectHeigh) / 2);
        if (!walkable) {
            ___buildRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        } else {
            ___buildWalkableRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        }

        startX = (int) (((width - (2 * rectWidth)) / 4) + (width / 2));
        startY = (int) (((height / 2) - rectHeigh) / 2);
        if (!walkable) {
            ___buildRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        } else {
            ___buildWalkableRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        }

        startX = (int) (((width / 2) - rectWidth) / 2);
        startY = (int) (((height - (2 * rectHeigh)) / 4) + (height / 2));
        if (!walkable) {
            ___buildRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        } else {
            ___buildWalkableRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        }

        startX = (int) (((width - (2 * rectWidth)) / 4) + (width / 2));
        startY = (int) (((height - (2 * rectHeigh)) / 4) + (height / 2));
        if (!walkable) {
            ___buildRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        } else {
            ___buildWalkableRect(startX, rectWidth, startY, rectHeigh, listOfChildren, gf);
        }
    }

    private static void ___buildRect(int startX, int rectWidth, int startY, int rectHeigh, List<MyPaintable> listOfChildren, GameField gf) {
        for (int pseudoX = startX; pseudoX < rectWidth + startX; pseudoX++) {
            for (int pseudoY = startY; pseudoY < rectHeigh + startY; pseudoY++) {
                listOfChildren.add(new Policko(pseudoX, pseudoY, 0, hardWall, null, gf));
            }
        }
    }

    private static void ___buildWalkableRect(double startX, double rectWidth, double startY, double rectHeigh, List<MyPaintable> listOfChildren, GameField gf) {
        for (double pseudoX = startX; pseudoX < rectWidth + startX; pseudoX++) {
            for (double pseudoY = startY; pseudoY < rectHeigh + startY; pseudoY++) {
                if (pseudoX == (int) ((((rectWidth) / 7.0) * 3.0)) + startX) {
                    pseudoX = (int) (((rectWidth) / 7.0) * 4.0) + startX + 1;
                }
                if (pseudoY == (int) ((((rectHeigh) / 7.0) * 3.0)) + startY) {
                    pseudoY = (int) (((rectHeigh) / 7.0) * 4.0) + startY + 1;
                }
                listOfChildren.add(new Policko((int) pseudoX, (int) pseudoY, 0, hardWall, null, gf));
            }
        }
    }

}
