/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

/**
 *
 * @author Ondra
 */
public class Main extends Application {

    private static Scene scenee;
    private static Stage stageee;

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDoc.fxml"));

        Scene scene = new Scene(root);
        scenee = scene;

        stage.setScene(scene);
        stage.show();
        stageee = stage;

        final EventHandler<KeyEvent> keyEventHandler
                = new EventHandler<KeyEvent>() {
            public void handle(final KeyEvent keyEvent) {
                if (keyEvent.getCode() == KeyCode.F11) {
                    System.out.println("F11 pressed.");
                    stage.setFullScreen(!Main.getStageee().isFullScreen());
                }
            }
        };
        scene.setOnKeyPressed(keyEventHandler);
        stage.setTitle("Párty Svišťo had");
        stage.getIcons().add(ImageMyMy.getImage(Settings.APP_FOLDER_PRODVA, "p001xOxHad", "speedBoost.png"));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public static Scene getScenee() {
        return scenee;
    }

    public static Stage getStageee() {
        return stageee;
    }

}
