/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hryProDva.p001xOxHad;

/**
 *
 * @author Ondra
 */
public class Policko implements MyPaintable{
    private int x;
    private int y;
    private PolickoEnum polickoEnum;
    private int direction;  // direction = which direction to go next time (1N 2E 3S 4W)
    private MyPaintable myParent;
    GameField gameField;//pro pohyby

    public Policko(int x, int y, int direction, PolickoEnum polickoEnum,MyPaintable myParent, GameField gameField) {
        this.x = x;
        this.y = y;
        this.polickoEnum = polickoEnum;
        this.myParent=myParent;
        this.gameField=gameField;
        this.direction=direction;
        
        if(polickoEnum==null){
            System.err.println("CHYBA JE TU");
        }
//        direction=0;
    }
    
    public void TryToMove(){
        gameField.doPolickosMove(this);
    }
    
    
    
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    //-------------------------------------------------------------------------
    public void setDirection(int direction) {    
        this.direction = direction;
    }

    public int getDirection() {
        return direction;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public MyPaintable getMyParent() {
        return myParent;
    }

    public void setMyParent(MyPaintable myParent) {
        this.myParent = myParent;
    }
    

    public PolickoEnum getPolickoEnum() {
        return polickoEnum;
    }

    public void setPolickoEnum(PolickoEnum polickoEnum) {
        this.polickoEnum = polickoEnum;
    }

    @Override
    public Policko[] getAllPolickos() {
        return new Policko[]{this};
    }

    @Override
    public void doMove() {
        gameField.doPolickosMove(this);
    }

    @Override
    public String toString() {
        return "Policko{" + "x=" + x + ", y=" + y + ", polickoEnum=" + polickoEnum + ", direction=" + direction + '}';
    }
    
    
    
}
