/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g001xOxMiny;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import gitsvistohry.*;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;

/**
 *
 * @author Ondra
 */
public class FXMLDocumentController implements Initializable {

    private Label label;
    @FXML
    private Button button;
    @FXML
    private AnchorPane GamePane;
    @FXML
    private Label label_FlaggedID;
    @FXML
    private Label labelMinaCount;
    @FXML
    private Label label_WonOrLoosed;
    @FXML
    private Label lbl_UndiscoveredCount_ID;
    @FXML
    private TextField txt_minasCount;
    @FXML
    private TextField txtSizeCount;

    private GridPane gridPaneGameField = new GridPane();

    int _fieldSize = 23;
    int _minsCount = 35;
    Miny miny;

    final int maxPossibleFieldSize=45;
    @FXML
    private void handleButtonAction(ActionEvent event) {
        AppMyMy.EndTheApp();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        CreateGameField();

    }

    private void CreateGameField() {
        GamePane.getChildren().clear();
        gridPaneGameField = new GridPane();
        miny = new Miny(_fieldSize, _minsCount, labelMinaCount, label_FlaggedID,
                lbl_UndiscoveredCount_ID, label_WonOrLoosed);
        GamePane.getChildren().add(gridPaneGameField);

        gridPaneGameField.setHgap(5);
        gridPaneGameField.setVgap(5);

        AnchorPane.setTopAnchor(gridPaneGameField, 5d);
        AnchorPane.setLeftAnchor(gridPaneGameField, 5d);
        AnchorPane.setRightAnchor(gridPaneGameField, 5d);
        AnchorPane.setBottomAnchor(gridPaneGameField, 5d);

        gridPaneGameField.setCenterShape(true);

        String[][] values = miny.getFieldValues();
        for (int x = 0; x < _fieldSize; x++) {
            for (int y = 0; y < _fieldSize; y++) {
                PolickoNode labell = miny.getPolicko(x, y).getPolickoNode();
                gridPaneGameField.add(labell, x, y);
                GridPane.setHgrow(labell, Priority.ALWAYS);     //aby se grid roztahoval...
                GridPane.setVgrow(labell, Priority.ALWAYS);
                labell.setAlignment(Pos.CENTER_RIGHT);
            }
        }
        miny.ReloadPolickosValues();
    }

    @FXML
    private void ResetButtonClicked(ActionEvent event) {
        System.out.println("Reset Clicked");
        CreateGameField();
    }

    @FXML
    private void btmNewGameClicked(ActionEvent event) {
        if (isNumeric(txt_minasCount.getText()) && isNumeric(txtSizeCount.getText())) {
            int mins = Integer.valueOf(txt_minasCount.getText());
            int fieldS = Integer.valueOf(txtSizeCount.getText());

            if ((fieldS * fieldS > mins + 7) && fieldS < maxPossibleFieldSize) {
                _minsCount = mins;
                _fieldSize = fieldS;
                CreateGameField();
                return;
            }
        }
        System.err.println("e-e");
    }

    private static boolean isNumeric(String str) {
        if (str.isEmpty()) {
            return false;
        }
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    @FXML
    private void btnHardcorePressed(ActionEvent event) {

        if (isNumeric(txtSizeCount.getText())) {
            int fieldS = Integer.valueOf(txtSizeCount.getText());
            _minsCount = fieldS * fieldS - 1;
            if (fieldS < 45) {
                _fieldSize = fieldS;
                CreateGameField();
                return;
            }
        } else {
            _minsCount = _fieldSize * _fieldSize - 1;
            CreateGameField();
            return;
        }
        System.err.println("e-e");
    }

}
