package gitsvistohry.APPS.hry.g001xOxMiny;

import java.lang.reflect.Field;
import java.util.Random;
import gitsvistohry.*;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Label;

/**
 *
 * @author Ondra
 */
public class Miny {

    private final int size;
    private final int mineNumber;
    private Random rnd = new Random();
    private boolean loosed = false;
    private boolean won = false;

    private Policko[][] minovePole;

    private Label _lbl_mines;
    private Label _lbl_flagged;
    private Label lbl_Undiscovered;
    private Label lbl_wonOrLoosed;

    public Miny(int size, int mineNumber) {
        this.size = size;
        this.mineNumber = mineNumber;
        init();
    }

    public Miny(int size, int mineNumber, Label lbl_mines, Label lbl_flagged,
            Label lbl_Undiscovered, Label lbl_wonOrLoosed) {
        this.size = size;
        this.mineNumber = mineNumber;
        this._lbl_mines = lbl_mines;
        this._lbl_flagged = lbl_flagged;
        this.lbl_Undiscovered = lbl_Undiscovered;
        this.lbl_wonOrLoosed = lbl_wonOrLoosed;
        init();
    }

    /**
     * vytvoří pole a miny v něm
     */
    private void init() {
        CreateMinovePole();

        AddMinasToMinovePole();

    }

    private void AddMinasToMinovePole() {
        int x;
        int y;
        for (int i = 0; i < mineNumber; i++) {
            x = rnd.nextInt(size);
            y = rnd.nextInt(size);

//            DevFunctions.SOUT_myOutput_nepodstatne("náhodně vygenerováno --:-- x:" + x + ",y: " + y);
            if (minovePole[x][y].isIsMina()) {
                i--;
                DevFunctions.SOUT_myOutput_nepodstatne("Mina už tu je, zkusím to jinam.  (x: " + x + ",y: " + y + ")");
            } else {
                DevFunctions.SOUT_myOutput_nepodstatne("Mina přidána na souřadnice -:- x: " + x + ",y: " + y + "\n");
                minovePole[x][y].setIsMina(true);
                computeAndSetPocetOkolnichMin(x, y);
            }
        }
        DevFunctions.SOUT_myOutput_nepodstatne("-----\n\n");
    }

    private void CreateMinovePole() {
        minovePole = new Policko[size][size];
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                minovePole[x][y] = new Policko(x, y, this);
            }
        }
    }

    public Policko getPolicko(int x, int y) {
        try {
            return minovePole[x][y];
        } catch (Exception e) {
            System.err.println("tak si vocas protože šaháš mimo pole :D getPolicko.... x:" + x + ", y: " + y);
        }
        return minovePole[0][0];
    }

    public void computeAndSetPocetOkolnichMin(int x, int y) {
        int pocetOkolnichMin = 0;
        for (int i = x - 1; i < x + 2; i++) {
            for (int j = y - 1; j < y + 2; j++) {
                try {
                    minovePole[i][j].increment();
                } catch (Exception e) {

                }
            }
        }
    }

    private int getValuePolicka(int x, int y) {
        //sem dát ještě neobjeveno
        return minovePole[x][y].getValue();
    }

//    private void ComputePolickaValues() {
//        for (int x = 0; x < size; x++) {
//            for (int y = 0; y < size; y++) {
//                minovePole[x][y].setValue(getValuePolicka(x, y));
//            }
//        }
//    }
    public String[][] getFieldValues() {
        String[][] valuesField = new String[size][size];
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                valuesField[x][y] = "" + getValuePolicka(x, y);
            }
        }
        return valuesField;
    }

    public int getSize() {
        return size;
    }

    public int getMineNumber() {
        return mineNumber;
    }

    public boolean discover(int x, int y) {
        minovePole[x][y].Discover();
        if (minovePole[x][y].getValue() == 0) {
            discoverWave(x, y);
        }
//        ReloadPolickosValues();
        return loosed;
    }

    public void discoverWave(int x, int y) {
        boolean[][] wawed = new boolean[size][size];     //možná bude třeba všechno nastavit na 0
        discoverWave(x, y, wawed);
    }

    public void discoverWave(int x, int y, boolean[][] wawed) {
        wawed[x][y] = true;
        int directions[][] = new int[][]{
            {x - 1, y + 1}, {x, y + 1}, {x + 1, y + 1},
            {x - 1, y}, {x + 1, y},
            {x - 1, y - 1}, {x, y - 1}, {x + 1, y - 1}
        };
        for (int[] direction : directions) {
            try {
                if ((x == x % (size)) && (y == y % (size)) && (!wawed[direction[0]][direction[1]])) { //SIZE -1
                    tryWaweDiscover(direction[0], direction[1], wawed);
                }
            } catch (Exception e) {

            }
        }
    }

    public void tryWaweDiscover(int x, int y, boolean[][] wawed) {
        wawed[x][y] = true;
        minovePole[x][y].Discover();
        unflagIfFlagged(x, y);
        if (minovePole[x][y].getValue() == 0) {
            discoverWave(x, y, wawed);
        }
    }

    public boolean isLoosed() {
        return loosed;
    }

    public void ReloadPolickosValues() {
        System.out.println("reloading");
        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                getPolicko(x, y).getPolickoNode().setValueAndSetText();
            }
        }
        ReloadStatLabels();
        if (loosed) {
            showAllUndiscovered();
        }
    }

    public void setLoosed(boolean loosed) {
        if (loosed && !this.loosed && !won) {
            YouLoosed();
        } else{
            System.err.println("nastavuješ Loosed na false, nebo nějáká kravina se stala, bude někde asi chyba.... je to sice setter ale takhle ne");
        }
    }

    private void unflagIfFlagged(int x, int y) {
        if (minovePole[x][y].isFlagged()) {
            minovePole[x][y].setFlagged(false);
        }
    }

    public int getNuberOfFlaggedPolickos() {
        int count = 0;
        for (Policko[] polePolicek : minovePole) {
            for (Policko policko : polePolicek) {
                if (policko.isFlagged() && !policko.isDiscovered()) {
                    count++;
                }
            }
        }
        return count;
    }

    public int getNumberOfUndiscoveredPolickos() {
        int count = 0;
        for (Policko[] polePolicek : minovePole) {
            for (Policko policko : polePolicek) {
                if (!policko.isDiscovered()) {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean checkIfWin() {
        int a = getNuberOfFlaggedPolickos();
        int b = getNumberOfUndiscoveredPolickos();
        if (b == mineNumber && !won && !loosed) { //(a == b) && b == mineNumber) ||
            YouWon();
            return true;
        }
        return false;
    }

    public Label getLbl_mines() {
        return _lbl_mines;
    }

    public Label getLbl_flagged() {
        return _lbl_flagged;
    }

    public Label getLbl_Undiscovered() {
        return lbl_Undiscovered;
    }

    public void ReloadStatLabels() {
        getLbl_flagged().setText("Flagged: " + getNuberOfFlaggedPolickos());
        getLbl_mines().setText("Mines: " + mineNumber);
        getLbl_Undiscovered().setText("Undiscovered: " + getNumberOfUndiscoveredPolickos());
        setLabelWinOrLoosed();
        checkIfWin();
    }

    public void showAllUndiscovered() {
        int count = 0;
        for (Policko[] polePolicek : minovePole) {
            for (Policko policko : polePolicek) {
                if (!policko.isDiscovered()) {
                    policko.getPolickoNode().setText(policko.getPolickoNode().getStringByValue());
                }
            }
        }
    }

    private void setLabelWinOrLoosed() {
        if (won) {
            lbl_wonOrLoosed.setText("YOU WON");
            lbl_wonOrLoosed.setStyle(DevSettings.standardPoleFxCSS + "-fx-text-fill: green;");
        } else if (loosed) {
            lbl_wonOrLoosed.setText("YOU LOOSED");
            lbl_wonOrLoosed.setStyle(DevSettings.standardPoleFxCSS + "-fx-text-fill: red;");
        } else {
            lbl_wonOrLoosed.setText("Game started.");
            lbl_wonOrLoosed.setStyle("");
        }
    }

    private void YouLoosed() {
        showAllUndiscovered();
        this.loosed = true;
    }

    private void YouWon() {
        System.out.println("YOU WON !!!!");
        won = true;
        showAllUndiscovered();
    }

}
