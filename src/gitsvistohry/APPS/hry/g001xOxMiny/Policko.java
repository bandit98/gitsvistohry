/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g001xOxMiny;

import javafx.event.EventHandler;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Ondra
 */
public class Policko {
    Miny miny;

    private boolean discovered = false;
    private boolean isMina = false;
    private int Value = 0;
    private PolickoNode polickoNode;
    private boolean flagged=false;

    public Miny getMiny() {
        return miny;
    }

    public void setMiny(Miny miny) {
        this.miny = miny;
    }

    private int x;
    private int y;

    public Policko(int x, int y,Miny miny) {
        this.miny=miny;
        this.x=x;
        this.y=y;
        polickoNode = new PolickoNode(this);
    }

    /**
     *
     * @return returns true when Mina was discoveded.
     */
    public void Discover() {
        if (this.discovered) {
            System.out.println("\nuž je discovered!\n");
        }
        this.discovered = true;
        if(isMina){
            System.out.println("prohrál si.");
            miny.setLoosed(true);
        }
    }

    public boolean isDiscovered() {
        return discovered;
    }

    public void setDiscovered(boolean discovered) {
        this.discovered = discovered;
    }

    public void increment() {
        if (!(this.Value == -1)) {
            this.Value++;
        }
    }

    public boolean isIsMina() {
        return isMina;
    }

    public void setIsMina(boolean isMina) {
        this.Value = -1;
        this.isMina = isMina;
    }

    public int getValue() {
        return Value;
    }

    public void setValue(int Value) {
        this.Value = Value;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public PolickoNode getPolickoNode() {
        return polickoNode;
    }

    public void setPolickoNode(PolickoNode polickoNode) {
        this.polickoNode = polickoNode;
    }

    
    public boolean isFlagged() {
        return flagged;
    }

    public void setFlagged(boolean flagged) {
        if(flagged){
            polickoNode.flag();
        }else{
            polickoNode.unflag();
        }
        this.flagged = flagged;
    }

}
