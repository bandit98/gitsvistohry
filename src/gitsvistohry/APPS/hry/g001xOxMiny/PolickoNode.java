/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g001xOxMiny;

import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 *
 * @author ucebna
 */
public class PolickoNode extends Label {

    private final Policko policko;

//    int x;
//    int y;
    public PolickoNode(Policko policko) {
        makeEventHandlers();
        this.setStyle(DevSettings.standardPoleFxCSS);
        this.policko = policko;
    }

    public void setValueAndSetText() {
        String finalSetText = "";
        if (!policko.isDiscovered()) {
            finalSetText = "?";
        } else {
            finalSetText = getStringByValue();
        }
        this.setText(finalSetText);
    }

    public String getStringByValue() {
        String finalSetText;
        switch (policko.getValue()) {
            case -1:
                this.setEffect(new DropShadow(7,Color.RED));
                finalSetText = "X";
                break;
            case 0:
                finalSetText = "";
                break;
            default:
                finalSetText = "" + policko.getValue();
                break;
        }
        return finalSetText;
    }

    @Override
    public String toString() {
        return "PolickoNode{" + "x=" + policko.getX() + ", y=" + policko.getY() + '}';
    }

    private void makeEventHandlers() {
        this.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getButton() == MouseButton.SECONDARY) {

                    if ((!policko.isFlagged()) && !policko.isDiscovered()) {
                        policko.setFlagged(true);
                    } else {
                        policko.setFlagged(false);
                    }
                    e.consume();
                }
                policko.miny.ReloadStatLabels();
            }
        });
        this.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent e) {
                if (e.getButton() == MouseButton.PRIMARY) {
                    policko.miny.ReloadStatLabels();
                    if (!policko.miny.isLoosed()) {
                        if (!policko.isFlagged()) {
                            policko.miny.discover(policko.getX(), policko.getY());
                            setEffect(new DropShadow());
                            policko.miny.ReloadPolickosValues();
                        }
                    }
                }
                policko.miny.ReloadStatLabels();
            }
        });
    }

    /**
     * do NOT use directly. Use policko.setFlagged(boolean value) instead
     */
    public void flag() {
        setStyle(DevSettings.standardPoleFxCSS + " -fx-text-fill: red;");
        System.out.println("flagged, x:" + policko.getX() + ", y: " + policko.getY());
    }

    /**
     * do NOT use directly. Use policko.setFlagged(boolean value) instead
     */
    public void unflag() {
        setStyle(DevSettings.standardPoleFxCSS);
        System.out.println("unflagged, x:" + policko.getX() + ", y: " + policko.getY());
    }

}
