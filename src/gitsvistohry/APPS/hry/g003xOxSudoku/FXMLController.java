/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g003xOxSudoku;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author Ondra
 */
public class FXMLController implements Initializable {

    @FXML
    private AnchorPane pane_SudokuRender;
    @FXML
    private ToggleGroup difficulty;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void btnGenerateGame(ActionEvent event) {
    }

    @FXML
    private void btnSolveGame(ActionEvent event) {
    }

    @FXML
    private void btnInsertCustomGame(ActionEvent event) {
    }
    
}
