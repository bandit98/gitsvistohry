/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g003xOxSudoku;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Ondra
 */
public class SudokuSolver {

//    int[][][][] sudokuBaseField = new int[3][3][3][3];    //a,b,c,d 
    private long krokuKVyreseni = 0;
    private final long MAXkrokuKVyreseni = 500000;

    private int a = 0, b = 0, c = 0, d = 0;
    private final boolean[] checkBoolField = new boolean[10];

    public static void main(String[] args) {
        SudokuSolver game = new SudokuSolver();
//        game.solve(""
//                + "000200063\n"
//                + "300005401\n"
//                + "001003980\n"
//                + "000000090\n"
//                + "000538000\n"
//                + "030000000\n"
//                + "026300500\n"
//                + "503700008\n"
//                + "470001000\n"
//        );
        String results[] = game.solve(""
                + "530070000\n"
                + "600195000\n"
                + "098000060\n"
                + "800060003\n"
                + "400803001\n"
                + "700020006\n"
                + "060000280\n"
                + "000419005\n"
                + "000080079\n"
        );


////        game.solve(""
////                + "020501403\n"
////                + "103064007\n"
////                + "004300051\n"
////                + "400000516\n"
////                + "060013040\n"
////                + "710045302\n"
////                + "092106000\n"
////                + "876430120\n"
////                + "000070600\n"
////        );
//////        System.out.println("Existuje úbec šance na vyřešení : " + game.isFieldCorrect());
//////        System.out.println("Vstupní sudoku : \n" + game.sudokuIntFieldToString(game.getSudokuBaseField()));
//        String[] solutions = game.getPossibleSolutionsBacktrack(game.getSudokuBaseField().clone());
        String[] solutions = results;
        System.out.print("Počet řešení: _" + solutions.length + "_");
        for (int i = 0; i < solutions.length; i++) {
            System.out.println("\nMožné řešení číslo " + (i + 1) + " : \n");
            System.out.println(solutions[i]);
        }
        if (solutions.length == 0) {
            System.out.println("\nNemá řešení.");
        }
        System.out.println("Počet potřebných kroků k vyhledání všech řešení [backtrack] : " + game.krokuKVyreseni);
    }

    /**
     *
     * @param sudokuInputStr
     * @return vrátí všechny možné řešení
     */
    public String[] solve(String sudokuInputStr) {
        int[][][][] myFieldClone = new int[3][3][3][3];
        SudokuSolver.loadStringSudokuToField(sudokuInputStr, myFieldClone);

        krokuKVyreseni = 0;
        String[] possibleSolutions = new String[0];
        try {
            possibleSolutions = getPossibleSolutionsStr(myFieldClone);
        } catch (Exception e) {
            System.err.println(e);
        }
        return possibleSolutions;
    }

    public ArrayList<int[][][][]> solve(int[][][][] sudokuInputFi) {
        int[][][][] myFieldClone = SudokuSolver.cloneSudokuMartix(sudokuInputFi);

        krokuKVyreseni = 0;
        ArrayList<int[][][][]> possibleSolutions = new ArrayList<>();
        try {
            possibleSolutions = getPossibleSolutionsField(myFieldClone);
        } catch (Exception e) {
            System.err.println(e);
        }

        return possibleSolutions;
    }

//    private int[][][][] cloneAndSetField(int[][][][] sudokuInputField) {
//        sudokuBaseField = sudokuInputField;
//        int[][][][] myFieldClone = cloneSudokuMartix(sudokuBaseField);
//        return myFieldClone;
//    }
    /**
     * načte z původního stringu (ve formě ŘÁDEK S 9 ČÍSLY, \n, (a takhle 9 řádků) )
     *
     * @param sudokuInputStr
     * @param fieldToWriteIn
     */
    private static void loadStringSudokuToField(String sudokuInputStr, int[][][][] fieldToWriteIn) {
        String[] lines = sudokuInputStr.split("\n");

        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
                    for (int d = 0; d < 3; d++) {
                        fieldToWriteIn[a][b][c][d] = Character.getNumericValue(lines[b * 3 + d].charAt(a * 3 + c));
                    }
                }
            }
        }
    }

    private static int[][][][] convertStringSudokuToField(String sudokuInputStr) {
        int[][][][] fieldToWriteIn = new int[3][3][3][3];
        String[] lines = sudokuInputStr.split("\n");

        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
                    for (int d = 0; d < 3; d++) {
                        fieldToWriteIn[a][b][c][d] = Character.getNumericValue(lines[b * 3 + d].charAt(a * 3 + c));
                    }
                }
            }
        }
        return fieldToWriteIn;
    }

    public String[] getPossibleSolutionsStr(int[][][][] sudokuBaseField) throws Exception {
        ArrayList<int[][][][]> possibleSolutionsList = getPossibleSolutionsField(sudokuBaseField);
        //převod z Listu do String[]u :
        String[] solutions = new String[possibleSolutionsList.size()];
        int i = 0;
        for (int[][][][] intField : possibleSolutionsList) {
            solutions[i] = sudokuIntFieldToString(intField);
            i++;
        }
        return solutions;
    }

    public ArrayList<int[][][][]> getPossibleSolutionsField(int[][][][] sudokuField) throws Exception {
        ArrayList<Integer[]> coordsToFill = new ArrayList<>();

        //zjistím si který souřadnice musím vyplnit
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
                    for (int d = 0; d < 3; d++) {
                        if (sudokuField[a][b][c][d] == 0) {
                            coordsToFill.add(new Integer[]{a, b, c, d});
                        }
                    }
                }
            }
        }

        int maxLevel = coordsToFill.size() - 1;
        MyRefInt refActualLevel = new MyRefInt(-1);
        ArrayList<int[][][][]> possibleSolutionsList = new ArrayList<>();

        backTrackInnerSolve(refActualLevel, maxLevel, sudokuField, coordsToFill, possibleSolutionsList);

        return possibleSolutionsList;
    }

    private void backTrackInnerSolve(MyRefInt actualLevel, int maxLevel, int[][][][] sudokuField, ArrayList<Integer[]> coordsToFill, ArrayList<int[][][][]> possibleSolutionsList) throws Exception {
        actualLevel.mujInt++;
        if (krokuKVyreseni++ == MAXkrokuKVyreseni) {
            throw new Exception("Tohle by se prostě mělo jednoduše ošetřit, hledat řešení trvá moc dlouho [ víc než " + MAXkrokuKVyreseni + "]...");
        }
        //samotné řešení:
        for (int i = 1; i <= 9; i++) {
            sudokuField[coordsToFill.get(actualLevel.mujInt)[0]][coordsToFill.get(actualLevel.mujInt)[1]][coordsToFill.get(actualLevel.mujInt)[2]][coordsToFill.get(actualLevel.mujInt)[3]] = i;
            if (isFieldCorrect(sudokuField)) {
                if (actualLevel.mujInt == maxLevel) {
                    possibleSolutionsList.add(cloneSudokuMartix(sudokuField));
//                        System.out.println("SOLUTION FOUND!");
                    continue;
                } else {
                    backTrackInnerSolve(actualLevel, maxLevel, sudokuField, coordsToFill, possibleSolutionsList);
                }
            }
        }
        sudokuField[coordsToFill.get(actualLevel.mujInt)[0]][coordsToFill.get(actualLevel.mujInt)[1]][coordsToFill.get(actualLevel.mujInt)[2]][coordsToFill.get(actualLevel.mujInt)[3]] = 0;
        actualLevel.mujInt--;
    }

    private void backTrackInnerCountSolutions(MyRefInt actualLevel, int maxLevel, int[][][][] sudokuField,
            ArrayList<Integer[]> coordsToFill,
            MyRefInt numOfSolutions) throws Exception {
        actualLevel.mujInt++;
        if (krokuKVyreseni++ == MAXkrokuKVyreseni) {
            throw new Exception("Tohle by se prostě mělo jednoduše ošetřit, hledat řešení trvá moc dlouho [ víc než " + MAXkrokuKVyreseni + "]...");
        }
        //samotné řešení:
        for (int i = 1; i <= 9; i++) {
            sudokuField[coordsToFill.get(actualLevel.mujInt)[0]][coordsToFill.get(actualLevel.mujInt)[1]][coordsToFill.get(actualLevel.mujInt)[2]][coordsToFill.get(actualLevel.mujInt)[3]] = i;
            if (isFieldCorrect(sudokuField)) {
                if (actualLevel.mujInt == maxLevel) {
                    numOfSolutions.mujInt++;
                    return;
//                  System.out.println("SOLUTION FOUND!");
                } else {
                    backTrackInnerCountSolutions(actualLevel, maxLevel, sudokuField, coordsToFill, numOfSolutions);
                }
            }
        }
        sudokuField[coordsToFill.get(actualLevel.mujInt)[0]][coordsToFill.get(actualLevel.mujInt)[1]][coordsToFill.get(actualLevel.mujInt)[2]][coordsToFill.get(actualLevel.mujInt)[3]] = 0;
        actualLevel.mujInt--;
    }


    /*
    vrátí true  pokud sudoku není zatím v rozporu s žádným pravidlem (žádná čísla 1-9 se neopakují v řádcích sloucích ani čtvercích
     */
    public boolean isFieldCorrect(int[][][][] sudokuBaseField) {

        clearBoolField(checkBoolField);
        //kontroluju čísla v každé kostičce 3*3
        for (a = 0; a < 3; a++) {
            for (b = 0; b < 3; b++) {

                for (c = 0; c < 3; c++) {
                    for (d = 0; d < 3; d++) {
                        if (sudokuBaseField[a][b][c][d] == 0) {
                            continue;
                        }
                        if (checkBoolField[sudokuBaseField[a][b][c][d]]) {
//                            System.out.println(sudokuIntFieldToString(sudokuBaseField));
                            return false;
                        }
                        checkBoolField[(int) sudokuBaseField[a][b][c][d]] = true;
                    }
                }
                clearBoolField(checkBoolField);
            }
        }

        clearBoolField(checkBoolField);
        //projdu zde každý sloupec (každé X)
        for (a = 0; a < 3; a++) {
            for (c = 0; c < 3; c++) {
                //tu sem v každém sloupci

                for (b = 0; b < 3; b++) {
                    for (d = 0; d < 3; d++) {
                        if (sudokuBaseField[a][b][c][d] == 0) {
                            continue;
                        }
                        if (checkBoolField[sudokuBaseField[a][b][c][d]]) {
//                            System.out.println(sudokuIntFieldToString(sudokuBaseField));
                            return false;
                        }
                        checkBoolField[(int) sudokuBaseField[a][b][c][d]] = true;
                    }
                }
                clearBoolField(checkBoolField);
            }
        }

        clearBoolField(checkBoolField);
        //projdu zde každý řádek (každé Y)
        for (b = 0; b < 3; b++) {
            for (d = 0; d < 3; d++) {
                //tu sem v každém řádku

                for (a = 0; a < 3; a++) {
                    for (c = 0; c < 3; c++) {
                        if (sudokuBaseField[a][b][c][d] == 0) {
                            continue;
                        }
                        if (checkBoolField[sudokuBaseField[a][b][c][d]]) {
//                            System.out.println(sudokuIntFieldToString(sudokuBaseField));
                            return false;
                        }
                        checkBoolField[(int) sudokuBaseField[a][b][c][d]] = true;
                    }
                }
                clearBoolField(checkBoolField);
            }
        }
//        System.out.println(sudokuIntFieldToString(sudokuBaseField));
        return true;
    }

    private void clearBoolField(boolean[] inputField) {
        for (int i = 0; i < inputField.length; i++) {
            inputField[i] = false;
        }
    }

    public static boolean isFieldCompleted(int[][][][] inField) {
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
                    for (int d = 0; d < 3; d++) {
                        if (inField[a][b][c][d] == 0) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public static String sudokuIntFieldToString(int[][][][] inputSudokuField) {
        String tmp = "";
        for (int b = 0; b < 3; b++) {
            for (int d = 0; d < 3; d++) {
                //tu sem v každém řádku
                for (int a = 0; a < 3; a++) {
                    for (int c = 0; c < 3; c++) {
                        tmp += inputSudokuField[a][b][c][d];
                    }
                }
                tmp += "\n";
            }
        }
        return tmp;
    }

    public static int[][][][] cloneSudokuMartix(int[][][][] inputField) {
        int[][][][] tmp = new int[inputField.length][inputField[0].length][inputField[0][0].length][inputField[0][0][0].length];
        for (int i = 0; i < inputField.length; i++) {
            for (int j = 0; j < inputField[0].length; j++) {
                for (int k = 0; k < inputField[0][0].length; k++) {
                    tmp[i][j][k] = inputField[i][j][k].clone();
                }
            }
        }
        return tmp;
    }

    int getNumberOfSolutions(int[][][][] baseSudokuF) {
        if (SudokuSolver.isFieldCompleted(baseSudokuF)) {
            return 1;
        }
        ArrayList<Integer[]> coordsToFill = new ArrayList<>();

        //zjistím si který souřadnice musím vyplnit
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
                    for (int d = 0; d < 3; d++) {
                        if (baseSudokuF[a][b][c][d] == 0) {
                            coordsToFill.add(new Integer[]{a, b, c, d});
                        }
                    }
                }
            }
        }

        int maxLevel = coordsToFill.size() - 1;
        MyRefInt refActualLevel = new MyRefInt(-1);
        MyRefInt numberOfSolutons = new MyRefInt(0);

        try {
            krokuKVyreseni = 0;
            backTrackInnerCountSolutions(refActualLevel, maxLevel, SudokuSolver.cloneSudokuMartix(baseSudokuF), coordsToFill, numberOfSolutons);
        } catch (Exception e) {
            System.err.println("nacházení řešení trvá moc dlouho! vracím -1");
            return -1;
        }

////        System.out.println("Num of solutions : "+numberOfSolutons.mujInt);
        return numberOfSolutons.mujInt;
    }

    private class MyRefInt {

        public int mujInt;

        public MyRefInt(int mujInt) {
            this.mujInt = mujInt;
        }

    }

}
/*
 + "093105640\n"
                + "700000005\n"
                + "501209307\n"
                + "200000003\n"
                + "036907520\n"
                + "900000001\n"
                + "302408109\n"
                + "600000004\n"
                + "047302850\n"
 */
