/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g003xOxSudoku;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 *
 * @author Ondra
 */
public class SudokuGenerator {

    private int[][][][] sudokuBaseField = new int[3][3][3][3];    //a,b,c,d 
    private int countOfNumbersToPreFill = 30;

    private SudokuSolver sSolver = new SudokuSolver();
    private int krokuKVygenerovani = 0;

    private final int minimumFilledPolickos = 20;
    private final int MAXLEVEL = 3 * 3 * 3 * 3;

//    public static void main(String[] args) {
//        SudokuGenerator suGen = new SudokuGenerator();
//        System.out.println(suGen.GenerateSudoku());
//    }
    public static void main(String[] args) {
        SudokuGenerator suGen = new SudokuGenerator();

        int zerosRequesed = 16;
        int[][][][] generatedBaseField = suGen.GenerateSudokuField(zerosRequesed);

        System.out.println("výsledek: \n" + SudokuSolver.sudokuIntFieldToString(generatedBaseField));
    }

    public String GenerateSudoku(int zerosRequesed) {
        return SudokuSolver.sudokuIntFieldToString(GenerateSudokuField(zerosRequesed));
    }

    public int[][][][] GenerateSudokuField(int zerosRequesed) {
        int[][][][] baseSudokuF = GenerateRandomBaseSudokuField();

        List<Integer[]> coordsNOTToChange = null;
        MakeEmptySpaces(baseSudokuF, zerosRequesed, coordsNOTToChange);

        return baseSudokuF;
    }

    private int[][][][] GenerateRandomBaseSudokuField() {
        int[][][][] baseSolvedSudoku = GenerateBaseInnerSudoku(new int[3][3][3][3], new SimpleIntegerProperty(-1), new int[4], new boolean[10]);

        return baseSolvedSudoku;
    }

    //>1 zkus přidat další
    private int[][][][] GenerateBaseInnerSudoku(int[][][][] inField, IntegerProperty levelIntProp, int[] coordsField, boolean[] numbersToTry) {
        levelIntProp.set(levelIntProp.get() + 1);
        setFieldCoordsByLevel(levelIntProp, coordsField);

        clearBoolField(numbersToTry);
        RandomNumbersEngine rndNumEng = new RandomNumbersEngine();
i:      for (int i = 1; i <= 9; i++) {
            setFieldCoordsByLevel(levelIntProp, coordsField);
            inField[coordsField[0]][coordsField[1]][coordsField[2]][coordsField[3]] = rndNumEng.getNextNum();
            if (sSolver.isFieldCorrect(inField)) {
                if (levelIntProp.get() == MAXLEVEL) {
                    System.out.println("sme tůůů");
                    break;
                } else {
                    int[][][][] possRes = GenerateBaseInnerSudoku(inField, levelIntProp, coordsField, numbersToTry);
                    if (possRes == null) { //další možnosti tímto směrem nevedou
                        if (i == 9) { // všechny možnosti sou špatně
                            inField[coordsField[0]][coordsField[1]][coordsField[2]][coordsField[3]] = 0;
                            levelIntProp.set(levelIntProp.get() - 1);
                            setFieldCoordsByLevel(levelIntProp, coordsField);
                            return null;
                        }
                    } else {
                        break;
                    }
                }
            } else if (i == 9) { // všechny možnosti sou špatně
                inField[coordsField[0]][coordsField[1]][coordsField[2]][coordsField[3]] = 0;
                levelIntProp.set(levelIntProp.get() - 1);
                setFieldCoordsByLevel(levelIntProp, coordsField);
                return null;
            }
        }

        levelIntProp.set(levelIntProp.get() - 1);
        return inField;
    }

    private void setFieldCoordsByLevel(IntegerProperty levelIntProp, int[] coordsField) {
        int tmpLev = 0;
        int level = levelIntProp.get();
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
                    for (int d = 0; d < 3; d++) {
                        if (tmpLev == level) {
                            coordsField[0] = a;
                            coordsField[1] = b;
                            coordsField[2] = c;
                            coordsField[3] = d;
//                            System.out.println("" + a + b + c + d);
                        }
                        tmpLev++;
                    }
                }
            }
        }
    }

    private void MakeEmptySpaces(int[][][][] baseSudokuF, int zerosRequesed, List<Integer[]> coordsNOTToChange) {//tady je chyba...
        SudokuSolver suSolver = new SudokuSolver();
        Random rnd = new Random();

        List<Integer[]> coordsToDel = new LinkedList<>();

        //zjistím si který souřadnice musím vyplnit
        for (int a = 0; a < 3; a++) {
            for (int b = 0; b < 3; b++) {
                for (int c = 0; c < 3; c++) {
d:                  for (int d = 0; d < 3; d++) {

                        if (coordsNOTToChange != null) {
                            for (Integer[] coords : coordsNOTToChange) {
                                if (a == coords[0]
                                        && b == coords[1]
                                        && c == coords[2]
                                        && d == coords[3]) {
                                    continue d;
                                }
                            }
                        }
                        coordsToDel.add(new Integer[]{a, b, c, d});

                    }
                }
            }
        }

        //náhodný zamíchání vstupních souřadnic
        for (int i = 0; i < 200; i++) {
            Integer[] coordsToPutOnTop = coordsToDel.remove(rnd.nextInt(coordsToDel.size() - 1));
            coordsToDel.add(coordsToPutOnTop);
        }

        IntegerProperty levelProp = new SimpleIntegerProperty(-1);
        IntegerProperty zerosRequestedProp = new SimpleIntegerProperty(zerosRequesed);
        int[][][][] resultSudoku = null;
        resultSudoku = MakeEmptySpacesINNER(levelProp, baseSudokuF, zerosRequestedProp, rnd, suSolver, coordsToDel);
        System.out.println(resultSudoku);
        System.out.println("Výsledek je : \n"+SudokuSolver.sudokuIntFieldToString(resultSudoku));
    }

    int maxDosazenyLevelSmazat=0;
    private int[][][][] MakeEmptySpacesINNER(IntegerProperty levelProp, int[][][][] baseSudokuF, IntegerProperty zerosCountRequesed,
            Random rnd, SudokuSolver suSolver,List<Integer[]> coordsToPossiblyDel) {
        levelProp.set(levelProp.get() + 1);
        
        if(rnd.nextInt(70000)==8){
                    System.out.println("Level: "+levelProp.get());
        }

        Integer[] actualCoords;
i:      for (int i = 0; i < coordsToPossiblyDel.size() - 1; i++) {
            actualCoords = coordsToPossiblyDel.remove(0);

            int a = actualCoords[0];
            int b = actualCoords[1];
            int c = actualCoords[2];
            int d = actualCoords[3];
            int originalPolickoValue = baseSudokuF[a][b][c][d];
            if(baseSudokuF[a][b][c][d]==0){
                continue i;
            }
            baseSudokuF[a][b][c][d] = 0;
            if (suSolver.getNumberOfSolutions(baseSudokuF) == 1) {
                if(rnd.nextInt(70000)==8){
                    System.out.println("Level2: "+levelProp.get());
                }
                if(levelProp.get()>maxDosazenyLevelSmazat){
                    maxDosazenyLevelSmazat=levelProp.get();
                    System.out.println(maxDosazenyLevelSmazat);
                }
                //tady - sudoku má i po přidání této nuly přesně 1 řešení. můžeme pokračovat
                if (levelProp.get() == zerosCountRequesed.get()) {
                    return SudokuSolver.cloneSudokuMartix(baseSudokuF);
                }
                int[][][][] possRes = MakeEmptySpacesINNER(levelProp, baseSudokuF, zerosCountRequesed, rnd, suSolver, coordsToPossiblyDel);
                if (possRes != null) {
                    return possRes;
                }
            }
            baseSudokuF[a][b][c][d]=originalPolickoValue;

            coordsToPossiblyDel.add(actualCoords);
        }

        levelProp.set(levelProp.get() - 1);
        return null;
    }

    //------------------------------------------------------------------
    //------------------------------------------------------------------
    //------------------------------------------------------------------
    private void clearBoolField(boolean[] inputField) {
        for (int i = 0; i < inputField.length; i++) {
            inputField[i] = false;
        }
    }

    private final class RandomNumbersEngine {

        private final int[] numsField = new int[9];
        private int mixXTimes = 20;
        private Random rnd = new Random();

        private int iterator = 0;

        public RandomNumbersEngine() {
            for (int i = 0; i <= 8; i++) {
                numsField[i] = i + 1;
            }
            mixNumbersAndReset();
        }

        public void mixNumbersAndReset() {
            resetIterator();
            for (int i = 0; i < mixXTimes; i++) {
                int rndIter1 = rnd.nextInt(numsField.length);
                int rndIter2 = rnd.nextInt(numsField.length);
                int tmp = numsField[rndIter1];
                numsField[rndIter1] = numsField[rndIter2];
                numsField[rndIter2] = tmp;
            }
        }

        public void resetIterator() {
            iterator = 0;
        }

        public int getNextNum() {
            return numsField[iterator++];
        }

    }
}
