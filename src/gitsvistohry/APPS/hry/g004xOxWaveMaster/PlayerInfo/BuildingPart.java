/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.PlayerInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.scene.layout.TilePane;

/**
 *
 * @author Ondra
 */
class BuildingPart extends TilePane {

    public BuildingPart() {
        addDefaultTBGNodes();
    }

    private void addDefaultTBGNodes() {
        this.getChildren().addAll(TowerBuildGroupNode.defaultTBGN);
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //

    public void addTowerBuildGroupNode(TowerBuildGroupNode tbnToAdd) {
        this.getChildren().add(tbnToAdd);
    }

    public final void addAllTowersBuildGroupNodes(TowerBuildGroupNode... tbnsToAdd) {
        this.getChildren().addAll(Arrays.asList(tbnsToAdd));
    }

}
