/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.PlayerInfo;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Game;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Ondra
 */
class TowerBuildGroupNode extends VBox {

    public static final TowerBuildGroupNode[] defaultTBGN = new TowerBuildGroupNode[]{
        new TowerBuildGroupNode(
        new TowerBuildNode(new t001LaserTower(0, 0, null, null)),
        new TowerBuildNode(new t002LaserTowerBigger(0, 0, null, null))
        ),
        new TowerBuildGroupNode(
        new TowerBuildNode(new t003Launcher(0, 0, null, null))
        )
    };

    public TowerBuildGroupNode() {

    }

    public TowerBuildGroupNode(TowerBuildNode... tbnsToAdd) {
        addAllTowersBuildNodes(tbnsToAdd);
    }

    public void addTowerBuildNode(TowerBuildNode tbnToAdd) {
        this.getChildren().add(tbnToAdd);
    }

    public final void addAllTowersBuildNodes(TowerBuildNode... tbnsToAdd) {
        this.getChildren().addAll(Arrays.asList(tbnsToAdd));
    }
}
