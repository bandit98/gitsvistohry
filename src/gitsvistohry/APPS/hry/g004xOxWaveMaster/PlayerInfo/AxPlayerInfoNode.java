/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.PlayerInfo;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.*;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.geometry.HPos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Ondra
 */
public class AxPlayerInfoNode extends VBox implements Actualizable {

    private Game game;

    private final ModeInfo modelInfo = new ModeInfo(10);

    public AxPlayerInfoNode(Game game, DoubleProperty x, int y, int w, int h) {
        this.game = game;
        this.setPrefWidth(w);
        this.setPrefHeight(h);
        this.layoutXProperty().bind(x);
        this.setLayoutY(y);
        this.setStyle("-fx-background-color: cacaca;");
        init();
    }

    private void init() {
        addPlayersInfo();
        addBuildingPart();
        addNextWaveNode();
        addModelInfoNode();
    }

    protected void addNextWaveNode() {
        Button btnNextWave = new Button("Next Wave");
        btnNextWave.setOnAction((ActionEvent event) -> {
            this.game.nextWaveAction();
        });
        this.getChildren().add(btnNextWave);
    }

    private void addPlayersInfo() {
        GridPane gridP = new GridPane();
        gridP.setHgap(5);
        gridP.setVgap(5);
        ColumnConstraints column1 = new ColumnConstraints(100);
        ColumnConstraints column2 = new ColumnConstraints(50, 150, 300);
        column2.setHgrow(Priority.ALWAYS);
        gridP.getColumnConstraints().addAll(column1, column2);

        Label lblHpFinal = new Label("HP: ");
        GridPane.setHalignment(lblHpFinal, HPos.RIGHT);
        gridP.add(lblHpFinal, 0, 0);

        Label lblHp = new Label();
        lblHp.textProperty().bind(game.getPlayer().getHp().asString());
        GridPane.setHalignment(lblHp, HPos.CENTER);
        gridP.add(lblHp, 1, 0);

        Label lblScoreFinal = new Label("Score: ");
        GridPane.setHalignment(lblScoreFinal, HPos.RIGHT);
        gridP.add(lblScoreFinal, 0, 1);

        Label lblScore = new Label();
        lblScore.textProperty().bind(game.getPlayer().getScore().asString());
        GridPane.setHalignment(lblScore, HPos.CENTER);
        gridP.add(lblScore, 1, 1);

        Label lblMoneyFinal = new Label("Money: ");
        GridPane.setHalignment(lblMoneyFinal, HPos.RIGHT);
        gridP.add(lblMoneyFinal, 0, 2);

        Label lblMoney = new Label();
        lblMoney.textProperty().bind(game.getPlayer().getMoney().asString());
        GridPane.setHalignment(lblMoney, HPos.CENTER);
        gridP.add(lblMoney, 1, 2);

        this.getChildren().add(gridP);
    }

    private void addBuildingPart() {
        this.getChildren().add(new BuildingPart());

        for (Node node : this.getChildren()) {
            if (node instanceof BuildingPart) {

                for (Node tbgn : ((BuildingPart) node).getChildren()) {
                    if (tbgn instanceof TowerBuildGroupNode) {

                        for (Node tbn : ((TowerBuildGroupNode) tbgn).getChildren()) {
                            if (tbn instanceof TowerBuildNode) {
                                ((TowerBuildNode) tbn).setGame(this.game);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void actualize() {
        this.getChildren().stream().filter((node) -> (node instanceof Actualizable)).forEach((node) -> {
            ((Actualizable) node).actualize();
        });
    }

    private void addModelInfoNode() {
        this.getChildren().add(modelInfo);
    }

    //--------------------------------------//--------------------------------------//-------------------------
    //--------------------------------------//--------------------------------------//-------------------------
    //--------------------------------------//--------------------------------------//-------------------------
    //--------------------------------------//--------------------------------------//-------------------------
    //--------------------------------------//--------------------------------------//-------------------------
    //--------------------------------------//--------------------------------------//-------------------------
    //--------------------------------------//--------------------------------------//-------------------------
    //
    public Game getGame() {
        return game;
    }

    public ModeInfo getModelInfo() {
        return modelInfo;
    }

}
