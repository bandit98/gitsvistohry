/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.PlayerInfo;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author Ondra
 */
public class ModeInfo extends VBox implements Actualizable {

    private Entity myWatchingEntity;


    private final String defaultHealthText = "Nic.";
    private final Label healthLbl = new Label(defaultHealthText);

    private final String defaultKilledCountText = "";
    private final Label killedCountLbl = new Label(defaultKilledCountText);

    public ModeInfo(double spacing) {
        super(spacing);
        myInit();
    }

    private void myInit() {
        this.getChildren().addAll(healthLbl,killedCountLbl);
    }

    @Override
    public void actualize() {
        if (myWatchingEntity != null) {
            healthLbl.setText("health : " + myWatchingEntity.getHealth());
            killedCountLbl.setText("Killed "+myWatchingEntity.getEntitiesIKilled().size()+" entities.");
        } else {
            healthLbl.setText(defaultHealthText);
            killedCountLbl.setText(defaultKilledCountText);
        }

    }

    public void setMyWatchingEntity(Entity myWatchingEntity) {
        this.myWatchingEntity = myWatchingEntity;
        actualize();
    }
    
    
    public Entity getMyWatchingEntity() {
        return myWatchingEntity;
    }

}
