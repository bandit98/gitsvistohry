/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.PlayerInfo;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.DefenceTower;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Game;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.GameModeEnum;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.SETTINGS;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

/**
 *
 * @author Ondra
 */
public class TowerBuildNode extends StackPane {

    private DefenceTower myDefTower;
    private final int size = 50;
    private Game game;

    public TowerBuildNode(DefenceTower dt) {
//        this.getChildren().add(new Label(dt.getClass().getName().split("\\.")[dt.getClass().getName().split("\\.").length-1]));
        this.getChildren().add(new ImageView(SETTINGS.towerBuildNodeIMAGE) {
            {
                setFitWidth(size * 1.2);
                setFitHeight(size * 1.2);
            }
        });
        if (dt != null) {
            this.getChildren().add(new ImageView(dt.getImageToPaint()) {
                {
                    setFitWidth(size);
                    setFitHeight(size);
                }
            });
        } else {
            System.err.println("DefTower os null (TowerBuildNode)");
        }
        this.getChildren().add(new Label("\n\n"+dt.getPriceOfDT()){{setWrapText(true);setStyle("-fx-text-fill: lightblue;-fx-font-weight: 800;");}});
        

        this.setOnMouseClicked((MouseEvent event) -> {
            mouseClicked();
        });

        myDefTower = dt;
    }

    public DefenceTower getMyDefTower() {
        return myDefTower;
    }

    private void mouseClicked() {
        if (game != null) {
            game.setGameNodeEnum(GameModeEnum.BUILDING);
            game.setModeTowerBuildNode(this);
            System.out.println("yyyep");
        } else {
            for (int i = 0; i < 20; i++) {
                System.err.println("game is NULL!!!!!");
            }
            throw new RuntimeException();
        }
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

}
