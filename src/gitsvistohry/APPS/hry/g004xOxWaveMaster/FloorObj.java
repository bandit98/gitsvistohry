/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;

/**
 *
 * @author Ondra
 */
public class FloorObj extends PaintableObject {
    
    private final FloorObjEnum flObjEnum;

    {
        this.objectEnum = PaintableObjectEnum.FLOOR;
    }

    public FloorObj(long x, long y,FloorObjEnum flObjEnum) {
        super(x, y);
        this.flObjEnum=flObjEnum;
        
        findAndSetMyImage2();
    }
    
    protected void findAndSetMyImage2() {
        //nastavit obrázek
        String[] path = this.getClass().getName().split("\\.");
        int u=0;
        for (int i = 0; i < path.length; i++) {
            if(path[i].equals(Settings.APP_FOLDER_GAME.split("/")[Settings.APP_FOLDER_GAME.split("/").length-1])){
                u=i; 
                break;
            }
        }
        String[] res=new String[path.length-(u+1)];
        int z=0;
        for (int i = u+1; i < path.length-1; i++) {
            res[z++]=path[i];
        }
        res[res.length-1]=path[path.length-1]+flObjEnum.getMyChar()+".png";
        
        this.imageToPaint=ImageMyMy.getImage(Settings.APP_FOLDER_GAME, res);//4
    }

    public FloorObjEnum getFlObjEnum() {
        return flObjEnum;
    }

    public PaintableObjectEnum getObjectEnum() {
        return objectEnum;
    }
    
    
}
