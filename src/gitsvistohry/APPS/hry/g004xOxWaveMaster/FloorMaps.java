/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ondra
 */
public class FloorMaps {

    private static final List<String> floorMapsStrings = new ArrayList<>();

    static {
        floorMapsStrings.add(""//0.
                + "BBBBBBBBBBWWWWWWBB\n"
                + "BSWWWWBWWBWBBBWWBB\n"
                + "BBBBBWWWWWWWBBBWBB\n"
                + "BBBWWWBBBBWWWBBWBB\n"
                + "BBBWBBBBBBWBWBBWWW\n"
                + "BBBWBBBBBBBBWWWBBW\n"
                + "BBBWBBBBBBBBWBWWTW\n"
                + "BBBWWWWWWWWWWWBBBB\n"
                + "BBBBBBWBBBWWBBBBBB\n"
                + "BBBBBBWWWWWBBBBBBB\n");

        floorMapsStrings.add(""//1.
                + "BBWWSBBBB\n"
                + "BBWBWBBBB\n"
                + "BBWWWWWBB\n"
                + "BBWBBBWWT\n"
                + "BBWWWWWBB\n"
                + "BBBBBBBBB\n");

    }
    
    public static FloorMap getFloormap(int indexOfMap){
        return new FloorMap(floorMapsStrings.get(indexOfMap));
    }
}
