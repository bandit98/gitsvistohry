/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import java.util.LinkedList;
import java.util.List;
import javafx.scene.image.Image;

/**
 *
 * @author Ondra
 */
public class Level {

    private List<Wave> listOfWawes;
    private int actualWaveIter = 0;
    private List<Wave> actualWavesList = new LinkedList<>();
    private Path[] possiblePaths;
    private FloorMap floorMap;
    private Game game;

    private boolean isAllWavesPushed = false;

    private Image backgroundImage;

    private String levelName;

    public Level(List<Wave> listOfWawes, FloorMap floorMap, Image backgroundImage, String levelName, Game game) {
        this.listOfWawes = listOfWawes;
        this.floorMap = floorMap;
        this.backgroundImage = backgroundImage;
        this.levelName = levelName;
        this.game = game;
        tryToFindAndSetPaths(floorMap);
    }

    private void tryToFindAndSetPaths(FloorMap floorMap1) {
        possiblePaths = Path.findPaths(floorMap1);
    }

    void tick() {
        if (!isAllWavesPushed) {
            if (actualWavesList.isEmpty()) {
                actualWavesList.add(listOfWawes.get(0));
            }
            boolean lastWaveEnded = false;
            for (Wave actualWave : actualWavesList) {
                if (!actualWave.tick()) {
                    lastWaveEnded = false;
                    if (actualWave.peekEnemiesREADYToRoll() != null) {

                        List<Enemy> tmp = actualWave.popEnemiesREADYToRoll();
                        for (int j = 0; j < tmp.size(); j++) {
                            if (game != null) {
                                game.addToAllEntities(tmp.get(j));
                            } else {
                                System.err.println("game is null.");
                            }
                        }
                    }
                } else {
                    lastWaveEnded = true;
                }
            }

            if (lastWaveEnded && actualWaveIter < listOfWawes.size() - 1) {
                actualWaveIter++;
                actualWavesList.add(listOfWawes.get(actualWaveIter));
                System.out.println("pushing " + actualWaveIter + ". wave");
            } else {
                if (!lastWaveEnded) {
                    return;
                }
                isAllWavesPushed = true;
//////////////////////////////////////////            System.out.println("all waves pushed");
            }
        } else {  //všechny waveky sou vypuštěny...

        }
    }

    void nextWaveAction() {
        if (actualWaveIter < listOfWawes.size() - 1) {
            actualWaveIter++;
            actualWavesList.add(listOfWawes.get(actualWaveIter));
            System.out.println("pushing " + actualWaveIter + ". wave");
            actualWavesList.get(actualWavesList.size() - 1).comeNow_fckWaiting();
        } else {
//            System.out.println("všechny Waveky už byly vypuštěny.");
        }

    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    public List<Wave> getListOfWawes() {
        return listOfWawes;
    }

    public void setListOfWawes(List<Wave> listOfWawes) {
        this.listOfWawes = listOfWawes;
    }

    public FloorMap getFloorMap() {
        return floorMap;
    }

    public void setFloorMap(FloorMap floorMap) {
        this.floorMap = floorMap;
    }

    public Image getBackgroundImage() {
        return backgroundImage;
    }

    public void setBackgroundImage(Image backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public String getLevelName() {
        return levelName;
    }

    public void setLevelName(String levelName) {
        this.levelName = levelName;
    }

    public Path[] getPossiblePaths() {
        return possiblePaths;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public boolean isIsAllWavesPushed() {
        return isAllWavesPushed;
    }

    @Override
    public String toString() {
        return "Level{" + "listOfWawes=" + listOfWawes + ", floorMap=" + floorMap
                + ", backgroundImage=" + backgroundImage + ", levelName=" + levelName + '}';
    }

}
