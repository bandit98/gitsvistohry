/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author Ondra
 */
public class FXMLController implements Initializable {

    @FXML
    private Pane mainGamePane;
    @FXML
    private AnchorPane basePane;

    MainGame mainGame;

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        mainGame = new MainGame(mainGamePane);
//        mainGame.

        System.out.println(mainGame.getGame().getAllEntitesInGameList());

        mainGame.run(this);

    }

    @FXML
    private void nextWaveBtnAction(ActionEvent event) {
        if (mainGame != null) {
            mainGame.nextWaveAction();
        } else {
            System.err.println("MainGame is null.");
        }
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //
    public Pane getMainGamePane() {
        return mainGamePane;
    }

}
