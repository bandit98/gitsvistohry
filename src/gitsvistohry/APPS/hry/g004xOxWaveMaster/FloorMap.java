/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import java.util.ArrayList;

/**
 *
 * @author Ondra
 */
public class FloorMap implements PaintableInter {

    private FloorObj[] floorObjects;
    private Path[] paths;

    private int xSize_width;
    private int ySize_Heigh;

    /**
     * <div style="color:red;">NEDOPORUČOVÁNO</div> - lepší je vstupní String... takhle budeš muset ještě nastavit každý
     * políčko zvlášť
     *
     * @param xSize
     * @param ySize
     */
    public FloorMap(int xSize, int ySize) {
        this.xSize_width = xSize;
        this.ySize_Heigh = ySize;
        floorObjects = new FloorObj[xSize * ySize];
        int i = 0;
        for (int y = 0; y < ySize; y++) {
            for (int x = 0; x < xSize; x++) {
                floorObjects[i] = new FloorObj(x, y, FloorObjEnum.U_N_K_N_O_W_N);
                i++;
            }
        }
    }

    public FloorMap(String inputString) {
        createFloorMapByString(inputString);
    }

    /**
     * co znamená jaké písmeno je definováno v FloorObjEnum... odděluj řádky pomocí \n
     *
     * @param inputString
     * @throws UnsupportedOperationException
     */
    private void createFloorMapByString(String inputString) throws UnsupportedOperationException {
        String[] rows = inputString.split("\n");
        this.xSize_width = rows[0].length();
        this.ySize_Heigh = rows.length;
        floorObjects = new FloorObj[xSize_width * ySize_Heigh];

        int i = 0;
        int y = 0;
        for (String row : rows) {
            for (int x = 0; x < row.length(); x++) {
                floorObjects[i++] = new FloorObj(x, y, FloorObjEnum.getEnumByChar(row.charAt(x)));
            }
            y++;
        }
    }

    public void setFloorObj(int realX, int realY, FloorObj newObj) {
        for (int i = 0; i < floorObjects.length; i++) {
            if (floorObjects[i].getX() == realX && floorObjects[i].getY() == realY) {
                floorObjects[i] = newObj;
                return;
            }
        }
        System.err.println("podlaháá s x,y:" + realX + "," + realY + " nebyla nalezena, nemůžu jí na nic nastavit....");
    }
    
    public FloorObj getFloorObj(int x, int y) {
        for (int i = 0; i < floorObjects.length; i++) {
            if (floorObjects[i].getX() == x && floorObjects[i].getY() == y) {
                return floorObjects[i];
            }
        }
        System.err.println("podlaháá s x,y:" + x + "," + y + " nebyla nalezena, nemůžu jí vrátit.... vracim null");
        return null;
    }

    public static FloorMap getDefaultFloorMap() {
        return FloorMaps.getFloormap(0);
    }

    @Override
    public PaintableObject[] getPaintableObjects() {
        return floorObjects;
    }

    public long getBigStartingX() throws Exception {
        for (int i = 0; i < floorObjects.length; i++) {
            if (floorObjects[i].getFlObjEnum()== FloorObjEnum.ENEMY_START) {
                return floorObjects[i].getX();
            }
        }
        System.err.println("Nenašel jsem startovací X");
        Thread.sleep(700);
        throw new Exception();
    }

    public long getBigStartingY() throws Exception {
        for (int i = 0; i < floorObjects.length; i++) {
            if (floorObjects[i].getFlObjEnum()== FloorObjEnum.ENEMY_START) {
                return floorObjects[i].getY();
            }
        }
        System.err.println("Nenašel jsem startovací Y");
        Thread.sleep(700);
        throw new Exception();
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    public int getxSize_width() {
        return xSize_width;
    }

    public int getySize_Heigh() {
        return ySize_Heigh;
    }

    public static void main(String[] args) {
        
        FloorMap flMap = getDefaultFloorMap();
        
        Path[] paths=Path.findPaths(flMap);
        
        for (Path path : paths) {
            System.out.println(path);
        }

        int satan = 666;
    }
}
