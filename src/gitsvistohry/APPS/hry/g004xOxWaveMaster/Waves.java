/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.e001BasicEnemy;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.e002BasicEnemyBigger;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.e003SmallBrick;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.e004BigBrick;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ondra
 */
public class Waves {

    public static List<Wave> getWavesList(Game game, int numberOfWaveList) {
        switch (numberOfWaveList) {
            case 0:
                List<Wave> wavesList = new ArrayList<>();

                Wave wv1 = new Wave(4000);
                wv1.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY / 10);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                for (int i = 0; i < 60; i++) {
                    wv1.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                }

                Wave wv2 = new Wave(3000);
                wv2.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY / 10);
                wv2.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY + 3000);
                wv2.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY + 3000);

                Wave wv3 = new Wave(1000);
                wv3.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY / 10);
                wv3.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY + 3000);
                wv3.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY + 3000);

                Wave wv4 = new Wave(2000);
                wv4.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY / 10);
                wv4.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv4.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);

                Wave wv5 = new Wave(7000);
                wv5.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY / 10);
                wv5.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv5.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv5.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv5.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv5.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);

                Wave wv6 = new Wave(9000);
                wv6.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY / 10);
                wv6.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv6.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv6.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv6.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv6.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);

                wavesList.add(wv1);
                wavesList.add(wv2);
                wavesList.add(wv3);
                wavesList.add(wv4);
                wavesList.add(wv5);
                wavesList.add(wv6);
                return wavesList;
            case -1:
                List<Wave> wavesList_ = new ArrayList<>();

                Wave wv1_ = new Wave(4000);
                wv1_.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv1_.addEnemy(new e001BasicEnemy(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);

                Wave wv2_ = new Wave(5000);
                wv2_.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv2_.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);

                Wave wv3_ = new Wave(5000);
                wv3_.addEnemy(new e003SmallBrick(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                wv3_.addEnemy(new e003SmallBrick(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);

                Wave wv4_ = new Wave(7000);
                wv4_.addEnemy(new e004BigBrick(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                for (int i = 0; i < 100; i++) {
                    wv4_.addEnemy(new e004BigBrick(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                }
                
                Wave wv5_ = new Wave(5000);
                wv5_.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                for (int i = 0; i < 100; i++) {
                    wv5_.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                }
                
                Wave wv6_ = new Wave(7000);
                wv6_.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
                for (int i = 0; i < 230; i++) {
                    wv6_.addEnemy(new e002BasicEnemyBigger(0, 0, game, null), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY/7);
                }
                
                

                wavesList_.add(wv1_);
                wavesList_.add(wv2_);
                wavesList_.add(wv3_);
                wavesList_.add(wv4_);
                wavesList_.add(wv5_);
                wavesList_.add(wv6_);
                return wavesList_;
        }
        System.err.println("Tato wave neexistuje [" + numberOfWaveList + "].. vracím null");
        return null;
    }
}
