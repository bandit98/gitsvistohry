/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.DefenceTower;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.AbstractFire;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.f002Rocket;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Game;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Path;

/**
 *
 * @author Ondra
 */
public class t003Launcher extends  DefenceTower{
    
    {
        this.health*=50;
        this.primaryAttackReloadTicks*=4;
        this.primaryAttackDamage*=20;
        this.priceOfDT*=7;
    }
    
    public t003Launcher(long x, long y, Game game, Path path) {
        super(x, y, game, path);
    }

    @Override
    protected AbstractFire createAndGetMyFire(Entity entityToAttack) {
        return new f002Rocket(primaryAttackDamage, this, entityToAttack);
    }
    
}
