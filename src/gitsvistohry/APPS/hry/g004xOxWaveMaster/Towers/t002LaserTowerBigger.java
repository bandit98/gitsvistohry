/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.DefenceTower;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.*;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Game;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Path;

/**
 *
 * @author Ondra
 */
public class t002LaserTowerBigger extends DefenceTower {

    {
        primaryAttackDamage = (long) (primaryAttackDamage * 2.5);
    }

    public t002LaserTowerBigger(long x, long y, Game game, Path path) {
        super(x, y, game, path);
    }

    @Override
    protected AbstractFire createAndGetMyFire(Entity entityToAttack) {
        return new f001LaserFire(primaryAttackDamage, this, entityToAttack);
    }
    
    public static void main(String[] args) {
         t002LaserTowerBigger tw = new t002LaserTowerBigger(4, 6, null, null);
         
         DefenceTower dt = tw.getNewInstanceOfMyself(0, 0, null, null);
         
         double satan = 666;
    }
}
