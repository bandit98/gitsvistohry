package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.e001BasicEnemy;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.AbstractFire;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers.t001LaserTower;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Ondra
 */
public abstract class Entity extends PaintableObject {

    protected Game game;
    protected Path path;
    protected int pathProgress = 0;//kolik kostiček už sem ušel

    protected List<PaintableObjectEnum> entitiesIWantToAttack = new ArrayList<>();

    protected List<Entity> entitiesIKilled = new LinkedList<>();
    protected List<AbstractFire> firesFired = new LinkedList<>();

    private final List<AbstractFire> firesToRemove = new LinkedList<>();

    protected long primaryAttackRange = SETTINGS.POLICKO_LENGTH * 4; //může střílet v rádiusu 4 políček
    protected long primaryAttackDamage = SETTINGS.BASIC_DEFAULT_ATTACK_DAMAGE; //nastaví defaultní útok
    protected long primaryAttackReloadTicks = SETTINGS.BASIC_DEFAULT_ATTACK_RELOAD_TICKS; //nastaví defaultní útok
    protected long health = SETTINGS.BASIC_DEFAULT_MAX_HEALTH; //
    protected long maxHealth = health; //
    protected long speedPerTick = SETTINGS.DEFAULT_SPEED_PER_TICK; //

    private boolean rightNowChangingX = false;

    private int ticksAfterLastShooting;

    public Entity(long x, long y, Game game, Path path) {
        super(x, y);
        this.game = game;
        this.path = path;

        maxHealth = health;
    }

    protected abstract AbstractFire createAndGetMyFire(Entity entityToAttack);

    /**
     * vrátí true pokud byla entita zásahem zabita
     *
     * @param attackpower
     */
    public boolean getHit(long attackpower) {
        if (isAlive()) {
            this.health -= attackpower;
            if (!isAlive()) {
                die();
            }
        }
        return !isAlive();
    }

    public void die() {
        primaryAttackDamage = 0;
        speedPerTick = 0;
        primaryAttackRange = 0;
        this.game.removeEntityFromAllEntitiesList(this);
        if(this instanceof Enemy){
            game.getPlayer().addScore(((Enemy)this).scoreToGet);
            game.getPlayer().addMoney(((Enemy)this).scoreToGet);
        }
    }

    protected void attack(Entity entityToAttack) {
        if (entityToAttack != null && entityToAttack.isAlive()) {
//////////////////////            System.out.println("Attacking " + entityToAttack);
            firesFired.add(createAndGetMyFire(entityToAttack));
        } else {
            if (SETTINGS.debugging) {
                System.err.println("Nemám na koho útočit. [jsem : " + this + "]");
            }
        }
    }

    public void tick() {
        tickMoveThis();
        tickAutoAttacking();
        tickAllFires();
    }

    private void tickAutoAttacking() {
        ticksAfterLastShooting += ticksAfterLastShooting >= primaryAttackReloadTicks ? 0 : 1;
        if (ticksAfterLastShooting >= primaryAttackReloadTicks) {
            ticksAfterLastShooting = 0;
            attack(findClosestEntityToAttack());
        }
    }

    protected void tickAllFires() {
        for (AbstractFire fire : firesFired) {
            if (fire.tick()) {// true - střela už je hotová
                firesToRemove.add(fire);
            }
        }
        for (int i = 0; i < firesToRemove.size(); i++) {
            firesFired.remove(firesToRemove.get(i));
            firesToRemove.remove(i);
        }
    }

    private void tickMoveThis() {
        if (path != null) {
            if (!isFinishedPath()) {
                path.setNextAutomaticCoords(this);
            }
        } else {
            if (this.getObjectEnum().isShouldIMove()) {
                //pokud není nastavena Path, zkusím si jí najít
                int satan = 666;
                if (game != null) {
                    if (game.getLevel().getPossiblePaths() != null) {
                        System.out.println("Beru si náhodnou Path.. jsem [" + this + "]");
                        path = game.getLevel().getPossiblePaths()[new Random().nextInt(game.getLevel().getPossiblePaths().length)];
                    } else {
                        System.err.println("tohle je zlý");
                    }
                } else {
                    System.err.println("tohle je ještě horší...");
                }
            }
        }
    }

    //------------------------------------------//------------------------------------------//--------------------
    //------------------------------------------//------------------------------------------//--------------------
    //------------------------------------------//------------------------------------------//--------------------
    //------------------------------------------//------------------------------------------//--------------------
    //------------------------------------------//------------------------------------------//--------------------
    //------------------------------------------//------------------------------------------//--------------------
    //------------------------------------------//------------------------------------------//--------------------
    //
    public final boolean isAlive() {
        return this.health > 0;
    }

    /**
     *
     * @return vrátí entitu která je v Range, je to někdo kdo je v listu těch po kom chci
     * střílet (enum), a je nejbližší.. pokud nenajde =>null
     */
    public Entity findClosestEntityToAttack() {
        Entity tmpEntity = null;
        long lastDistance = Long.MAX_VALUE;
        for (Entity entity : game.getAllEntitesInGameList()) {
            if (entitiesIWantToAttack.contains(entity.getObjectEnum())
                    && entity.isAlive()
                    && isInMyRange(entity)
                    && computeRealDistance(this, entity) < lastDistance) {
                tmpEntity = entity;
                lastDistance = computeRealDistance(this, entity);
            }
        }
        return tmpEntity;
    }

    public static long computeRealDistance(Entity from, Entity to) {
        return (long) Math.hypot(from.getX() - to.getX(), from.getY() - to.getY());
    }

    public boolean isInMyRange(Entity enemyEntity) {
        return Math.hypot(this.getX() - enemyEntity.getX(), this.getY() - enemyEntity.getY())
                <= this.primaryAttackRange;
    }

    //------------------------------------------//------------------------------------------//--------------------
    //------------------------------------------//------------------------------------------//--------------------
    public long getRange() {
        return primaryAttackRange;
    }

    public void setRange(long range) {
        this.primaryAttackRange = range;
    }

    public long getPrimaryAttackDamage() {
        return primaryAttackDamage;
    }

    public void setPrimaryAttackDamage(long primaryAttackDamage) {
        this.primaryAttackDamage = primaryAttackDamage;
    }

    public long getPrimaryAttackReloadTicks() {
        return primaryAttackReloadTicks;
    }

    public void setPrimaryAttackReloadTicks(long primaryAttackReloadTicks) {
        this.primaryAttackReloadTicks = primaryAttackReloadTicks;
    }

    public long getHealth() {
        return health;
    }

    public long getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(long maxHealth) {
        this.maxHealth = maxHealth;
    }

    public long getSpeed() {
        return speedPerTick;
    }

    public void setSpeed(long speed) {
        this.speedPerTick = speed;
    }

    public List<Entity> getEntitiesIKilled() {
        return entitiesIKilled;
    }

    public List<AbstractFire> getFiresFired() {
        return firesFired;
    }

    public long getPrimaryAttackRange() {
        return primaryAttackRange;
    }

    public void setPrimaryAttackRange(long primaryAttackRange) {
        this.primaryAttackRange = primaryAttackRange;
    }

    public long getSpeedPerTick() {
        return speedPerTick;
    }

    public void setSpeedPerTick(long speedPerTick) {
        this.speedPerTick = speedPerTick;
    }

    public Path getPath() {
        return path;
    }

    public int getPathProgress() {
        return pathProgress;
    }

    public void setPathProgress(int pathProgress) {
        this.pathProgress = pathProgress;
    }

    public final boolean isRightNowChangingX() {
        return rightNowChangingX;
    }

    public final void setRightNowChangingX(boolean rightNowChangingX) {
        this.rightNowChangingX = rightNowChangingX;
    }

    private boolean finishedPath = false;

    public void incrementPathProgress() {
        if (pathProgress < path.getSteps().size() - 2) {
            this.pathProgress++;
        } else {
            finishedPath = true;
        }
    }

    public boolean isFinishedPath() {
        return finishedPath;
    }

    @Override
    public String toString() {
        return "Entity{" + "x=" + x + ", y=" + y + ",range=" + primaryAttackRange + ", primaryAttackDamage=" + primaryAttackDamage
                + ", primaryAttackReloadMilis=" + primaryAttackReloadTicks + ", health=" + health
                + ", maxHealth=" + maxHealth + ", speed=" + speedPerTick + ", hash: " + this.hashCode() + '}';
    }

    public static void main(String[] args) {
        int x = 1000000;

        Game game = new Game();
        e001BasicEnemy e01 = new e001BasicEnemy(x * 0, 0, game, null);
        e001BasicEnemy e02 = new e001BasicEnemy(x * 9, 0, game, null);
        t001LaserTower t01 = new t001LaserTower(x * 10, 0, game, null);
        t001LaserTower t02 = new t001LaserTower(x * 10, 0, game, null);

        System.out.println(e01.getObjectEnum().name());
        System.out.println(e01);

        System.out.println("Closest: " + e01.findClosestEntityToAttack());
        System.out.println("Closest: " + e02.findClosestEntityToAttack());
    }

}
