/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

/**
 *
 * @author Ondra
 */
public abstract class DefenceTower extends Entity {

    protected int towerLevel;
    protected long priceOfDT = SETTINGS.DEFAULT_DEFENTE_TOWER_PRICE;

    {
        this.objectEnum = PaintableObjectEnum.DEFENCE_TOWER;
        entitiesIWantToAttack.add(PaintableObjectEnum.ENEMY);
    }

    public DefenceTower(long x, long y, Game game, Path path) {
        super(x, y, game, path);
        health = SETTINGS.BASIC_DEFAULT_MAX_HEALTH * 10;
    }

    public DefenceTower getNewInstanceOfMyself(long x, long y, Game game, Path path) {
        try {
            return getClass().getConstructor(Long.TYPE,Long.TYPE,Game.class,Path.class).newInstance(x, y, game, path);
        } catch (Exception ex) {
            System.err.println("getNewInstanceOfMyself getNewInstanceOfMyself getNewInstanceOfMyself "
                    + "getNewInstanceOfMyself getNewInstanceOfMyself getNewInstanceOfMyself getNewInstanceOfMyself");
            System.err.println(ex);
            return null;
        }
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    public int getTowerLevel() {
        return towerLevel;
    }

    public void setTowerLevel(int towerLevel) {
        this.towerLevel = towerLevel;
    }

    public long getPriceOfDT() {
        return priceOfDT;
    }

    public void setPriceOfDT(long priceOfDT) {
        this.priceOfDT = priceOfDT;
    }
}
