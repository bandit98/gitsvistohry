/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;
import javafx.scene.image.Image;

/**
 *
 * @author Ondra
 */
public final class SETTINGS {

    public static final long POLICKO_LENGTH = 1000000;//1000000
    public static final double ENEMY_SIZE__policko_part = 0.61;//0.61
    public static final long DEFAULT_SPEED_PER_TICK = 13000;  //BYLO BY HODNĚ FAJN KDYBY TO BYLO DĚLITELNÝ POLICKO_LENGTH.. až bude hra hotová můžeš to zkusit bez toho, t bude cool
    public static final long DEFAULT_WAIT_BEFORE_NEXT_ENEMY = 800;//800 
    public static final long DEFAULT_SCORE_TO_GET_FROM_ENEMY = 1; //1
    public static final long DEFAULT_DEFENTE_TOWER_PRICE = 10; //10
    
    public static final long TICKS_PER_SECOND = 64;//64
    public static final long MILIS_TIME_FOR_ONE_TICK = (1000 / TICKS_PER_SECOND) + 1;

    public static final long BASIC_DEFAULT_MAX_HEALTH = 100;//100
    public static final long BASIC_DEFAULT_ATTACK_DAMAGE = 8;//8
    public static final long BASIC_DEFAULT_ATTACK_RELOAD_TICKS = TICKS_PER_SECOND/2; //tps/2
    
    public static final boolean debugging=false;//jestli mam vypisovat úplně všechny p&#@#[iny
    
    public static final Image towerBuildNodeIMAGE =ImageMyMy.getImage(Settings.APP_FOLDER_GAME,"g004xOxWaveMaster","TowerBuildNode.png");
    public static final Image towerBuildNodeIMAGEActive =ImageMyMy.getImage(Settings.APP_FOLDER_GAME,"g004xOxWaveMaster","TowerBuildNodeActive.png");


    private SETTINGS() {
    }
    //todo:
    //žebříček skóre - hlavně aby si člověk mohl zvolit méno
    //upgradey - range, damage, rate
}
