/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.PlayerInfo.AxPlayerInfoNode;
import javafx.application.Platform;
import javafx.scene.layout.Pane;

/**
 *
 * @author Ondra
 */
public class MainGame {

    private Game game;
    private static MyGameThread myGameThread;

    public static boolean running = true;
    private boolean isPAUSE = false;

    private int totalTickCount = 0;

    private FXMLController fxmlCont;

    private AxPlayerInfoNode plInfoNode;

    public MainGame(Pane paneParent) {
        game = new Game();
        myGameThread = new MyGameThread();
        paneParent.getChildren().add(game.getMyGameCanvas());
        game.getMyGameCanvas().createAndBindCanvas();
        game.setLevel(Levels.getLevels(game)[0]);
    }

    public void run(FXMLController fxmlCont) {
        this.fxmlCont = fxmlCont;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                myGameThread.start();
            }
        });

        plInfoNode = null;
        plInfoNode = new AxPlayerInfoNode(game, game.getMyGameCanvas().getNewPlayerInfoRequiredX_Propperty(), game.getMyGameCanvas().getPlayerInfoRequiredY(), MyGameCanvas.getPlayerInfoRequiredWidth(), MyGameCanvas.getPlayerInfoRequiredHeight());
        fxmlCont.getMainGamePane().getChildren().add(plInfoNode);
        game.setplayerInfoNode(plInfoNode);

    }

    void nextWaveAction() {
        game.nextWaveAction();
    }

    public class MyGameThread extends Thread {

        {
            this.setDaemon(true);
            running = false;
        }

        @Override
        public void run() {
            // ! - > přidat 
            int frames = 0;
            double unprocessedSeconds = 0;
            long previousTime = System.nanoTime();

            double secondsForEachTick = 1 / (60.0);

            long tickCount = 0; //RENDER COUNTER
            boolean ticked = false;

            long currentTime = 0;
            long passedTime = 0;
            running = true;
            while (running) {
                currentTime = System.nanoTime();
                passedTime = currentTime - previousTime;

                previousTime = currentTime;

                unprocessedSeconds = unprocessedSeconds + passedTime / 1000000000.0;

                int count = 0;
                while (unprocessedSeconds > secondsForEachTick) {
                    tick();
                    count++;
                    unprocessedSeconds -= secondsForEachTick;

                    ticked = true;
                    tickCount++;
                    totalTickCount++;

                    if (tickCount % 60 == 0) {
//                        System.out.println(frames + " fps");
//                        MainSnakeGame.fps = frames;
                        previousTime += 1000;
                        frames = 0;
                    }
                }
//                System.out.println("proběhlo loopů ticků za 1 while: " + count);
                if (ticked) {
                    Platform.runLater(() -> {
                        render();
                    });
                    frames++;
                    ticked = false;
                }
                while (isPAUSE) {
                    try {
                        double satan = 666; //protože proto.
                        Thread.sleep(500);
                    } catch (InterruptedException ex) {
//                            Logger.getLogger(MainSnakeGame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                previousTime = System.nanoTime();
            }
        }
    }

    private void tick() {
        game.tickEverything();
    }

    private void render() {
//        System.out.println("rendering");
        game.getMyGameCanvas().render();
        plInfoNode.actualize();
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //
    public Game getGame() {
        return game;
    }

    public FXMLController getFxmlCont() {
        return fxmlCont;
    }

}
