/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Ondra
 */
public class Path {

    private List<Integer[]> steps = new LinkedList<>();//integer[0] => x,integer[1] => y

    private boolean repeat = false;//jestli má ta věc po dokončení cesty jít zase zpátky a zase na konec a zase....
    private static final int maxPathsToLookFor = 1000;

    public Path() {
    }

    public Path(List<Integer[]> steps) {
        List<Integer[]> stepss = new LinkedList<>();
        for (Integer[] step : steps) {
            stepss.add(new Integer[]{step[0], step[1]});
        }
        this.steps = stepss;
    }

    /**
     * nastaví souřadnice na další v pořadí - s ohledem na Speed a aktuální souradnice.
     *
     * @param entity
     */
    public void setNextAutomaticCoords(Entity entity) {
        moveEntity(entity, entity.getSpeed());
    }

    private void moveEntity(Entity entity, long howFar) {

        if (entity.isRightNowChangingX()) { //tady je chyba.. chce to zjistit jestli se má hejbat vlastně po X nebo Y
            //budu měnit X
            if ((entity.getX() - (MyGameCanvas.bigXtoRealX(steps.get(entity.getPathProgress() + 1)[0]) + (SETTINGS.POLICKO_LENGTH / 2)))
                    < 0) {
                //X do +
                entity.setX(entity.getX() + howFar);
                if (isVerticallyOk(entity, true)) {
                    entity.incrementPathProgress();
                    entity.setRightNowChangingX(!(steps.get(entity.getPathProgress() + 1)[0] == MyGameCanvas.realXtoBigX(entity.getX())));
                }
            } else {
                //X do -
                entity.setX(entity.getX() - howFar);
                if (isVerticallyOk(entity, false)) {
                    entity.incrementPathProgress();
                    entity.setRightNowChangingX(!(steps.get(entity.getPathProgress() + 1)[0] == MyGameCanvas.realXtoBigX(entity.getX())));

                }
            }
        } else //budu měnit Y
        {
            if ((entity.getY() - (MyGameCanvas.bigYtoRealY(steps.get(entity.getPathProgress() + 1)[1]) + (SETTINGS.POLICKO_LENGTH / 2))) < 0) {
                //Y do +
                entity.setY(entity.getY() + howFar);
                if (isHorizontallyOk(entity, true)) {
                    entity.incrementPathProgress();
                    entity.setRightNowChangingX(!(steps.get(entity.getPathProgress() + 1)[0] == MyGameCanvas.realXtoBigX(entity.getX())));

                }
            } else {
                //Y do -
                entity.setY(entity.getY() - howFar);
                if (isHorizontallyOk(entity, false)) {
                    entity.incrementPathProgress();
                    entity.setRightNowChangingX(!(steps.get(entity.getPathProgress() + 1)[0] == MyGameCanvas.realXtoBigX(entity.getX())));

                }
            }
        }
    }

    private boolean isHorizontallyOk(Entity entity, boolean goingPlus) {
        int znaminko = goingPlus ? 1 : -1;
        return entity.getY() * znaminko > (MyGameCanvas.bigYtoRealY(steps.get(entity.getPathProgress() + 1)[1]) + (SETTINGS.POLICKO_LENGTH / 2)) * znaminko;
    }

    private boolean isVerticallyOk(Entity entity, boolean goingPlus) {
        int znaminko = goingPlus ? 1 : -1;
        return entity.getX() * znaminko > (MyGameCanvas.bigXtoRealX(steps.get(entity.getPathProgress() + 1)[0]) + (SETTINGS.POLICKO_LENGTH / 2)) * znaminko;
    }

    public static Path[] findPaths(FloorMap floorMap) {
        List<Path> listOfOkPaths = new ArrayList<>();
        Path path = new Path();

        int bigY;
        int bigX;
        try {
            bigX = (int) floorMap.getBigStartingX();
            bigY = (int) floorMap.getBigStartingY();
        } catch (Exception ex) {
            System.err.println("nemůžu najít ani startovací bod. vracím null (findPaths)");
            return null;
        }

        boolean[][] whereIWasField = new boolean[floorMap.getxSize_width()][floorMap.getySize_Heigh()];

        System.out.println("Finding paths:");
        innerFindPaths(bigX, bigY, floorMap, whereIWasField, path, listOfOkPaths, false);
        System.out.println("Found paths : "+listOfOkPaths.size());

        Path[] pathsList = new Path[listOfOkPaths.size()];
        int i = 0;
        for (Path okPath : listOfOkPaths) {
            pathsList[i++] = okPath;
        }

        return pathsList;
    }

    private static void innerFindPaths(int bigX, int bigY, FloorMap floorMap, boolean[][] whereIWasField, Path path, List<Path> listOfOkPaths, boolean createYourOwnWhereIWasField) {
        if (listOfOkPaths.size() > maxPathsToLookFor) {
            System.err.println("Příliš mnoho možných cest. končím. [" + maxPathsToLookFor + "]");
            return;
        }
        if (createYourOwnWhereIWasField) {
            whereIWasField = myDeepMatrixClone(whereIWasField);
        }
        if (canIGoThere(bigX, bigY, floorMap, whereIWasField)) {
            whereIWasField[bigX][bigY] = true;

            path.addStep(new Integer[]{bigX, bigY});
            if (floorMap.getFloorObj(bigX, bigY).getFlObjEnum() == FloorObjEnum.ENEMY_TARGET) {
                listOfOkPaths.add(path);
                System.out.println("found Path: " + path);
                return;
            }

            int crossroadsCount = 0;//pokud je víc možných dalších cest musím pro každou vytvořit nový boolean pole
            crossroadsCount += canIGoThere(bigX, bigY + 1, floorMap, whereIWasField) ? 1 : 0;
            crossroadsCount += canIGoThere(bigX + 1, bigY, floorMap, whereIWasField) ? 1 : 0;
            crossroadsCount += canIGoThere(bigX, bigY - 1, floorMap, whereIWasField) ? 1 : 0;
            crossroadsCount += canIGoThere(bigX - 1, bigY, floorMap, whereIWasField) ? 1 : 0;
            boolean y = crossroadsCount > 1;

            innerFindPaths(bigX, bigY + 1, floorMap, whereIWasField, y ? new Path(path.getSteps()) : path, listOfOkPaths, y);
            innerFindPaths(bigX + 1, bigY, floorMap, whereIWasField, y ? new Path(path.getSteps()) : path, listOfOkPaths, y);
            innerFindPaths(bigX, bigY - 1, floorMap, whereIWasField, y ? new Path(path.getSteps()) : path, listOfOkPaths, y);
            innerFindPaths(bigX - 1, bigY, floorMap, whereIWasField, y ? new Path(path.getSteps()) : path, listOfOkPaths, y);
        }
    }

    private static boolean canIGoThere(int bigX, int bigY, FloorMap floorMap, boolean[][] whereIWas) {
        if (bigX < 0 || bigY < 0 || bigX >= floorMap.getxSize_width() || bigY >= floorMap.getySize_Heigh()) {
            return false;
        }
        return !whereIWas[bigX][bigY] && floorMap.getFloorObj(bigX, bigY).getFlObjEnum().isWalkable();
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //.
    private static boolean[][] myDeepMatrixClone(boolean[][] whereIWasField) {
        boolean[][] newIWasField = new boolean[whereIWasField.length][];
        for (int i = 0; i < whereIWasField.length; i++) {
            newIWasField[i] = whereIWasField[i].clone();
        }
        return newIWasField;
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public List<Integer[]> getSteps() {
        return steps;
    }

    public boolean addStep(Integer[] e) {
        return steps.add(e);
    }

    @Override
    public String toString() {
        String tmp = "";
        for (Integer[] step : steps) {
            tmp += "x:" + step[0] + ", y:" + step[1] + ",    ";
        }
        return "Path{" + " repeat=" + repeat + ", steps=" + tmp + '}';
    }

}
