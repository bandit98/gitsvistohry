/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

/**
 *
 * @author Ondra
 */
public enum FloorObjEnum {
    U_N_K_N_O_W_N(false, false, 'Z'),
    WALKABLE(false, true, 'W'),
    BUILDABLE(true, false, 'B'),
    WTF(true, true, 'X'),
    ENEMY_START(false, true, 'S'),
    ENEMY_TARGET(false, true, 'T'),
    UNREACHABLE(false, false, 'U'),;

    private final boolean buildable;
    private final boolean walkable;
    private final char myChar;

    private FloorObjEnum(boolean buildable, boolean walkable, char myChar) {
        this.buildable = buildable;
        this.walkable = walkable;
        this.myChar = myChar;
    }

    public boolean isBuildable() {
        return buildable;
    }

    public boolean isWalkable() {
        return walkable;
    }

    public char getMyChar() {
        return myChar;
    }

    public static FloorObjEnum getEnumByChar(char lookingForChar) {
        for (FloorObjEnum en : FloorObjEnum.values()) {
            if (en.getMyChar() == lookingForChar) {
                return en;
            }
        }
        return FloorObjEnum.U_N_K_N_O_W_N;
    }

}
