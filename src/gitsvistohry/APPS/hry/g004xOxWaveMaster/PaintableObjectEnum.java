/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

/**
 *
 * @author Ondra
 */
public enum PaintableObjectEnum {
    U_N_K_N_O_W_N(false),
    DEFENCE_TOWER(false),
    ENEMY(true),
    FLOOR(false),;

    boolean shouldIMove;

    private PaintableObjectEnum(boolean shouldIMove) {
        this.shouldIMove = shouldIMove;
    }

    public boolean isShouldIMove() {
        return shouldIMove;
    }

}
