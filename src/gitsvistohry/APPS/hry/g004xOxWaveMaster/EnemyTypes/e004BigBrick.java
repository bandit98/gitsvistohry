/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Game;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Path;

/**
 *
 * @author Ondra
 */
public class e004BigBrick extends  e003SmallBrick{
    
    {
        this.speedPerTick/=1.3;
        this.health*=1.4;
    }
    
    public e004BigBrick(long x, long y, Game game, Path path) {
        super(x, y, game, path);
    }
    
}
