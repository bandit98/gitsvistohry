/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Enemy;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.AbstractFire;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.f001LaserFire;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Game;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Path;

/**
 *
 * @author Ondra
 */
public class e003SmallBrick extends Enemy {
    
    {
        this.speedPerTick*=3;
        this.health/=7;
    }

    public e003SmallBrick(long x, long y, Game game, Path path) {
        super(x, y, game, path);
    }

    @Override
    protected AbstractFire createAndGetMyFire(Entity entityToAttack) {
        return new f001LaserFire(primaryAttackDamage, this, entityToAttack);
    }
    
}
