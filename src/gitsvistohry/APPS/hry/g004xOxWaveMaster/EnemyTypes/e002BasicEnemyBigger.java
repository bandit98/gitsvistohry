/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Enemy;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.*;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Game;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Path;

/**
 *
 * @author Ondra
 */
public class e002BasicEnemyBigger extends Enemy {

    {
        primaryAttackDamage=(long) (primaryAttackDamage*1.4);
        health=(long) (health*1.5);
        maxHealth=health;//!!!!!!!!!!!!
        speedPerTick=(long) (speedPerTick*1.13);
    }
    
    public e002BasicEnemyBigger(long x, long y, Game game, Path path) {
        super(x, y, game, path);
    }
    
    @Override
    protected AbstractFire createAndGetMyFire(Entity entityToAttack) {
        return new f001LaserFire(primaryAttackDamage, this, entityToAttack);
    }
}
