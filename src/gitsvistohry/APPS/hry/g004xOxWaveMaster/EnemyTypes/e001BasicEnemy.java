/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.*;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.AbstractFire;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.*;

/**
 *
 * @author Ondra
 */
public class e001BasicEnemy extends Enemy {

    public e001BasicEnemy(long x, long y, Game game, Path path) {
        super(x, y, game, path);
    }

@Override
    protected AbstractFire createAndGetMyFire(Entity entityToAttack) {
        return new f001LaserFire(primaryAttackDamage, this, entityToAttack);
    }
}
