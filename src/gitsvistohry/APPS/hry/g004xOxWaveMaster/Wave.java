/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import java.util.LinkedList;
import java.util.List;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.*;
import java.util.Queue;
import java.util.Random;

/**
 * jak používat ==> při každém průchodu spustit metodu Tick() .. a potom si sebrat pole
 * enemies READY to roll
 *
 * @author Ondra
 */
public class Wave {

    static Random rnd = new Random();

    private final Queue<Enemy> queueOfEnemies;
    private final Queue<Long> queueWaitBeforeEnemyMilis;   //čas který se počká než se vypustí další nepřídel ve wawece

    private List<Enemy> listEnemiesREADYToRoll;

    private long lastMilisTime;

    private int waitBeforeWave;
    private int originalWaitBeforeWave;
    private boolean isFirstTick=true;

    public Wave(int waitBeforeWave) {
        queueOfEnemies = new LinkedList<>();
        queueWaitBeforeEnemyMilis = new LinkedList<>();
        listEnemiesREADYToRoll = new LinkedList<>();
        lastMilisTime = System.currentTimeMillis();
        this.waitBeforeWave = waitBeforeWave;
        this.originalWaitBeforeWave = waitBeforeWave;
    }

//    public Wave(/*Enemy... enemies*/) {
//        this();
////        for (Enemy enemy : enemies) {
////            queueOfEnemies.add(enemy);
////        }
//    }
    public void addEnemy(Enemy newEnemyToAdd, long waitBeforeEnemyMilis) {
        if (queueOfEnemies.contains(newEnemyToAdd)) {
            System.err.println("TOTO ENEMY UŽ V LISTU JE. NEPŘIDÁVÁM [" + newEnemyToAdd + "]");
            return;
        }
        if (waitBeforeEnemyMilis <= 1) {
            System.err.println("waitAfterEnemyMilis nesmí být <=1 [" + waitBeforeEnemyMilis + "]");
            return;
        }
        queueOfEnemies.add(newEnemyToAdd);
        queueWaitBeforeEnemyMilis.add(waitBeforeEnemyMilis);
    }

    public List<Enemy> popEnemiesREADYToRoll() {
        if (listEnemiesREADYToRoll == null || listEnemiesREADYToRoll.isEmpty()) {
            return null;
        }
        List<Enemy> tmp = listEnemiesREADYToRoll;
        listEnemiesREADYToRoll = new LinkedList<>();
        return tmp;
    }

    public List<Enemy> peekEnemiesREADYToRoll() {
        if (listEnemiesREADYToRoll == null || listEnemiesREADYToRoll.isEmpty()) {
            return null;
        }
        return listEnemiesREADYToRoll;
    }

    /* vrátí true pokud je včechno z wawe vystrkáno. v podstatě vrací funkci isAllPushedOut();*/
    public boolean tick() {
        if(isFirstTick){
            lastMilisTime=System.currentTimeMillis();
            isFirstTick=false;
        }
        if (waitBeforeWave >= 0) {
            waitBeforeWave-=System.currentTimeMillis()-lastMilisTime;
            lastMilisTime=System.currentTimeMillis();
        } else {

            if (queueWaitBeforeEnemyMilis.size() != queueOfEnemies.size()) {
                System.err.println("neeeeee");
            }

            if (!(queueWaitBeforeEnemyMilis.isEmpty() && queueOfEnemies.isEmpty())) {
                if (System.currentTimeMillis() - lastMilisTime > queueWaitBeforeEnemyMilis.peek()) {

                    listEnemiesREADYToRoll.add(queueOfEnemies.poll());
                    queueWaitBeforeEnemyMilis.poll();
                    lastMilisTime = System.currentTimeMillis();

                    System.out.println("PUSHING ENEMY OUT");
                    tick();
                }
            }
            return isAllPushedOut();
        }
        return false;
    }

    public boolean isAllPushedOut() {
        if (queueWaitBeforeEnemyMilis.isEmpty() && queueOfEnemies.isEmpty()) {
            return listEnemiesREADYToRoll.isEmpty();
        }
        if (queueWaitBeforeEnemyMilis.isEmpty()) {
            System.err.println("jenom stackWaitAfterEnemyMilis je empty, ten druhý ne. něco se podělalo");
            return listEnemiesREADYToRoll.isEmpty();
        }
        if (queueOfEnemies.isEmpty()) {
            System.err.println("jenom stackOfEnemies je empty, ten druhý ne. něco se podělalo");
            return listEnemiesREADYToRoll.isEmpty();
        }
        return false;
    }
    
    public void comeNow_fckWaiting(){
        waitBeforeWave=-1;
    }

    public static Wave getDefaultWave(Game game) {
        Wave wv = new Wave(300);
        try {
            wv.addEnemy(new e001BasicEnemy(game.getLevel().getFloorMap().getBigStartingX(), game.getLevel().getFloorMap().getBigStartingY(), game,
                    game.getLevel().getPossiblePaths()[rnd.nextInt(game.getLevel().getPossiblePaths().length)]), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
            wv.addEnemy(new e001BasicEnemy(game.getLevel().getFloorMap().getBigStartingX(), game.getLevel().getFloorMap().getBigStartingY(), game,
                    game.getLevel().getPossiblePaths()[rnd.nextInt(game.getLevel().getPossiblePaths().length)]), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
            wv.addEnemy(new e001BasicEnemy(game.getLevel().getFloorMap().getBigStartingX(), game.getLevel().getFloorMap().getBigStartingY(), game,
                    game.getLevel().getPossiblePaths()[rnd.nextInt(game.getLevel().getPossiblePaths().length)]), SETTINGS.DEFAULT_WAIT_BEFORE_NEXT_ENEMY);
        } catch (Exception e) {
            System.err.println("něco se uuuu podělalo (getDefaultWave): " + e);
        }
        return wv;
    }

    //-----------------------------//-----------------------------//-----------------------------
    //-----------------------------//-----------------------------//-----------------------------
    //-----------------------------//-----------------------------//-----------------------------
    //-----------------------------//-----------------------------//-----------------------------
    //-----------------------------//-----------------------------//-----------------------------
    public static void main(String[] args) {

    }
}
