/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import javafx.application.Platform;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;

/**
 *
 * @author Ondra
 */
public class Player {

    private String name;
    private LongProperty money = new SimpleLongProperty(7);
    private LongProperty hp = new SimpleLongProperty(7);
    private final long maxXp;
    private LongProperty score = new SimpleLongProperty(0);

    public Player(String name, long money, long hp, long score) {
        this.name = name;
        this.money = new SimpleLongProperty(money);
        this.hp = new SimpleLongProperty(hp);
        this.score = new SimpleLongProperty(score);
        this.maxXp = hp;
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LongProperty getMoney() {
        return money;
    }

    public void addMoney(long moneyToAdd) {
        Platform.runLater(() -> {
            this.money.set(this.money.get() + moneyToAdd);
        });
    }

    public LongProperty getHp() {
        return hp;
    }

    public void setHp(LongProperty hp) {
        this.hp = hp;
    }

    public long getMaxXp() {
        return maxXp;
    }

    public LongProperty getScore() {
        return score;
    }

    public void addScore(long score) {
        Platform.runLater(() -> {
            this.score.set(this.score.get() + score);
        });
    }

}
