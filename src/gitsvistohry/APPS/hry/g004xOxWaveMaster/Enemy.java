/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

/**
 *
 * @author Ondra
 */
public abstract class Enemy extends Entity {

    {
        this.objectEnum = PaintableObjectEnum.ENEMY;
        entitiesIWantToAttack.add(PaintableObjectEnum.DEFENCE_TOWER);
        scoreToGet = SETTINGS.DEFAULT_SCORE_TO_GET_FROM_ENEMY;
    }

    protected long scoreToGet;

    public Enemy(long x, long y, Game game, Path path) {
        super(x, y, game, path);
        primaryAttackDamage /= 3;
        primaryAttackReloadTicks *=4;
    }

}
