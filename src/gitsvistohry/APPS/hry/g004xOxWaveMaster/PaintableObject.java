/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import javafx.scene.image.Image;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.*;
import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;

/**
 *
 * @author Ondra
 */
public abstract class PaintableObject implements PaintableInter {

    /**
     * X = 0 >>> vlevo ... X = hodně >>> vpravo
     */
    protected long x;
    /**
     * Y = 0 >>> nahoře ... Y = hodně >>> dole
     */
    protected long y;

    protected Image imageToPaint;
    protected PaintableObjectEnum objectEnum = PaintableObjectEnum.U_N_K_N_O_W_N;

    protected PaintableObject[] thisPaintableMyMyObject = new PaintableObject[]{this};

    public PaintableObject(long x, long y) {
        this.x = x;
        this.y = y;
    }

    {
        if (!(this instanceof FloorObj)) {//podlaha má vlastní nastavování obrázků
            findAndSetMyImage();
        }
    }

    @Override
    public PaintableObject[] getPaintableObjects() {
        return thisPaintableMyMyObject;
    }

    private void findAndSetMyImage() {
        //nastavit obrázek
        String[] path = this.getClass().getName().split("\\.");
        int u = 0;
        for (int i = 0; i < path.length; i++) {
            if (path[i].equals(Settings.APP_FOLDER_GAME.split("/")[Settings.APP_FOLDER_GAME.split("/").length - 1])) {
                u = i;
                break;
            }
        }
        String[] res = new String[path.length - (u + 1)];
        int z = 0;
        for (int i = u + 1; i < path.length - 1; i++) {
            res[z++] = path[i];
        }
        res[res.length - 1] = path[path.length - 1] + ".png";

        this.imageToPaint = ImageMyMy.getImage(Settings.APP_FOLDER_GAME, res);//4
    }

    public long getX() {
        return x;
    }

    public void setX(long x) {
        if (SETTINGS.debugging) {
            System.out.println("Changing x to " + x + "... I am " + this);
        }
        this.x = x;
    }

    public void setY(long y) {
        if (SETTINGS.debugging) {
            System.out.println("Changing y to " + y + "... I am " + this);
        }
        this.y = y;
    }

    public long getY() {
        return y;
    }

    public Image getImageToPaint() {
        return imageToPaint;
    }

    public void setImageToPaint(Image imageToPaint) {
        this.imageToPaint = imageToPaint;
    }

    public PaintableObjectEnum getObjectEnum() {
        return objectEnum;
    }

    @Override
    public String toString() {
        return "PaintableMyMyObject{" + "x=" + x + ", y=" + y + ", imageToPaint=" + imageToPaint
                + ", thisPaintableMyMyObject length=" + thisPaintableMyMyObject.length
                + '}';
    }

    public static void main(String[] args) {
        e001BasicEnemy en = new e001BasicEnemy(0, 0, null, null);
        System.out.println(en);

    }

}
