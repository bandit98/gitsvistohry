/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.DefenceTower;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Enemy;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.MyGameCanvas;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.SETTINGS;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Ondra
 */
public class f001LaserFire extends AbstractFire {

    public f001LaserFire(long attackDamage, Entity summoner, Entity targetedEntity) {
        super(attackDamage, summoner, targetedEntity);
    }

    public f001LaserFire(long attackDamage, long xFrom, long yFrom, long xTo, long yTo, Entity summoner, Entity targetedEntity) {
        super(attackDamage, xFrom, yFrom, xTo, yTo, summoner, targetedEntity);
    }

    @Override
    public void paintMyselfToCanvas(MyGameCanvas gameCanvas) {
        GraphicsContext gc = gameCanvas.getgCanvas().getGraphicsContext2D();
        gc.setFill(Color.RED);
        gc.setGlobalAlpha(summoner instanceof Enemy ? this.getProgress() * 0.8 : this.getProgress());

        if (summoner instanceof DefenceTower) {
            gc.setFill(Color.CHARTREUSE);
        }
        int posunX_s = 11;
        int posunX_t = 7;
        int posunY_s = 0;
        int posunY_t = 0;

        int vyrovnaniX_s = posunX_s / 2;
        int vyrovnaniX_t = posunX_t / 2;
        int vyrovnaniY_s = 0;
        int vyrovnaniY_t = 0;

        if (MyGameCanvas.realYtoBigY(summoner.getY()) == MyGameCanvas.realYtoBigY(targetedEntity.getY())) {//pokud je nepřítel hned vedle mě, tak aby bylo úbec něco vidět
            posunY_s = posunX_s;
            posunY_t = posunX_t;
            posunX_s = 0;
            posunX_t = 0;

            vyrovnaniY_s = vyrovnaniX_s;
            vyrovnaniY_t = vyrovnaniX_t;
            vyrovnaniX_s = 0;
            vyrovnaniX_t = 0;
        }

        gc.fillPolygon(
                new double[]{gameCanvas.realXtoRENDERX(summoner.getX())-vyrovnaniX_s,
                    gameCanvas.realXtoRENDERX(summoner.getX()) + posunX_s-vyrovnaniX_s,
                    gameCanvas.realXtoRENDERX(targetedEntity.getX())-vyrovnaniX_t,
                    gameCanvas.realXtoRENDERX(targetedEntity.getX()) + posunX_t-vyrovnaniX_t},
                new double[]{gameCanvas.realYtoRENDERY(summoner.getY())-vyrovnaniY_s,
                    gameCanvas.realYtoRENDERY(summoner.getY()) + posunY_s-vyrovnaniY_s,
                    gameCanvas.realYtoRENDERY(targetedEntity.getY())-vyrovnaniY_t,
                    gameCanvas.realYtoRENDERY(targetedEntity.getY()) + posunY_t-vyrovnaniY_t},
                4
        );
        gc.setGlobalAlpha(1);
    }

}
