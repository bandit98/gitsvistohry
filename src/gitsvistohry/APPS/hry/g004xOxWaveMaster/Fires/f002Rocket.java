/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.MyGameCanvas;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.SETTINGS;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 *
 * @author Ondra
 */
public class f002Rocket extends AbstractFire {

    private static Image img=null;

    public f002Rocket(long attackDamage, Entity summoner, Entity targetedEntity) {
        super(attackDamage, summoner, targetedEntity);
        if (img == null) {
            findAndSetMyImage();//!!!!!!!!!!!!!!!!!!!!!!!!!!!
            img = imageToPaint;
        }
        this.ticksToLive = (long) (SETTINGS.TICKS_PER_SECOND * 0.73);
    }

    @Override
    public void paintMyselfToCanvas(MyGameCanvas canvas) {

        double partOf1Policko = 0.7;

        double w = canvas.realXtoRENDERX(SETTINGS.POLICKO_LENGTH) * partOf1Policko;
        double h = canvas.realYtoRENDERY(SETTINGS.POLICKO_LENGTH) * partOf1Policko;

        long x = (long) ((xFrom + (-xFrom + targetedEntity.getX()) * this.getProgress()) - (SETTINGS.POLICKO_LENGTH * partOf1Policko) / 2);
        long y = (long) ((yFrom + (-yFrom + targetedEntity.getY()) * this.getProgress()) - (SETTINGS.POLICKO_LENGTH * partOf1Policko) / 2);

        GraphicsContext gc = canvas.getgCanvas().getGraphicsContext2D();

        gc.drawImage(this.img, canvas.realXtoRENDERX(x), canvas.realYtoRENDERY(y), w, h);

    }

}
