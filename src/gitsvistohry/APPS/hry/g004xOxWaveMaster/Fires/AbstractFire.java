/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.*;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.*;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers.*;
import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;
import javafx.scene.image.Image;

/**
 *
 * @author Ondra
 */
public abstract class AbstractFire {
    
    protected Image imageToPaint;

    protected long attackDamage;
    protected long xFrom;
    protected long yFrom;
    protected long xTo;
    protected long yTo;
    protected Entity summoner;
    protected Entity targetedEntity;//nemusí zasáhnout jen jeho...

    private boolean fired = false;
    private long ticksAlive = 0;
    protected long ticksToLive = SETTINGS.TICKS_PER_SECOND / 2; //měla by se přenastavit v konstruktoru

    public abstract void paintMyselfToCanvas(MyGameCanvas canvas);

    public AbstractFire(long attackDamage, Entity summoner, Entity targetedEntity) {
        this(attackDamage, summoner.getX(), summoner.getY(), targetedEntity.getX(),
                targetedEntity.getY(), summoner, targetedEntity);
    }

    public AbstractFire(long attackDamage, long xFrom, long yFrom, long xTo, long yTo,
            Entity summoner, Entity targetedEntity) {
        this.attackDamage = attackDamage;
        this.xFrom = xFrom;
        this.yFrom = yFrom;
        this.xTo = xTo;
        this.yTo = yTo;
        this.summoner = summoner;
        this.targetedEntity = targetedEntity;

        this.ticksToLive = (long) (summoner.getPrimaryAttackReloadTicks() * 0.9);

        this.fire();
    }

    public final void fire() {
        if (!fired && targetedEntity.isAlive()) {
            fired = true;
        } else {
        }
    }

    /* vrátí true pokud byly všechny požkození uděleny a střela už může zmizet. */
    public boolean tick() {
        if (fired && isFireDone()) {
            return true;
        }
        if (fired && ticksAlive < ticksToLive) {
            ticksAlive++;
            changeOnTick();
            if (ticksAlive == ticksToLive) {
                doTheDamage();
            }
        }
        return isFireDone();
    }

    public boolean isFireDone() {
        return ticksAlive >= ticksToLive || !targetedEntity.isAlive();
    }

    protected void doTheDamage() {
        if (targetedEntity != null) {
            if (targetedEntity.isAlive()) {
                if (targetedEntity.getHit(attackDamage)) {
                    //vražda
                    if (SETTINGS.debugging) {
                        System.out.println("Summoner zabil entitu.. sum: " + summoner + "\n , ent: " + targetedEntity);
                    }
                    summoner.getEntitiesIKilled().add(targetedEntity);
                }
            }
        } else {
            System.err.println("entita na kterou sem útočil je null... wtf");
        }
    }

    protected void changeOnTick() {//změna po každém ticku.. může pomoct vykreslování/renderování
        // :(
    }
    
    protected final void findAndSetMyImage() {
        //nastavit obrázek
        String[] path = this.getClass().getName().split("\\.");
        int u = 0;
        for (int i = 0; i < path.length; i++) {
            if (path[i].equals(Settings.APP_FOLDER_GAME.split("/")[Settings.APP_FOLDER_GAME.split("/").length - 1])) {
                u = i;
                break;
            }
        }
        String[] res = new String[path.length - (u + 1)];
        int z = 0;
        for (int i = u + 1; i < path.length - 1; i++) {
            res[z++] = path[i];
        }
        res[res.length - 1] = path[path.length - 1] + ".png";

        this.imageToPaint = ImageMyMy.getImage(Settings.APP_FOLDER_GAME, res);//4
    }

    //--------------------------------------//--------------------------------------//----------
    //--------------------------------------//--------------------------------------//----------
    //--------------------------------------//--------------------------------------//----------
    //--------------------------------------//--------------------------------------//----------
    //--------------------------------------//--------------------------------------//----------
    //--------------------------------------//--------------------------------------//----------
    //--------------------------------------//--------------------------------------//----------
    //.
    /**
     *
     * @return 0, 0.3...., 0.726,...., 1
     */
    public double getProgress() {
        return (double) ticksAlive / (double) ticksToLive;
    }

    //--------------------------------------//--------------------------------------//----------
    //--------------------------------------//--------------------------------------//----------
    public long getFireDamage() {
        return attackDamage;
    }

    public void setFireDamage(long fireDamage) {
        this.attackDamage = fireDamage;
    }

    public long getxFrom() {
        return xFrom;
    }

    public void setxFrom(long xFrom) {
        this.xFrom = xFrom;
    }

    public long getyFrom() {
        return yFrom;
    }

    public void setyFrom(long yFrom) {
        this.yFrom = yFrom;
    }

    public long getxTo() {
        return xTo;
    }

    public void setxTo(long xTo) {
        this.xTo = xTo;
    }

    public long getyTo() {
        return yTo;
    }

    public void setyTo(long yTo) {
        this.yTo = yTo;
    }

    public boolean isFired() {
        return fired;
    }

    public void setFired(boolean fired) {
        this.fired = fired;
    }

    public long getTicksAlive() {
        return ticksAlive;
    }

    public void setTicksAlive(long ticksAlive) {
        this.ticksAlive = ticksAlive;
    }

    public long getTicksToLive() {
        return ticksToLive;
    }

    public void setTicksToLive(long ticksToLive) {
        this.ticksToLive = ticksToLive;
    }

    @Override
    public String toString() {
        return "AbstractFire{" + "fireDamage=" + attackDamage
                + ", xFrom=" + xFrom + ", yFrom=" + yFrom + ", xTo=" + xTo
                + ", yTo=" + yTo + ", summoner=" + summoner + ", targetedEnemy="
                + targetedEntity + ", fired=" + fired + ", ticksAlive=" + ticksAlive
                + ", ticksToLive=" + ticksToLive + '}';
    }

    public static void main(String[] args) {
        Enemy enemy = new e001BasicEnemy(0, 0, null, null);
        for (int i = 1; i < 4; i++) {
            System.out.println("\n\n " + i + ". ÚTOK: \n\n");
            AbstractFire fire = new AbstractFire(80, 0, 0, 0, 0, new t001LaserTower(0, 0, null, null), enemy) {
                @Override
                public void paintMyselfToCanvas(MyGameCanvas canvas) {
                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            };
            System.out.println(fire);
            System.out.println("Enemy: " + enemy);

            System.out.println("Fire in the hole!");
            fire.fire();

            while (!fire.tick()) {
                System.out.println(fire.getProgress() + ", " + fire.getTicksAlive());
            }
            System.out.println(fire);
            System.out.println("Enemy: " + enemy);
        }
    }

}
