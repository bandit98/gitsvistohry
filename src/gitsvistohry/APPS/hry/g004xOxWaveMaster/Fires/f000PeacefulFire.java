/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Entity;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.MyGameCanvas;

/**
 *
 * @author Ondra
 */
public class f000PeacefulFire extends AbstractFire {

    {
        this.ticksToLive = 2;
        this.attackDamage = 0;
    }

    public f000PeacefulFire(long attackDamage, Entity summoner, Entity targetedEntity) {
        super(attackDamage, summoner, targetedEntity);
    }

    @Override
    public void paintMyselfToCanvas(MyGameCanvas canvas) {
    }

}
