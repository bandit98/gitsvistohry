/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers.t001LaserTower;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers.t002LaserTowerBigger;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.Towers.t003Launcher;
import gitsvistohry.APPS.hry.g004xOxWaveMaster.PlayerInfo.*;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Ondra
 */
public class Game {

    private GameModeEnum gameNodeEnum = GameModeEnum.NOTHING;
    private TowerBuildNode modeTowerBuildNode;

    private final List<Entity> allEntitesInGameList = new LinkedList<>();
    private final List<Entity> listEntitiesToRemove = new LinkedList<>();
    private final List<Entity> listEntitiesToAdd = new LinkedList<>();

    private AxPlayerInfoNode plInfoNode;

    private MyGameCanvas myGameCanvas;
    private Player player = new Player("noName", 80000, 100, 0);

    private Level level;

    public Game() {
//        level = new Level(Waves.getEasyWaves(this), FloorMap.getDefaultFloorMap(), null, "eaaaaaasy", this);
        myGameCanvas = new MyGameCanvas();
        myGameCanvas.setGame(this);
//        allEntitesInGameList.add(new t001LaserTower((long) (5.5 * SETTINGS.POLICKO_LENGTH), (long) (4.5 * SETTINGS.POLICKO_LENGTH), this, null));
//        allEntitesInGameList.add(new t001LaserTower((long) (7.5 * SETTINGS.POLICKO_LENGTH), (long) (4.5 * SETTINGS.POLICKO_LENGTH), this, null));
//        allEntitesInGameList.add(new t002LaserTowerBigger((long) (14.5 * SETTINGS.POLICKO_LENGTH), (long) (7.5 * SETTINGS.POLICKO_LENGTH), this, null));
//        allEntitesInGameList.add(new t002LaserTowerBigger((long) (15.5 * SETTINGS.POLICKO_LENGTH), (long) (7.5 * SETTINGS.POLICKO_LENGTH), this, null));
//        allEntitesInGameList.add(new t002LaserTowerBigger((long) (15.5 * SETTINGS.POLICKO_LENGTH), (long) (8.5 * SETTINGS.POLICKO_LENGTH), this, null));
//        allEntitesInGameList.add(new t002LaserTowerBigger((long) (15.5 * SETTINGS.POLICKO_LENGTH), (long) (9.5 * SETTINGS.POLICKO_LENGTH), this, null));
//        allEntitesInGameList.add(new t002LaserTowerBigger((long) (7.5 * SETTINGS.POLICKO_LENGTH), (long) (8.5 * SETTINGS.POLICKO_LENGTH), this, null));
//        allEntitesInGameList.add(new t003Launcher((long) (5.5 * SETTINGS.POLICKO_LENGTH), (long) (8.5 * SETTINGS.POLICKO_LENGTH), this, null));
    }

    public void nextWaveAction() {
        level.nextWaveAction();
    }

    public void tickEverything() {

        //musí vytickovat všechno vnitřní co potřebuje tikat
        level.tick();

        for (Entity entity : listEntitiesToAdd) {
            allEntitesInGameList.add(entity);
        }
        listEntitiesToAdd.clear();

        for (Entity entity : allEntitesInGameList) {
            entity.tick();
        }

        for (Entity entity : listEntitiesToRemove) {
            allEntitesInGameList.remove(entity);
        }
        listEntitiesToRemove.clear();
    }

    public List<Entity> getAllEntitesInGameList() {
        return allEntitesInGameList;
    }

    public void clearAllEntitiesList() {
        allEntitesInGameList.clear();
    }

    public boolean canINowBuildThere(long renderX, long renderY, MyGameCanvas gCanvas) {
        long bigX = gCanvas.RENDERXtoBigX(renderX);
        long bigY = gCanvas.RENDERYtoBigY(renderY);

        FloorObj flObj = this.getLevel().getFloorMap().getFloorObj((int) bigX, (int) bigY);
        if (flObj == null) {
            return false;
        }
        //nemůžu tu stavět protože na to není vhodná podlaha
        if (!flObj.getFlObjEnum().isBuildable()) {
            System.err.println("I CANT BUILD THERE");
            return false;
        }

        //nemůžu tu stavět protože už tu něco je
        for (Entity entity : this.getAllEntitesInGameList()) {
            if (entity instanceof DefenceTower) {
                if (MyGameCanvas.realXtoBigX(entity.getX()) == bigX && MyGameCanvas.realYtoBigY(entity.getY()) == bigY) {
                    System.err.println("I CANT BUILD THERE - already builded");
                    return false;
                }
            }
        }

        System.out.println("I CAN BUILD THERE");
        return true;
    }

    public boolean addToAllEntities(Entity entity) {
        if (entity instanceof Enemy) {  //nastaví nově příchozím Enemy defaultní startovací souřadnice na startovací pozici.
            ((Enemy) entity).setX(MyGameCanvas.bigXtoRealX(level.getPossiblePaths()[0].getSteps().get(0)[0]) + SETTINGS.POLICKO_LENGTH / 2);
            ((Enemy) entity).setY(MyGameCanvas.bigYtoRealY(level.getPossiblePaths()[0].getSteps().get(0)[1]) + SETTINGS.POLICKO_LENGTH / 2);
        }
        return allEntitesInGameList.add(entity);
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //
    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    public void setMyCanvas(MyGameCanvas myCanvas) {
        this.myGameCanvas = myCanvas;
        myCanvas.setGame(this);
    }

    public MyGameCanvas getMyGameCanvas() {
        return myGameCanvas;
    }

    public void removeEntityFromAllEntitiesList(Entity o) {
        listEntitiesToRemove.add(o);
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public GameModeEnum getGameNodeEnum() {
        return gameNodeEnum;
    }

    public void setGameNodeEnum(GameModeEnum gameNodeEnum) {
        this.gameNodeEnum = gameNodeEnum;
    }

    public TowerBuildNode getModeTowerBuildNode() {
        return modeTowerBuildNode;
    }

    public void setModeTowerBuildNode(TowerBuildNode modeTowerBuildNode) {
        this.modeTowerBuildNode = modeTowerBuildNode;
    }

    public List<Entity> getListEntitiesToAdd() {
        return listEntitiesToAdd;
    }

    void setplayerInfoNode(AxPlayerInfoNode plInfoNode) {
        this.plInfoNode = plInfoNode;
    }

    public AxPlayerInfoNode getPlInfoNode() {
        return plInfoNode;
    }
}
