/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.Fires.AbstractFire;
import gitsvistohry.IMGS.ImageMyMy;
import gitsvistohry.Settings;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX  //TXX
//XCCCCCCCCCCCCCCCCCCCCCXXSSSSSSSSSSSSSSSX  //XXX
//XCCCCCCCCCCCCCCCCCCCCCXXSSSSSSSSSSSSSSSX  //XXX
//XCCCCCCCCCCCCCCCCCCCCCXXSSSSSSSSSSSSSSSX  //XXX
//XCCCCCCCCCCCCCCCCCCCCCXXSSSSSSSSSSSSSSSX  //XXX
//XCCCCCCCCCCCCCCCCCCCCCXXSSSSSSSSSSSSSSSX  //XXX
//XCCCCCCCCCCCCCCCCCCCCCXXSSSSSSSSSSSSSSSX  //XXX
//XCCCCCCCCCCCCCCCCCCCCCXXSSSSSSSSSSSSSSSX  //XXX
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX  //XBX
//----------------------------------------  //XXX--------
//LXXXXXXXXXXXXXXXXXXXXXAAAAAAAAAAAAAAAAAA  //XXX
//XXXXXXXXXXXXXXXXXXXXXXZZXXXXXXXXXXXXXXXX  //XXX //ano, Z je i na dalším řádku.. hodnoty sou stejný
//XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXZZ  //XXX
// S - údaje pro uživatele ; ovládání
// C - hlavní herní plocha
/**
 *
 * @author Ondra
 */
public class MyGameCanvas extends Pane {

    public DoubleProperty getNewPlayerInfoRequiredX_Propperty() {
        DoubleProperty dp = new SimpleDoubleProperty();
        dp.bind(this.gCanvas.widthProperty().add(_L_Width + _z_Width));
        return dp;
    }

    public int getPlayerInfoRequiredY() {
        return _t_Heigh;
    }

    private Game game;

    private static final int _t_Heigh = 20;
    private static final int _b_Heigh = 30;
    private static final int _s_Heigh = 800;
    private static final int _s_Width = 400;
    private static final int _L_Width = 5;
    private static final int _z_Width = 30;
    private static final int _a_Width = _z_Width + _s_Width;

    private Canvas gCanvas;
    private GraphicsContext gc;

    double bigSquareRenderWidth;
    double bigSquareRenderHeigh;

    private double _lastMouseRENDERXPosition;
    private double _lastMouseRENDERYPosition;

    public void render() {
        int a = game.getLevel().getFloorMap().getxSize_width();
        int b = (int) gCanvas.getWidth();
        bigSquareRenderWidth = (gCanvas.getWidth() / (game.getLevel().getFloorMap().getxSize_width()));
        bigSquareRenderHeigh = (gCanvas.getHeight() / (game.getLevel().getFloorMap().getySize_Heigh()));

        clearCanvas();
        paintFloor();
        paintGameMode();
        paintEntities();
        paintFires();
    }

    private void clearCanvas() {
        gc.clearRect(0, 0, gCanvas.getWidth(), gCanvas.getHeight());
    }

    private void paintFloor() {
        for (PaintableObject floorObj : game.getLevel().getFloorMap().getPaintableObjects()) {
            gc.drawImage(floorObj.getImageToPaint(), floorObj.getX() * bigSquareRenderWidth, floorObj.getY() * bigSquareRenderHeigh, bigSquareRenderWidth, bigSquareRenderHeigh);
        }
    }

    private void paintFires() {
        for (Entity entity : game.getLevel().getGame().getAllEntitesInGameList()) {
            for (AbstractFire abstractFire : entity.getFiresFired()) {
                abstractFire.paintMyselfToCanvas(this);
            }
        }
    }

    private void paintEntities() {
        double x;
        double y;
        double w;
        double h;
        for (Entity entity : game.getLevel().getGame().getAllEntitesInGameList()) {
            x = entity.getX() * bigSquareRenderWidth / SETTINGS.POLICKO_LENGTH;
            y = entity.getY() * bigSquareRenderHeigh / SETTINGS.POLICKO_LENGTH;
            w = bigSquareRenderWidth * (SETTINGS.ENEMY_SIZE__policko_part);
            h = bigSquareRenderHeigh * (SETTINGS.ENEMY_SIZE__policko_part);
            
//            if (entity instanceof Enemy || entity instanceof DefenceTower) {//budou se zobrazovat jakoby x a y měli uprostřed
                x = x - (w / 2);
                y = y - (h / 2);
//            }

            gc.drawImage(entity.getImageToPaint(), x, y, w, h);

            gc.setFill(Color.AQUA);
            gc.fillText(Long.toString(entity.getHealth()), x + w / 2, y + h * 1.25);
        }
    }

    private void paintGameMode() {
        if (_lastMouseRENDERXPosition > this.getWidth() || _lastMouseRENDERXPosition < 0
                || _lastMouseRENDERYPosition > this.getHeight() || _lastMouseRENDERYPosition < 0) {
            return;
        }

        switch (game.getGameNodeEnum()) {
            case BUILDING:
                long lastX = (long) _lastMouseRENDERXPosition;
                long lastY = (long) _lastMouseRENDERYPosition;
                gc.setFill(Color.RED);
                if (game.canINowBuildThere(lastX, lastY, this)) {
                    gc.setFill(Color.LIME);
                }
                gc.setGlobalAlpha(0.55);
                gc.fillRect(realXtoRENDERX(bigXtoRealX((int) RENDERXtoBigX(lastX))), realYtoRENDERY(bigYtoRealY((int) RENDERYtoBigY(lastY))),
                        realXtoRENDERX(SETTINGS.POLICKO_LENGTH), realYtoRENDERY(SETTINGS.POLICKO_LENGTH));
                gc.setGlobalAlpha(0.37);
                gc.drawImage(
                        game.getModeTowerBuildNode().getMyDefTower().getImageToPaint(),
                        realXtoRENDERX(bigXtoRealX((int) RENDERXtoBigX(lastX))),
                        realYtoRENDERY(bigYtoRealY((int) RENDERYtoBigY(lastY))),
                        realXtoRENDERX(SETTINGS.POLICKO_LENGTH),
                        realYtoRENDERY(SETTINGS.POLICKO_LENGTH));
                gc.setGlobalAlpha(1);
                _paintEntityRange(
                        bigXtoRealX((int) (RENDERXtoBigX(lastX)))+SETTINGS.POLICKO_LENGTH/2,
                        bigYtoRealY((int) (RENDERYtoBigY(lastY)))+SETTINGS.POLICKO_LENGTH/2,
                        game.getModeTowerBuildNode().getMyDefTower().getRange(),
                        Color.PURPLE, 0.26);
                break;
            case WATCHING_INFO:
                long entityRealX = game.getPlInfoNode().getModelInfo().getMyWatchingEntity().getX();
                long entityRealY = game.getPlInfoNode().getModelInfo().getMyWatchingEntity().getY();

                if (game.getPlInfoNode().getModelInfo().getMyWatchingEntity().isAlive()) {
                    _paintWatchingEntityFrame(entityRealX, entityRealY);
                    _paintEntityRange(
                            entityRealX,
                            entityRealY,
                            game.getPlInfoNode().getModelInfo().getMyWatchingEntity().getRange(),
                            game.getPlInfoNode().getModelInfo().getMyWatchingEntity() instanceof Enemy ? Color.RED : Color.GREENYELLOW,
                            0.09);
                }
                break;
            default:
                break;
        }
    }

    private final Image imageFRAMEToPaint = ImageMyMy.getImage(Settings.APP_FOLDER_GAME, "g004xOxWaveMaster", "selected.png");

    private void _paintWatchingEntityFrame(long entityRealX, long entityRealY) {
        gc.drawImage(imageFRAMEToPaint,
                realXtoRENDERX(entityRealX - SETTINGS.POLICKO_LENGTH / 2),
                realYtoRENDERY(entityRealY - SETTINGS.POLICKO_LENGTH / 2),
                realXtoRENDERX(SETTINGS.POLICKO_LENGTH),
                realYtoRENDERY(SETTINGS.POLICKO_LENGTH));
    }

    private void _paintEntityRange(long startRealX, long startRealY, long realRange, Paint color, double alphaOpacity) {
        gc.setGlobalAlpha(alphaOpacity);

        gc.setFill(color);
        gc.fillOval(
                realXtoRENDERX(startRealX - realRange),
                realYtoRENDERY(startRealY - realRange),
                realXtoRENDERX(realRange * 2),
                realYtoRENDERY(realRange * 2));
        gc.setGlobalAlpha(gc.getGlobalAlpha()+0.1);
        
        gc.setStroke(Color.BLUE);
        gc.setLineWidth(5);
        gc.strokeOval(
                realXtoRENDERX(startRealX - realRange),
                realYtoRENDERY(startRealY - realRange),
                realXtoRENDERX(realRange * 2),
                realYtoRENDERY(realRange * 2));

        gc.setGlobalAlpha(1);
    }

    public MyGameCanvas() {
        init();
    }

    public void init() {
//        bindThisSize();
//        createAndBindCanvas();
//        this.setStyle("-fx-background-color: #c6c6c6;");

        this.setOnMouseClicked((MouseEvent event) -> {
            mouseClicked(event);
        });
        this.setOnMouseMoved((MouseEvent event) -> {
            mouseMoved(event);
        });
    }

    private void bindThisSize() {
//        Platform.runLater(() -> {
        try {
            Thread.sleep(50);
            this.prefHeightProperty().bind(((Pane) this.getParent()).heightProperty());
            this.prefWidthProperty().bind(((Pane) this.getParent()).widthProperty());
        } catch (Exception e) {
            System.err.println(e);
        }
//        });
    }

    public void createAndBindCanvas() {
        gCanvas = new Canvas();
        gCanvas.setLayoutX(_L_Width);
        gCanvas.setLayoutY(_t_Heigh);
        this.getChildren().add(gCanvas);
        gCanvas.widthProperty().bind(this.widthProperty().subtract(_L_Width + _a_Width));
        gCanvas.heightProperty().bind(this.heightProperty().subtract(_t_Heigh + _b_Heigh));
        this.prefWidthProperty().bind(((Pane) this.getParent()).widthProperty());
        this.prefHeightProperty().bind(((Pane) this.getParent()).heightProperty());
        gc = gCanvas.getGraphicsContext2D();
    }

    public Canvas getgCanvas() {
        return gCanvas;
    }

    public static int realXtoBigX(long realX) {
        return (int) (realX / SETTINGS.POLICKO_LENGTH);
    }

    public static int realYtoBigY(long realY) {
        return (int) (realY / SETTINGS.POLICKO_LENGTH);
    }

    public static long bigXtoRealX(int bigX) {
        return (int) (bigX * SETTINGS.POLICKO_LENGTH);
    }

    public static long bigYtoRealY(int bigY) {
        return (int) (bigY * SETTINGS.POLICKO_LENGTH);
    }

    public long realXtoRENDERX(long realX) {
        return (long) (realX * (gCanvas.getWidth() / (game.getLevel().getFloorMap().getxSize_width() * SETTINGS.POLICKO_LENGTH)));
    }

    public long realYtoRENDERY(long realY) {
        return (long) (realY * (gCanvas.getHeight() / (game.getLevel().getFloorMap().getySize_Heigh() * SETTINGS.POLICKO_LENGTH)));
    }

    public long RENDERXtoBigX(long renderX) {
        return realXtoBigX((long) (renderX / (gCanvas.getWidth() / (game.getLevel().getFloorMap().getxSize_width() * SETTINGS.POLICKO_LENGTH))));
    }

    public long RENDERYtoBigY(long renderY) {
        return realYtoBigY((long) (renderY / (gCanvas.getHeight() / (game.getLevel().getFloorMap().getySize_Heigh() * SETTINGS.POLICKO_LENGTH))));
    }

    public long RENDERXtoRealX(long renderX) {
        return (long) (renderX / (gCanvas.getWidth() / (game.getLevel().getFloorMap().getxSize_width() * SETTINGS.POLICKO_LENGTH)));
    }

    public long RENDERYtoRealY(long renderY) {
        return (long) (renderY / (gCanvas.getHeight() / (game.getLevel().getFloorMap().getySize_Heigh() * SETTINGS.POLICKO_LENGTH)));
    }

    private void mouseClicked(MouseEvent event) {
        switch (game.getGameNodeEnum()) {
            case BUILDING:
                DefenceTower gModeDt = game.getModeTowerBuildNode().getMyDefTower();
                if (game.getPlayer().getMoney().get() > gModeDt.getPriceOfDT() && game.canINowBuildThere((long) event.getX(),
                        (long) event.getY(), this)) {
                    //odebrání peněz za nákup
                    game.getPlayer().getMoney().set(game.getPlayer().getMoney().get() - gModeDt.getPriceOfDT());

                    //přidání td do hry
                    Platform.runLater(() -> {
                        game.getListEntitiesToAdd().add(
                                gModeDt.getNewInstanceOfMyself(
                                        bigXtoRealX((int) this.RENDERXtoBigX((long) event.getX())) + SETTINGS.POLICKO_LENGTH / 2,
                                        bigYtoRealY((int) this.RENDERYtoBigY((long) event.getY())) + SETTINGS.POLICKO_LENGTH / 2, game, null)
                        );
                    });
                } else {
                    game.setGameNodeEnum(GameModeEnum.NOTHING);
                }
                break;
            case NOTHING:
            case WATCHING_INFO:
                System.out.println("nothing");
                long mouseRealX = RENDERXtoRealX((long) event.getX());
                long mouseRealY = RENDERYtoRealY((long) event.getY());

                Entity closestEntity = null;
                long actDistance;
                long smallestDistance = Long.MAX_VALUE;
                for (Entity entity : game.getAllEntitesInGameList()) {
                    actDistance = (long) Math.hypot(mouseRealX - entity.getX(), mouseRealY - entity.getY());
                    if (actDistance < smallestDistance) {
                        smallestDistance = actDistance;
                        closestEntity = entity;
                    }
                }

                if (closestEntity == game.getPlInfoNode().getModelInfo().getMyWatchingEntity()) {
                    game.getPlInfoNode().getModelInfo().setMyWatchingEntity(null);
                    game.setGameNodeEnum(GameModeEnum.NOTHING);
                    return;
                }

                if (closestEntity != null) {
                    game.getPlInfoNode().getModelInfo().setMyWatchingEntity(closestEntity);
                    game.setGameNodeEnum(GameModeEnum.WATCHING_INFO);
                }
                break;
            default:
                break;
        }
    }

    private void mouseMoved(MouseEvent event) {
        _lastMouseRENDERXPosition = event.getX();
        _lastMouseRENDERYPosition = event.getY();
    }

    private class InfoPane {
    }

    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    //--------------------------------------//--------------------------------------//--------------------------------------
    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public static int getPlayerInfoRequiredWidth() {
        return _s_Width - _z_Width;
    }

    public static int getPlayerInfoRequiredHeight() {
        return _s_Heigh;
    }

}
