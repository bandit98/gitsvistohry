/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.kraviny.k001xOxBinaryClock;

import gitsvistohry.AppMyMy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.util.Duration;

/**
 *
 * @author Ondra
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Circle circH1;
    @FXML
    private Circle circH2;
    @FXML
    private Circle circH22;
    @FXML
    private Circle circH21;
    @FXML
    private Circle circH24;
    @FXML
    private Circle circH23;
    @FXML
    private Circle circM2;
    @FXML
    private Circle circM1;
    @FXML
    private Circle circM23;
    @FXML
    private Circle circM24;
    @FXML
    private Circle circM21;
    @FXML
    private Circle circM22;
    @FXML
    private Circle circS3;
    @FXML
    private Circle circS1;
    @FXML
    private Circle circS2;
    @FXML
    private Circle circS23;
    @FXML
    private Circle circS24;
    @FXML
    private Circle circS21;
    @FXML
    private Circle circS22;
    @FXML
    private Circle circM3;

    Circle[] ciH1;
    Circle[] ciH2;

    Circle[] ciM1;
    Circle[] ciM2;

    Circle[] ciS1;
    Circle[] ciS2;

    Circle[][] circlesList;
    private CheckBox CheckboxID_CustomInput;
    private TextField txtField_CustomInput;
    
    Timeline timeline;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ciH1 = new Circle[]{circH1, circH2};
        ciH2 = new Circle[]{circH21, circH22, circH23, circH24};

        ciM1 = new Circle[]{circM1, circM2, circM3};
        ciM2 = new Circle[]{circM21, circM22, circM23, circM24};

        ciS1 = new Circle[]{circS1, circS2, circS3};
        ciS2 = new Circle[]{circS21, circS22, circS23, circS24};

        circlesList = new Circle[][]{ciH1, ciH2, ciM1, ciM2, ciS1, ciS2};

        timeline = new Timeline(new KeyFrame(
                Duration.millis(500),
                ae -> changeColours()));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    void changeColours() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HHmmss");
        String Timee = sdf.format(cal.getTime());
        System.out.println(Timee);

        for (Circle[] circ : circlesList) {
            for (int i = 0; i < circ.length; i++) {
                circ[i].setFill(binaryValEqual(Timee.charAt(0), i) ? Color.AQUA : Color.DARKGRAY);
            }
            Timee = Timee.substring(1);
        }
    }

    boolean binaryValEqual(int input, int position) {
        int tmpPos = (int) (Math.pow((double) 2, (double) position));
        return (tmpPos & input) == tmpPos;
    }

//    for (int i = 0; i < 6; i++) {
//            System.out.println(binaryValEqual(5, i));
//        }

    private void CheckOnAction_CustomInput(ActionEvent event) {
            txtField_CustomInput.setVisible(CheckboxID_CustomInput.isSelected());
    }

    @FXML
    private void QuitBtnOnAction(ActionEvent event) {
        timeline.stop();
        AppMyMy.EndTheApp();
    }
}
