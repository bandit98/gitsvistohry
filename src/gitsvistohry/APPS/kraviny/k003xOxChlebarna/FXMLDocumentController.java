/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.kraviny.k003xOxChlebarna;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URL;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
//import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

//import javax.swing.JOptionPane;              //potom by to bylo fajn předělat na javafx
/**
 *
 * @author Ondra
 */
public class FXMLDocumentController implements Initializable {

    String pomOutput;
    long pomWordsNumber;

    boolean printDone = false;
    JdeChleba jdeChleba = new JdeChleba();
    Food foodPom = new Food();              //pomocná proměnná
    ChlebarnaCore chlebarnaCore = new ChlebarnaCore();

    //language problems........
    final private String __words = "Words: ";
    final private String __nominative = "(1st) nominative";
    final private String __accusative = "(4th) accusative";
    final private String __vocative = "(5th) vocative";

    final private String __areYouSure = "Are you sure?";
    final private String __clearConfirmation = "Clear Confirmation";
    final private String __defaultsettingsConfirmation = "Default settings Confirmation";

    final private String __RedCssStyleMyMy = "-fx-text-box-border: red ;  -fx-focus-color: red ;";
    final private String __DefaultCssTextStyle = "";

    @FXML
    private Button btnSaveChangesFood;
    @FXML
    private Button btnDeleteFood;
    @FXML
    private Button btnPrint;
    @FXML
    private TextField text_1st;
    @FXML
    private TextField text_2nd;
    @FXML
    private TextField text_3rd;
    @FXML
    private Label LabelInput;
    @FXML
    private TableView<Food> TableMainStuffMyMy;
    @FXML
    private TextArea txtOutput;
    @FXML
    private CheckBox CheckSplitIntoLines;
    @FXML
    private ListView<String> ListViewAddons;
    @FXML
    private CheckBox CheckWriteTheEnd;
    @FXML
    private TextField txtAddon;
    @FXML
    private ComboBox<String> ComboTheEnd;
    @FXML
    private MenuItem menu_Help_About_Action;
    @FXML
    private Button btnDeleteSelectedAddon;
    @FXML
    private Button btnAddAddons;
    @FXML
    private Button btnSaveChangesSelectedAddon;
    @FXML
    private Button btnAddFood;
    @FXML
    private CheckMenuItem menu_File_Autosave__CheckMenuItem;
    @FXML
    private Menu Menu_Language;
    @FXML
    private Label labelWords;
    @FXML
    private ProgressIndicator ProgressIndicatorOutput;
    @FXML
    private AnchorPane anchorPaneOutput;

    //nemazat, to tu je schválně bez FXML
    ProgressIndicator progressIndic;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        actualizeAll();

        BindResizeMyMy();
        setMainTable();
        // TODO
    }

    @FXML
    private void clickPrint(ActionEvent event) {
        Task task = new Task<Void>() {
            @Override
            public Void call() {
                try {
                    pomOutput = jdeChleba.getTheFinalStory(CheckWriteTheEnd.isSelected(), (ComboTheEnd != null) ? ComboTheEnd.getSelectionModel().getSelectedItem() : "");
                    if (jdeChleba.isCountWords()) {
                        pomWordsNumber = chlebarnaCore.getNumberOfWords(chlebarnaCore.fuckSpaces(pomOutput));
                    }
//                    jdeChleba.setWords(chlebarnaCore.getNumberOfWords(chlebarnaCore.fuckSpaces(jdeChleba.getTheFinalStory(false))));
//      txtOutput.setText(jdeChleba.getTheFinalStory());//  labelWords.setText(""+chlebarnaCore.getNumberOfWords(chlebarnaCore.fuckSpaces(jdeChleba.getTheFinalStory(false))));//
                } catch (Exception e) {
                    System.err.println("ERROR ClickPrint: " + e.getMessage());
                }
                return null;
            }
        };

        MakeProgresIndicator();
        progressIndic.setPrefSize(80, 80);
        printDone = false;
        btnPrint.setDisable(true);
        txtOutput.requestFocus();
        txtOutput.setText("");
        labelWords.setText(__words);
//       ProgressIndicatorOutput.setVisible(true);
        new Thread(task).start();

        task.setOnSucceeded(new EventHandler() {
            @Override
            public void handle(Event t) {
                txtOutput.setText(pomOutput);
                labelWords.setText(__words + ((pomWordsNumber > 0) ? pomWordsNumber : ""));
//                ProgressIndicatorOutput.setVisible(false);
                btnPrint.setDisable(false);
                printDone = true;
                DeleteProgresIndicator();
            }
        });
    }

    private void MakeProgresIndicator() {
        progressIndic = new ProgressIndicator();
        progressIndic.setPrefSize(80, 80);
        RecomputeParamsToProgressIndic();
        anchorPaneOutput.getChildren().add(progressIndic);
    }

    private void DeleteProgresIndicator() {
        System.out.println(anchorPaneOutput.getChildren().remove(progressIndic));
        progressIndic = null;
    }

    private void setMainTable() {
        int minWidthMyMy = 50;

        TableColumn<Food, Integer> columnID = new TableColumn<>("ID");
        columnID.setMinWidth(20);
        columnID.setMaxWidth(20);
        columnID.setCellValueFactory(new PropertyValueFactory<Food, Integer>("id"));

        TableColumn<Food, String> column1st = new TableColumn<>(__nominative);
        column1st.setMinWidth(minWidthMyMy);
        column1st.setCellValueFactory(new PropertyValueFactory<Food, String>("prvniPad"));

        TableColumn<Food, String> column2nd = new TableColumn<>(__accusative);
        column2nd.setMinWidth(minWidthMyMy);
        column2nd.setCellValueFactory(new PropertyValueFactory<Food, String>("ctvrtyPad"));

        TableColumn<Food, String> column3rd = new TableColumn<>(__vocative);
        column3rd.setMinWidth(minWidthMyMy);
        column3rd.setCellValueFactory(new PropertyValueFactory<Food, String>("patyPad"));

        TableMainStuffMyMy.getColumns().addAll(columnID, column1st, column2nd, column3rd);
    }

    /**
     * načte jídlo ze zadaného textu a hodí ho do pomocné proměnné
     */
    private void setFoodFromTextToPom() {
        //foodPom = null;
        foodPom.setPrvniPad(text_1st.getText());
        foodPom.setCtvrtyPad(text_2nd.getText());
        foodPom.setPatyPad(text_3rd.getText());
    }

    /**
     * přepočítá IDčka všech jídel a dá je do proměnný jdeChleba
     */
    private void recomputeIDs() {
        for (int i = 0; i < jdeChleba.getlistOfFood().size(); i++) {
            jdeChleba.getlistOfFood().get(i).setId(i);
        }
    }

    @FXML
    private void CheckSplitIntoLinesAction(ActionEvent event) {
        jdeChleba.setSplitIntoLines(CheckSplitIntoLines.selectedProperty().getValue());
    }

    @FXML
    private void CheckWriteTheEndAction(ActionEvent event) {
        ComboTheEnd.setDisable(!CheckWriteTheEnd.selectedProperty().getValue());             //pokud je ChechWriteTheEnd true, umožní změnit co je v comboboxu
        if (CheckWriteTheEnd.selectedProperty().getValue() == true) {
            ComboTheEnd.getSelectionModel().selectFirst();
        }
        jdeChleba.setWriteTheEnd(CheckWriteTheEnd.selectedProperty().getValue());           //zapíše do "nastavení" chlebárny že se bude psát Konec (podle ComboBoxu)
    }

    @FXML
    private void comboTheEndAction(ActionEvent event) {
        jdeChleba.setTheEnd((ComboTheEnd.getSelectionModel().getSelectedItem() != null) ? ComboTheEnd.getSelectionModel().getSelectedItem() : "");
        System.out.println("comboTheEndAction done");
    }

    /**
     * při kliknutí zapíše informace o zvoleném jídle do vstupu (pro možnost editace nebo
     * vymazání
     */
    @FXML
    private void TableMainStuffMyMy_Action_MouseClicked(MouseEvent event) {
        if (TableMainStuffMyMy.getSelectionModel().getSelectedItem() != null) {             //kvůli klikání do prázdna
            foodPom = TableMainStuffMyMy.getSelectionModel().getSelectedItem();
            text_1st.setText(foodPom.getPrvniPad());
            text_2nd.setText(foodPom.getCtvrtyPad());
            text_3rd.setText(foodPom.getPatyPad());
            ClearFoodPom();

            btnDeleteFood.setDisable(false);
            btnSaveChangesFood.setDisable(false);

            System.out.println("test u TableMainStuffMyMy_Action");
        } else {
            System.out.println("kliknutí do prázdna");
        }
    }

    //                      FOOD
    @FXML
    private void btnDeleteSelectedFoodAction(ActionEvent event) {
        jdeChleba.getlistOfFood().remove(TableMainStuffMyMy.getSelectionModel().getSelectedIndex());
        recomputeIDs();
        ActualizeMainFoodTable();
        EndOfEditingFood();
    }

    @FXML
    private void btnSaveFoodChangesAction(ActionEvent event) {
        if (checkTextFieldAndColorIt(text_1st)) {
            setFoodFromTextToPom();
            int i = TableMainStuffMyMy.getSelectionModel().getSelectedIndex();
            foodPom.setId(i);
//            if(correctFoodPom()){
            jdeChleba.changeFood(i, foodPom);
            ClearFoodPom();
            ActualizeMainFoodTable();
            EndOfEditingFood();
//            }else{
//                System.err.println("unexpected ERROR in btnSaveFoodChangesAction 1 ");
//            }
        } else {
            System.err.println("invalid input /(btnSaveFoodChangesAction/)");
        }
    }

    @FXML
    private void btnAddFoodAction(ActionEvent event) {
        int i = 6;
        if (checkTextFieldAndColorIt(text_1st)) {
            setFoodFromTextToPom();
            correctFoodPom();
            foodPom.setId(jdeChleba.getlistOfFood().size());
            jdeChleba.AddFood(foodPom);
            ClearFoodPom();
            ActualizeMainFoodTable();
            EndOfEditingFood();
        } else {
            System.err.println("invalid input /(btnSaveFoodChangesAction/)");
        }
    }

    @FXML
    private void txtFoodAction(ActionEvent event) {    //po stisknutí enteru v jakýmkoliv FoodTextu - pokud je to možné ulož změny.  Pokud se ale nic nemění, přidej nový záznam.
        if (!btnSaveChangesFood.isDisabled()) {
            btnSaveFoodChangesAction(event);
        } else if (!btnAddAddons.isDisabled()) {
            btnAddFoodAction(event);
        } else {
            System.err.println("ERROR txtFoodAction 1");
        }
    }

    //               ADDONS
    @FXML
    private void btnAddAddonsAction(ActionEvent event) {
        String addon = txtAddon.getText();

        if (checkTextFieldAndColorIt(txtAddon)) {             //text je v pořádku, můžeme psát
            jdeChleba.AddAddon(chlebarnaCore.fuckSpaces(txtAddon.getText()));
            ListViewAddons.setItems(jdeChleba.getListOfAddons());
        }
        EndOfEditingAddons();
    }

    @FXML
    private void ListViewAddons_Action_MouseClicked(MouseEvent event) {
        if (ListViewAddons.getSelectionModel().getSelectedItem() != null) {             //kvůli klikání do prázdna
            txtAddon.setText(ListViewAddons.getSelectionModel().getSelectedItem());

            btnDeleteSelectedAddon.setDisable(false);
            btnSaveChangesSelectedAddon.setDisable(false);

            System.out.println("test u ListViewAddons_Action");
        } else {
            System.out.println("kliknutí do prázdna /(addons/)");
        }
    }

    @FXML
    private void txtAddon_Action(ActionEvent event) {
        if (!btnSaveChangesSelectedAddon.isDisabled()) {
            btnSaveChangesSelectedAddonAction(event);
        } else if (!btnAddAddons.isDisabled()) {
            btnAddAddonsAction(event);
        } else {
            System.err.println("ERROR txtAddonAction 1");
        }
    }

    @FXML
    private void btnSaveChangesSelectedAddonAction(ActionEvent event) {
        if (checkTextFieldAndColorIt(txtAddon)) {
            int i = ListViewAddons.getSelectionModel().getSelectedIndex();
            jdeChleba.changeAddon(i, chlebarnaCore.fuckSpaces(txtAddon.getText()));

            EndOfEditingAddons();
            ActualizeAddonsTable();
        }
    }

    @FXML
    private void btnDeleteSelectedAddonAction(ActionEvent event) {
        jdeChleba.getListOfAddons().remove(ListViewAddons.getSelectionModel().getSelectedIndex());
        ActualizeAddonsTable();
        EndOfEditingAddons();
    }

    //------
    @FXML
    private void menu_File_Close_Action(ActionEvent event) {
        ChlebarnaCore.shutdown();
    }

    @FXML
    private void btnAdvancedAction(ActionEvent event) {
        System.err.println("Not supported yet.");
    }

    @FXML
    private void btnClearAllAction(ActionEvent event) {
        if (ChlebarnaCore.msgConfirmBox(__areYouSure, __clearConfirmation)) {
            ClearInputAddons();
            ClearInputFoodTxts();
            DeleteAllAddons();
            DeleteAllFoods();
            txtOutput.setText("");
        }
    }

    @FXML
    private void btnSetEverythingDefaultAction(ActionEvent event) {
        if (ChlebarnaCore.msgConfirmBox(__areYouSure, "")) {
            DeleteAllFoods();

            jdeChleba.AddFood(0, "rohlík", "rohlík", "cohlíku");
            jdeChleba.AddFood(1, "chleba", "chleba", "chlebe");
            jdeChleba.AddFood(2, "dalamánek", "dalamánka", "dalamánku");
            jdeChleba.AddFood(3, "houska", "housku", "housko");
//            jdeChleba.AddFood(4,"houska","housku","housko");
//            jdeChleba.AddFood(5,"houska","housku","housko");
//            jdeChleba.AddFood(6,"houska","housku","housko");
//            jdeChleba.AddFood(7,"houska","housku","housko");
//            jdeChleba.AddFood(8,"houska","housku","housko");
//            jdeChleba.AddFood(9,"houska","housku","housko");
//            jdeChleba.AddFood(10,"houska","housku","housko");
//            jdeChleba.AddFood(11,"houska","housku","housko");
//            jdeChleba.AddFood(12,"houska","housku","housko");
//            jdeChleba.AddFood(13,"houska","housku","housko");
//            jdeChleba.AddFood(14,"houska","housku","housko");
//            jdeChleba.AddFood(15,"houska","housku","housko");
//            jdeChleba.AddFood(16,"houska","housku","housko");

            DeleteAllAddons();

            jdeChleba.AddAddon("s máslem");
            jdeChleba.AddAddon("s máslem se salámem");

            ActualizeMainFoodTable();
            ActualizeAddonsTable();

            CheckSplitIntoLines.setSelected(true);
            CheckWriteTheEndAction(new ActionEvent());

            btnPrint.requestFocus();
        } else {
            System.out.println("SetEverythingDefault Stopped");
        }
    }

    @FXML
    private void menu_File_AutosaveAction(ActionEvent event) {
    }

    @FXML
    private void Menu_File_SaveOutputAs_Action(ActionEvent event) {
    }

    //------------------------------------------------------------------------------------------------------------
    /**
     * ano, můžeš to prostě vyměnit za fuckSpaces(String pom)
     */
//    private String makeCorrectAddon(String pom){
//        return chlebarnaCore.fuckSpaces(pom);
//    }
    /**
     * * smaže mezery před a za
     */
    private Food fuckSpaces(Food food) {
        food.setPrvniPad(chlebarnaCore.fuckSpaces(food.getPrvniPad()));
        food.setCtvrtyPad(chlebarnaCore.fuckSpaces(food.getCtvrtyPad()));
        food.setPatyPad(chlebarnaCore.fuckSpaces(food.getPatyPad()));
        return food;
    }

    /**
     * odstraní mezery z konců, divný ID nastaví na -1
     */
    private boolean correctFoodPom() {
        if (!_completeFoodPomInside()) {
            return false;
        }
        foodPom = fuckSpaces(foodPom);
//        _completeFoodPomInside();
        return true;                        //opraveno úspěšně
    }

    /**
     * Vyplní nevyplněné u foodPom. Ale nejspíš by si měl použít prostě correctFoodPom()..
     * využívá i tuhle funkci.
     */
    private boolean _completeFoodPomInside() {
        if ((foodPom.getId() > -1)) {
        } else {
            foodPom.setId(-1);
        }
        if (chlebarnaCore.IsTextValid(foodPom.getPrvniPad())) {
            return false;
        }                   // vrátí false pokud se nezdařilo (pokud 1. pád není zadán) ////foodPom.setPrvniPad("what")  
        if ((foodPom.getCtvrtyPad() == null) || foodPom.getCtvrtyPad().length() < 1) {
            foodPom.setCtvrtyPad(foodPom.getPrvniPad());
        }
        if ((foodPom.getPatyPad() == null) || foodPom.getPatyPad().length() < 1) {
            foodPom.setPatyPad(foodPom.getPrvniPad());
        }
        return true;
    }

    private void ClearInputFoodTxts() {
        text_1st.setText("");
        text_2nd.setText("");
        text_3rd.setText("");
    }

    private void DeleteAllFoods() {
        jdeChleba.getlistOfFood().clear();
        ActualizeMainFoodTable();
    }

    private void ClearInputAddons() {
        txtAddon.setText("");
    }

    private void DeleteAllAddons() {
        jdeChleba.getListOfAddons().clear();
        ActualizeAddonsTable();
    }

    private void ActualizeMainFoodTable() {
        recomputeIDs();
        TableMainStuffMyMy.setItems(jdeChleba.getlistOfFood());
    }

    private void ActualizeEnds() {
        ComboTheEnd.setItems(jdeChleba.getListOfEnds());
    }

    private void ActualizeAddonsTable() {
        ListViewAddons.setItems(jdeChleba.getListOfAddons());
    }

    /**
     * zakáže tlačítka pro editaci jídla, vymaže text, odznačí
     */
    private void EndOfEditingFood() {
        btnDeleteFood.setDisable(true);
        btnSaveChangesFood.setDisable(true);

        ClearInputFoodTxts();

        TableMainStuffMyMy.getSelectionModel().clearSelection();

        text_1st.requestFocus();
    }

    private void EndOfEditingAddons() {
        btnDeleteSelectedAddon.setDisable(true);
        btnSaveChangesSelectedAddon.setDisable(true);

        ClearInputAddons();

        ListViewAddons.getSelectionModel().clearSelection();
    }

    private void ClearFoodPom() {
        foodPom = new Food();
    }

//    private boolean checkAddonInputAndColorIt(){
//        boolean bol=true;
//        if(!IsTextValid(txtAddon.getText())){
//            txtAddon.setStyle("-fx-text-box-border: red ;  -fx-focus-color: red ;");
//            return false;
//        }
//        txtAddon.setStyle(__DefaultCssTextStyle);
//        return true;
//    }
//    
    private boolean checkTextFieldAndColorIt(TextField textField) {
        if (!chlebarnaCore.IsTextValid(textField.getText())) {
            textField.setStyle(__RedCssStyleMyMy);
            return false;
        }
        textField.setStyle(__DefaultCssTextStyle);
        return true;
    }

    private void BindResizeMyMy() {
        anchorPaneOutput.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if (printDone == false && progressIndic != null) {
                    RecomputeProgX();
                }
            }
        });
        anchorPaneOutput.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> ov, Number t, Number t1) {
                if (printDone == false && progressIndic != null) {
                    RecomputeProgY();
                }
            }
        });

    }

    private void RecomputeParamsToProgressIndic() {
        RecomputeProgX();
        RecomputeProgY();
    }

    private void RecomputeProgX() {
        progressIndic.setLayoutX((anchorPaneOutput.getWidth() / 2) - (progressIndic.getPrefWidth() / 2));
    }

    private void RecomputeProgY() {
        progressIndic.setLayoutY((anchorPaneOutput.getHeight() / 2) - (progressIndic.getPrefHeight() / 2));
    }

    private void actualizeAll() {
        ActualizeMainFoodTable();
        ActualizeEnds();
        ActualizeAddonsTable();
    }

    public String getEnd() {
        return ComboTheEnd.getSelectionModel().getSelectedItem();
    }

    @FXML
    private void menuItem_saveSettingsAs(ActionEvent event) {
        final File outputFileDirectory = Paths.get(System.getenv("APPDATA") + "/AAchlebarna/").toFile();
        final FileChooser fileChooser = new FileChooser();
        outputFileDirectory.mkdirs();
        fileChooser.getExtensionFilters().addAll(new ExtensionFilter("JdeChleba Files", "*.chleba"));
        fileChooser.setInitialDirectory(outputFileDirectory);
        List<Food> listFood = jdeChleba.getlistOfFood();
        List<String> listAddons = jdeChleba.getListOfAddons();

        File file = fileChooser.showSaveDialog(new Stage());

        if (file == null) {
            System.err.println("Soubor nezvolen.");
            return;
        }

        saveSettToFile(file, listFood, listAddons);
    }

    @FXML
    private void loadSettingsFrom(ActionEvent event) {
        final File outputFileDirectory = Paths.get(System.getenv("APPDATA") + "/AAchlebarna/").toFile();
        final FileChooser fileChooser = new FileChooser();
        outputFileDirectory.mkdirs();
        fileChooser.setInitialDirectory(outputFileDirectory);
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("JdeChleba Files", "*.chleba"));
        String nazevSouboru = "err";
        try {
            File file = fileChooser.showOpenDialog(new Stage());
            loadSettingsFromFile(file);
        } catch (Exception e) {
            System.err.println(e);
        }
    }

    private void saveSettToFile(File file, List<Food> listFood, List<String> listAddons) {
        if (file == null) {
            System.out.println("ukládám do appdat.");
            file = Paths.get(System.getenv("APPDATA") + "/AAchlebarna/chlebaSettings.chleba").toFile();
            file.getParentFile().mkdirs();
        } else {
            System.out.println("Uložím kam si zadal [" + file.getAbsolutePath() + "]");
        }
        if (listFood == null || listAddons == null) {
            System.err.println("Emm.. asi ne. Některý list je null");
            return;
        }
        //zápis do souboru:
        BufferedWriter bw = null;
        FileWriter fw = null;
        if (file != null) {
            try {
                fw = new FileWriter(file);
                bw = new BufferedWriter(fw);

                for (Food food : listFood) {
                    bw.write(food.getPrvniPad());
                    bw.newLine();
                    bw.write(food.getCtvrtyPad());
                    bw.newLine();
                    bw.write(food.getPatyPad());
                    bw.newLine();
                }

                for (String addon : listAddons) {
                    bw.newLine();
                    bw.write(addon);
                }

                System.out.println("Hotovo. soubor zapsán");

            } catch (Exception e) {
                System.err.println(e);

            } finally {
                try {
                    if (bw != null) {
                        bw.close();
                    }
                    if (fw != null) {
                        fw.close();
                    }
                } catch (Exception ex) {
                    System.err.println(ex);
                    System.err.println("au");
                }
            }
        } else {
            System.err.println("File =null....");
        }
    }

    private void loadSettingsFromFile(File file) {
        if (file != null) {
            System.out.println("Soubor připraven ke čtení ");
            try {
                Scanner in = new Scanner(new FileReader(file));

                while (in.hasNext()) {
                    try {
                        Food food = new Food();

                        String nextL = in.nextLine();
                        if (nextL.equals("")) {
                            break;
                        }

                        food.setPrvniPad(nextL);
                        food.setCtvrtyPad(in.nextLine());
                        food.setPatyPad(in.nextLine());

                        jdeChleba.AddFood(food);
                    } catch (Exception e) {
                        System.err.println("V souboru bude asi chyba. Inportoval sem snad co se dalo...");
                    }
                }

                while (in.hasNext()) {
                    try {
                        jdeChleba.AddAddon(in.nextLine());
                    } catch (Exception e) {
                        System.err.println("V souboru bude asi chyba [až u addonů]. Inportoval sem snad co se dalo...");
                    }
                }
                in.close();
                ActualizeMainFoodTable();
                EndOfEditingFood();
            } catch (Exception e) {
                System.err.println("chyba  " + e);
            }

        } else {
            System.err.println("někde.. bude chyba kámo.. pokoušíš se otevřít : " + file.getPath());
        }
    }

    @FXML
    private void Menu_File_LoadSettings__Action(ActionEvent event) {
        try {
            loadSettingsFromFile(Paths.get(System.getenv("APPDATA") + "/AAchlebarna/chlebaSettings.chleba").toFile());
        } catch (Exception e) {
            System.err.println(e);
        }

    }

    @FXML
    private void Menu_File_SaveSettingsAction(ActionEvent event) {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new ExtensionFilter("JdeChleba Files", "*.chleba"));
        List<Food> listFood = jdeChleba.getlistOfFood();
        List<String> listAddons = jdeChleba.getListOfAddons();

        saveSettToFile(null, listFood, listAddons);
    }

}
