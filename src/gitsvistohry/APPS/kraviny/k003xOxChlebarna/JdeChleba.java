/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.kraviny.k003xOxChlebarna;

import java.io.Serializable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author Ondra
 */
public class JdeChleba implements Serializable {

    private transient ObservableList<Food> listOfFood = FXCollections.observableArrayList();
    private transient ObservableList<String> listOfAddons = FXCollections.observableArrayList();        // list přídavných věcí (s máslem ; se salámem)
    private transient ObservableList<String> listOfEnds = FXCollections.observableArrayList();

    private String TheEnd = "";

    private boolean splitIntoLines = true;
    private boolean writeTheEnd = true;
    private boolean countWords = true;

    private final String part1_1 = "A tak jde ";
    private final String part1_2 = " a potkají ";
    private final String part2_1 = "A ";
    private final String part2_2 = " se ptá: „";
    private final String part2_3 = " můžu jít s váma?\" ";
    private final String part3_1 = "Přičemž ";
    private final String part3_2 = " odpoví:";
    private final String part3_3 = " „Jó, můžeš.\"";

    private final String part01_1 = "Jde ";
    private final String part01_2 = " a potká ";
    private final String part02_1 = part2_1;
    private final String part02_2 = part2_2;
    private final String part02_3 = " můžu jít s tebou?\" ";
    private final String part03_1 = part3_1;
    private final String part03_2 = part3_2;
    private final String part03_3 = part3_3;

////    private final String[] loadedAddonsList;           
    String partSeparator = "\n";

    public JdeChleba() {                                //konstruktor
////        this.loadedAddonsList = new String[]{" s máslem", " s máslem se salámem"};
        RecomputeVariablesFromLoadedSettings();
        LoadSettings();
    }

    /**
     * * použije nastavení
     */
    private void RecomputeVariablesFromLoadedSettings() {                          //použití nastavení. použité i v konstruktoru
        if (splitIntoLines) {
            partSeparator = "\n";
        } else {
            partSeparator = "";
        }
//        TheEnd=FXMLDocumentController.getEnd();

    }

    /**
     * @return vrátí příběh o jdoucích jídlech pomocí načteného nastavení.
     */
    public String getTheFinalStory(boolean _writeTheEnd, String __TheEnd) {
        writeTheEnd = _writeTheEnd;
        TheEnd = __TheEnd;

        System.out.println("listOfFood.size: " + listOfFood.size());
        StringBuffer resultMyMyBuff = new StringBuffer(300);
        String resultMyMy = "err";

        RecomputeVariablesFromLoadedSettings();

        int FinalFoodsSize = listOfFood.size() * (listOfAddons.size() + 1);
        System.out.println("finalFoodSize= " + FinalFoodsSize);
        if (FinalFoodsSize <= 2) {
            System.err.println("[getTheFinalStory] MUSÍ BÝT ALESPOŇ 3 JÍDLA");
        } else {//můžeme začít psát text
            long start = 0;
            long end = 0;
            start = System.currentTimeMillis();

            //první část
            int numberOfWalkingFood = 1;      //  resultMyMy+=part01_1+listOfFood.get(0).getPrvniPad()+part01_2+getNextFood_4Pad(numberOfWalkingFood)+". "+partSeparator+//   part2_1+getNextFood_1Pad(numberOfWalkingFood)+part2_2+listOfFood.get(0).getPatyPad()+part02_3+partSeparator+//                    part3_1+ listOfFood.get(0).getPrvniPad()+part3_2+partSeparator+partSeparator;

            resultMyMyBuff.append(part01_1).append(getWalkingFoods_1Pad(numberOfWalkingFood)).
                    append(part01_2).append(getNextFood_4Pad(numberOfWalkingFood))
                    .append(". ").append(partSeparator).append(part02_1).append(getNextFood_1Pad(numberOfWalkingFood)).
                    append(part02_2).append(FirstCapital(getWalkingFoods_5Pad(numberOfWalkingFood))).append(part02_3).
                    append(partSeparator).append(part03_1).append(getWalkingFoods_1Pad(numberOfWalkingFood)).
                    append(part03_2 + part03_3);

            //hlavní cyklení všeho dalšího
            for (int i = 1; i < FinalFoodsSize - (listOfAddons.size() + 1); i++) {                          //-1 protože už 1 proběhlo, a další -(addons.size+1) protože chceme aby poslední jídlo šlo bez přídavků 
                numberOfWalkingFood++;
                resultMyMyBuff.append(partSeparator).append(partSeparator).append(part1_1)
                        .append(getWalkingFoods_1Pad(numberOfWalkingFood)).append(part1_2)
                        .append(getNextFood_4Pad(numberOfWalkingFood)).append(". ")
                        .append(partSeparator).append(part2_1).append(getNextFood_1Pad(numberOfWalkingFood))
                        .append(part2_2).append(FirstCapital(getWalkingFoods_5Pad(numberOfWalkingFood)))
                        .append(part2_3).append(partSeparator).append(part3_1)
                        .append(getWalkingFoods_1Pad(numberOfWalkingFood)).append(part3_2 + part3_3);
            }

            resultMyMy = resultMyMyBuff.toString();
            if (writeTheEnd) {
                resultMyMy = resultMyMy.substring(0, resultMyMy.length() - part3_3.length()) + ' ' + TheEnd;
            }

            end = System.currentTimeMillis();
            start = end - start;
            System.out.println("\n getTheFinalStory trvalo: " + start + " milisekund.\n");
        }
        return resultMyMy;
    }

    /**
     * * Vrátí String jídel oddělených čárkami v 1. pádě. Potřebné pro getTheFinalStory.
     */
    private String getWalkingFoods_1Pad(int numberOfWalkingFoods) {
        int numberOfWrittenFoods = 0;
        String pom = "";
        for (int i = 0; i < listOfFood.size(); i++) {
            pom += listOfFood.get(i).getPrvniPad();
            numberOfWrittenFoods++;
            if (numberOfWalkingFoods == numberOfWrittenFoods) {
                return pom;
            }
            pom += (numberOfWalkingFoods==(numberOfWrittenFoods+1)) ? " a " : ", ";
            for (int j = 0; j < listOfAddons.size(); j++) {
                pom += listOfFood.get(i).getPrvniPad() + ' ' + listOfAddons.get(j);
                numberOfWrittenFoods++;
                if (numberOfWalkingFoods == numberOfWrittenFoods) {
                    return pom;
                }
                pom += (numberOfWalkingFoods==(numberOfWrittenFoods+1)) ? " a " : ", ";
            }
        }
        System.err.println("až sem by to ale dojít nemělo.. kudla");
        return "RANDOM CHYBA 2542.";
    }

    private String getWalkingFoods_5Pad(int numberOfWalkingFoods) {
        int numberOfWrittenFoods = 0;
        String pom = "";
        for (int i = 0; i < listOfFood.size(); i++) {
            pom += listOfFood.get(i).getPatyPad();
            numberOfWrittenFoods++;
            if (numberOfWalkingFoods == numberOfWrittenFoods) {
                return pom;
            }
            pom += ", ";
            for (int j = 0; j < listOfAddons.size(); j++) {
                pom += listOfFood.get(i).getPatyPad() + ' ' + listOfAddons.get(j);
                numberOfWrittenFoods++;
                if (numberOfWalkingFoods == numberOfWrittenFoods) {
                    return pom;
                }
                pom += ", ";
            }
        }
        System.err.println("až sem by to ale dojít nemělo.. kudla");
        return "RANDOM CHYBA 36452.";
    }

    private String getNextFood_1Pad(int numberOfWalkingFoods) {
        int numberOfWrittenFoods = 0;
        for (int i = 0; i < listOfFood.size(); i++) {
            numberOfWrittenFoods++;
            if (numberOfWalkingFoods + 1 == numberOfWrittenFoods) {
                return listOfFood.get(i).getPrvniPad();
            }
            for (int j = 0; j < listOfAddons.size(); j++) {
                numberOfWrittenFoods++;
                if (numberOfWalkingFoods + 1 == numberOfWrittenFoods) {
                    return listOfFood.get(i).getPrvniPad() + ' ' + listOfAddons.get(j);
                }
            }
        }
        System.err.println("až sem by to ale dojít nemělo.. kudla");
        return "RANDOM CHYBA 787528472.";
    }

    private String getNextFood_4Pad(int numberOfWalkingFoods) {
        int numberOfWrittenFoods = 0;
        for (int i = 0; i < listOfFood.size(); i++) {
            numberOfWrittenFoods++;
            if (numberOfWalkingFoods + 1 == numberOfWrittenFoods) {
                return listOfFood.get(i).getCtvrtyPad();
            }
            for (int j = 0; j < listOfAddons.size(); j++) {
                numberOfWrittenFoods++;
                if (numberOfWalkingFoods + 1 == numberOfWrittenFoods) {
                    return listOfFood.get(i).getCtvrtyPad() + ' ' + listOfAddons.get(j);
                }
            }
        }
        System.err.println("numberOfWrittenFoods= " + numberOfWrittenFoods + ", " + "numberOfWalkingFoods: " + numberOfWalkingFoods);
        return "RANDOM CHYBA 35485354.";
    }

    public String getTheFinalStory(boolean SplitIntoLines) {
        boolean pom = splitIntoLines;
        splitIntoLines = SplitIntoLines;
        String result = getTheFinalStory(writeTheEnd, TheEnd);
        splitIntoLines = pom;
        return result;
    }

    public void changeFood(int index, Food food) {
        listOfFood.set(index, food);
    }

    public void changeAddon(int index, String addon) {
        listOfAddons.set(index, addon);
    }

    private String FirstCapital(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    //nezajímavé
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public ObservableList<Food> getlistOfFood() {
        return listOfFood;
    }

    public void setListOfFood(ObservableList<Food> listOfFood) {
        this.listOfFood = listOfFood;
    }

    public void AddFood(Food newFood) {
        listOfFood.add(newFood);
        //new Food(newFood.getId(),newFood.getPrvniPad(),newFood.getCtvrtyPad(),newFood.getPatyPad
    }

    public void AddFood(int index, String Pad1, String Pad4, String Pad5) {
        listOfFood.add(new Food(index, Pad1, Pad4, Pad5));
        //new Food(newFood.getId(),newFood.getPrvniPad(),newFood.getCtvrtyPad(),newFood.getPatyPad
    }

    public void AddAddon(String addon) {
        listOfAddons.add(addon);
    }

    public boolean isSplitIntoLines() {
        return splitIntoLines;
    }

    public void setSplitIntoLines(boolean splitIntoLines) {
        this.splitIntoLines = splitIntoLines;
        RecomputeVariablesFromLoadedSettings();
    }

    public ObservableList<String> getListOfAddons() {
        return listOfAddons;
    }

    public boolean isWriteTheEnd() {
        return writeTheEnd;
    }

    public void setWriteTheEnd(boolean writeTheEnd) {
        this.writeTheEnd = writeTheEnd;
    }

    public void setListOfAddons(ObservableList<String> listOfAddons) {
        this.listOfAddons = listOfAddons;
    }

    public void _DeleteListOfAddons() {
        listOfAddons.clear();
    }

    public ObservableList<String> getListOfEnds() {
        return listOfEnds;
    }

    public void setListOfEnds(ObservableList<String> listOfEnds) {
        this.listOfEnds = listOfEnds;
    }

    private void LoadSettings() {
        listOfEnds.clear();
        listOfEnds.addAll("„Ne.\"", "„Ne.\" Čas na smích.");

    }

    public String getTheEnd() {
        return TheEnd;
    }

    public void setTheEnd(String TheEnd) {
        this.TheEnd = TheEnd;
    }

    public boolean isCountWords() {
        return countWords;
    }

    public void setCountWords(boolean countWords) {
        this.countWords = countWords;
    }
}
