/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.kraviny.k003xOxChlebarna;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author Ondra
 */
public class Chlebarna extends Application {
    
    public static Stage stage;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
//        Application.setUserAgentStylesheet(Application.);      //modena/caspain
        
        
        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
        @Override
        public void handle(WindowEvent event) {
            ChlebarnaCore.shutdown();
            event.consume();
        }
        });
        
        Scene scene = new Scene(root);
        stage.setTitle("muhehehe");
        
        stage.setScene(scene);
        stage.setMinWidth(660);
        stage.setMinHeight(470);
        stage.show();
        this.stage=stage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
