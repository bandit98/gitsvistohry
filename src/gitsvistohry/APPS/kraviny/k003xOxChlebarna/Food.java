/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.kraviny.k003xOxChlebarna;

import java.io.Serializable;

/**
 *
 * @author Ondra
 */
public class Food implements Serializable{
    private int id =-3 ;
    private String prvniPad="";
    private String ctvrtyPad="";
    private String patyPad="";
    
    public Food() {
        id=-2;
        prvniPad="default, ERROR";
        ctvrtyPad="default, ERROR";
        patyPad="default, ERROR";
    }
    
    public Food( String prvniPad, String ctvrtyPad, String patyPad) {
        this.prvniPad = prvniPad;
        this.ctvrtyPad = ctvrtyPad;
        this.patyPad = patyPad;
    }

    public Food(int id, String prvniPad, String ctvrtyPad, String patyPad) {
        this.id = id;
        this.prvniPad = prvniPad;
        this.ctvrtyPad = ctvrtyPad;
        this.patyPad = patyPad;
    }
    
    
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrvniPad() {
        return prvniPad;
    }

    public void setPrvniPad(String prvniPad) {
        this.prvniPad = prvniPad;
    }

    public String getCtvrtyPad() {
        return ctvrtyPad;
    }

    public void setCtvrtyPad(String ctvrtyPad) {
        this.ctvrtyPad = ctvrtyPad;
    }

    public String getPatyPad() {
        return patyPad;
    }

    public void setPatyPad(String patyPad) {
        this.patyPad = patyPad;
    }
}
