/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gitsvistohry.APPS.hry.g004xOxWaveMaster;

import gitsvistohry.APPS.hry.g004xOxWaveMaster.EnemyTypes.e001BasicEnemy;
import java.util.List;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ondra
 */
public class WaveTest {

    public WaveTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of main method, of class Wave.
     */
    @Test
    public void testMain() {
        Wave wawe = new Wave(1);

        wawe.addEnemy(new e001BasicEnemy(0, 0,null,null), 6);
        wawe.addEnemy(new e001BasicEnemy(1, 0,null,null), 2000);
        wawe.addEnemy(new e001BasicEnemy(2, 0,null,null), 200);
        wawe.addEnemy(new e001BasicEnemy(3, 0,null,null), 2000);
        wawe.addEnemy(new e001BasicEnemy(4, 0,null,null), 1000);
        wawe.addEnemy(new e001BasicEnemy(5, 0,null,null), 30);
        wawe.addEnemy(new e001BasicEnemy(6, 0,null,null), 300);
        wawe.addEnemy(new e001BasicEnemy(7, 0,null,null), 300);
        wawe.addEnemy(new e001BasicEnemy(8, 0,null,null), 300);
        wawe.addEnemy(new e001BasicEnemy(9, 0,null,null), 300);
        wawe.addEnemy(new e001BasicEnemy(10, 0,null,null), 300);
        wawe.addEnemy(new e001BasicEnemy(11, 0,null,null), 300);
        wawe.addEnemy(new e001BasicEnemy(12, 0,null,null), 300);

        while (!wawe.tick()) {
            try {
                Thread.sleep(3);
            } catch (InterruptedException ex) {
                Logger.getLogger(Wave.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }

            if (wawe.peekEnemiesREADYToRoll() != null) {
                List<Enemy> tmp1 = wawe.peekEnemiesREADYToRoll();
                for (int j = 0; j < tmp1.size(); j++) {
                    System.out.println("peek: " + tmp1.get(j));
                }
                List<Enemy> tmp = wawe.popEnemiesREADYToRoll();
                for (int j = 0; j < tmp.size(); j++) {
                    System.out.println("pop: " + tmp.get(j));
                }
            }
        }

    }

}
